<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <?php require('include/top.php'); ?>
    <body>
        <?php require('include/script.php'); ?>
        <div id="dasboard">
            <header id="header" class="relv">
                <div class="logo_navbar clearfix">
                    <div class="container-fluid clearfix">
                        <a id="logo" href="<?php echo asset('/') ?>"><img src="<?php echo asset('images/logo.png') ?>" alt="instalimo" height="38" width="216"></a>
                        <a class="menu_trigger" href="#"><img src="<?php echo asset('images/menu.png') ?>" alt=""></a>
                        <nav id="main_nav">
                            <ul>
                                <li><a href="#">BLOG</a></li>
                                <?php if (Illuminate\Support\Facades\Auth::User()) { ?>
                                    <li><a href="<?php echo asset('logout') ?>">LOGOUT</a></li>
                                    <li><a href="<?php echo asset('dashboard') ?>">List Your Limo !</a></li>   
                                <?php } else { ?>

                                    <li><a class="fancybox "  href="#light_boxs">LOGIN</a></li>
                                    <li><a class="fancybox "  href="#light_boxs">List Your Limo !</a></li>   

                                <?php } ?>

                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
            <div id="light_boxs" class="light" style="display:none">
                <?php include 'include/login.php'; ?>
            </div>

            <div id="limo_slider" class="relv">
                <button class="btn" style="left:0; width:120px" onclick="goBack()" >Back</button>
                <!--                <a class="btn" href="#">Change to Map View</a>-->
                <div class="container relv">


                    <script>
                        function goBack() {
                            window.history.back();
                        }
                    </script>
                </div>
                <style>
                    #limo_slider .slide img {
                        visibility: hidden;
                    }
                    #limo_slider .slide div {
                        background-size:cover !important;
                    }
                </style>
                <div class="bxslider" > 
                    <?php foreach ($photo as $image): ?>
                        <div class="slide"  ><div style="background:url(<?php echo asset('images/limo_image/' . $image->image_name) ?>) center;"><img src="<?php echo asset('images/limo_image/' . $image->image_name) ?>" alt=""></div></div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="container relv" >

                <div id="bx-pager" >
                    <?php
                    $i = 0;
                    foreach ($photo as $image):
                        ?>

                        <a data-slide-index="<?php echo $i ?>" href=""><img src="<?php echo asset('images/limo_image/' . $image->image_name) ?>"/></a>

                        <?php
                        $i++;
                    endforeach;
                    ?>
                </div>
            </div>
            <div id="main_content" class="default">
                <div class="vehcice_nbar">
                    <div class="container">
                        <h1 class="vehilce_name inline_block"><?php echo $limo[0]->veh_name ?></h1>
                        <div class="pw-widget pw-size-medium pw-counter-show vehicle_sshare" id="javascriptWidget"> 
                            <div class="inline_block">Share:</div>
                            <a class="pw-button-facebook"></a>   
                            <a class="pw-button-pinterest"></a>
                            <a class="pw-button-twitter"></a>   
                            <a class="pw-button-email"></a>   
                            <a class="pw-button-post"></a>  
                        </div>
                        <div class="clear"></div>
                        <h2 class="vehilce_name inline_bloc">Seats(<?php echo $limo[0]->veh_seats ?>)</h2>
            <!--            <a class="star  <?php if ($sum >= 1) { ?>fullStar<?php } ?>" title="1"></a>
                        <a class="star <?php if ($sum >= 2) { ?>fullStar<?php } ?>" title="2"></a>
                        <a class="star <?php if ($sum >= 3) { ?>fullStar<?php } ?>" title="3"></a>
                        <a class="star <?php if ($sum >= 4) { ?>fullStar<?php } ?>" title="4"></a>
                        <a class="star <?php if ($sum >= 5) { ?>fullStar<?php } ?>" title="5"></a>-->
                    </div>

                </div>
                <div class="container">
                    <div class="row clearfix">
                        <div class="col-sm-8 col-md-9">
                            <div class="content_section">
                                <h2 class="sub_title">Limo Description</h2>
                                <p>
                                    <?php echo $limo[0]->veh_des ?>  
                                </p>
                            </div>
                            <div id="amenites" class="content_section">
                                <div class="row clearfix">
                                    <div class="col-sm-4">
                                        <h2 class="sub_title">Amenities</h2>
                                    </div>
                                    <!--<form>-->
                                    <div class="col-sm-8">
                                        <div class=" row clearfix">
                                            <?php
                                            foreach ($aminties as $amint) {
                                                $newarray = str_replace(',', '', $limo[0]->amenities);
                                                $findamienty = strrchr($newarray, $amint['amen_title']);
                                                ?>
                                                <div class="col-xs-6">
                                                    <div class="custom_inputs">
                                                        <input id="<?php echo $amint['amen_id'] ?>" type="checkbox" name="radio" value="<?php echo $amint['amen_id'] ?>" <?php if ($findamienty == TRUE) { ?> checked="checked" <?php } ?>disabled="">
                                                        <label for="<?php echo $amint['amen_id'] ?>"><span>&nbsp;</span><div class="inline_block"><?php echo $amint['amen_title'] ?></div> </label>
                                                    </div>

                                                </div>
                                            <?php }; ?>
                                        </div>


                                    </div>

                                    <!--</form>-->
                                </div>
                            </div>
                            <!--                 <div id="vehcile_review" class="content_section">
                                                <h2 class="sub_title inline_text"><?php echo $count ?> Reviews</h2> 
                                                <div class="stars inline_text">
                                                    <a class="star  <?php if ($sum >= 1) { ?>fullStar<?php } ?>" title="1"></a>
                                                    <a class="star <?php if ($sum >= 2) { ?>fullStar<?php } ?>" title="2"></a>
                                                    <a class="star <?php if ($sum >= 3) { ?>fullStar<?php } ?>" title="3"></a>
                                                    <a class="star <?php if ($sum >= 4) { ?>fullStar<?php } ?>" title="4"></a>
                                                    <a class="star <?php if ($sum >= 5) { ?>fullStar<?php } ?>" title="5"></a>
                                                 </div>
                                                <div class="row clearfix">
                                                    <div class="col-sm-4">
                                                            <h3 >Summary</h3>
                                                    </div>
                                                    <form>
                                                        <div class="col-sm-4 alpha_sm omega_sm col-xs-6">
                                                            <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Accuracy</div>
                                                                   <div class="star_rating col-xs-6">
                                                            <input type="radio" name="example" class="rating" value="1" />
                                                        <input type="radio" name="example" class="rating" value="2" />
                                                        <input type="radio" name="example" class="rating" value="3" />
                                                        <input type="radio" name="example" class="rating" value="4" />
                                                        <input type="radio" name="example" class="rating" value="5" />
                                                    </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Communication</div>
                                                                   <div class="star_rating col-xs-6">
                                                            <input type="radio" name="example" class="rating" value="1" />
                                                        <input type="radio" name="example" class="rating" value="2" />
                                                        <input type="radio" name="example" class="rating" value="3" />
                                                        <input type="radio" name="example" class="rating" value="4" />
                                                        <input type="radio" name="example" class="rating" value="5" />
                                                    </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Cleanliness</div>
                                                                   <div class="star_rating col-xs-6">
                                                            <input type="radio" name="example" class="rating" value="1" />
                                                        <input type="radio" name="example" class="rating" value="2" />
                                                        <input type="radio" name="example" class="rating" value="3" />
                                                        <input type="radio" name="example" class="rating" value="4" />
                                                        <input type="radio" name="example" class="rating" value="5" />
                                                    </div>
                                                                 </div>
                                                             </div>
                                                        </div>
                                                        <div class="col-sm-4 omega_sm col-xs-6">
                                                            <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name ">Speed</div>
                                                                   <div class="star_rating col-xs-6">
                                                            <input type="radio" name="example" class="rating" value="1" />
                                                        <input type="radio" name="example" class="rating" value="2" />
                                                        <input type="radio" name="example" class="rating" value="3" />
                                                        <input type="radio" name="example" class="rating" value="4" />
                                                        <input type="radio" name="example" class="rating" value="5" />
                                                    </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Price</div>
                                                                   <div class="star_rating col-xs-6">
                                                            <input type="radio" name="example" class="rating" value="1" />
                                                        <input type="radio" name="example" class="rating" value="2" />
                                                        <input type="radio" name="example" class="rating" value="3" />
                                                        <input type="radio" name="example" class="rating" value="4" />
                                                        <input type="radio" name="example" class="rating" value="5" />
                                                    </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Value</div>
                                                                   <div class="star_rating col-xs-6">
                                                            <input type="radio" name="example" class="rating" value="1" />
                                                        <input type="radio" name="example" class="rating" value="2" />
                                                        <input type="radio" name="example" class="rating" value="3" />
                                                        <input type="radio" name="example" class="rating" value="4" />
                                                        <input type="radio" name="example" class="rating" value="5" />
                                                    </div>
                                                                 </div>
                                                             </div>
                                                        </div>
                                                    </form>
                                                </div>
                                             </div>-->
                            <!--                 <div id="testimonial" class="content_section">
                                                  <div class="reviews_list">
                            <?php foreach ($reviews as $review) { ?>
                                                                  <div class="review">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-4 text-center u_img">
                                                                        <img src="<?php echo asset('images/review_images/' . $review->image) ?>"  alt=""/>
                                                                    <div><?php echo $review->firstname . ' ' . $review->lastname ?></div>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                   
                                                                        
                                                                        
                                                                            <p>
                                <?php echo $review->review_message ?>
                                                                            </p>
                                                                             <div class="stars">
                                                                                <a class="star  <?php if ($review->review_rating >= 1) { ?>fullStar<?php } ?>" title="1"></a>
                                                    <a class="star <?php if ($review->review_rating >= 2) { ?>fullStar<?php } ?>" title="2"></a>
                                                    <a class="star <?php if ($review->review_rating >= 3) { ?>fullStar<?php } ?>" title="3"></a>
                                                    <a class="star <?php if ($review->review_rating >= 4) { ?>fullStar<?php } ?>" title="4"></a>
                                                    <a class="star <?php if ($review->review_rating >= 5) { ?>fullStar<?php } ?>" title="5"></a>
                                                                             </div>
                                <?php
                                echo $newDate = date("d M Y", strtotime($review->review_time));
                                ?>
                                                                        </div>
                                                                    </div>
                                                                        
                                                                  </div><?php } ?>
                                                      <div class="text-right review_trigger"><a href="#">See All Reviews</a></div>
                                                </div>
                                                <div class="text-center"><a class="fancybox" href="#add-a-review">Submit Your Review</a></div>
                                                <style>
                                                                            #add-a-review .review .row  {
                                                                                            margin-bottom:20px;
                                                                                    }
                                                                            #add-a-review {
                                                                                    padding:30px 20px;
                                                                                    }
                                                </style>
                                                <div id="add-a-review" style="max-width:800px;display: none;">
                                                <a onclick="$.fancybox.close()" style="position:absolute; top:0; right:0; cursor:pointer"><img alt="" src="http://localhost:8000/images/close_btn.png"></a>
                                                        <h4 class="litbox_title">
                                                        Add a Review
                                                        <small>Please Fill in the Fields</small>
                                                        </h4>
                                                    <div class="spacer"></div>
                                                    <div class="spacer"></div>
                                                    <div class="spacer"></div>
                                                   <div class="row clearfix">
                                                        <div class="col-sm-6">
                                                            <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Accuracy</div>
                                                                    <div class="star_rating col-xs-6">
                                                                        <input type="radio" name="example" class="rating" value="1" />
                                                                        <input type="radio" name="example" class="rating" value="2" />
                                                                        <input type="radio" name="example" class="rating" value="3" />
                                                                        <input type="radio" name="example" class="rating" value="4" />
                                                                        <input type="radio" name="example" class="rating" value="5" />
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Communication</div>
                                                                    <div class="star_rating col-xs-6">
                                                                        <input type="radio" name="example" class="rating" value="1" />
                                                                        <input type="radio" name="example" class="rating" value="2" />
                                                                        <input type="radio" name="example" class="rating" value="3" />
                                                                        <input type="radio" name="example" class="rating" value="4" />
                                                                        <input type="radio" name="example" class="rating" value="5" />
                                                                    </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Cleanliness</div>
                                                                    <div class="star_rating col-xs-6">
                                                                        <input type="radio" name="example" class="rating" value="1" />
                                                                        <input type="radio" name="example" class="rating" value="2" />
                                                                        <input type="radio" name="example" class="rating" value="3" />
                                                                        <input type="radio" name="example" class="rating" value="4" />
                                                                        <input type="radio" name="example" class="rating" value="5" />
                                                                                    </div>
                                                                 </div>
                                                             </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Accuracy</div>
                                                                    <div class="star_rating col-xs-6">
                                                                        <input type="radio" name="example" class="rating" value="1" />
                                                                        <input type="radio" name="example" class="rating" value="2" />
                                                                        <input type="radio" name="example" class="rating" value="3" />
                                                                        <input type="radio" name="example" class="rating" value="4" />
                                                                        <input type="radio" name="example" class="rating" value="5" />
                                                                                    </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Communication</div>
                                                                    <div class="star_rating col-xs-6">
                                                                        <input type="radio" name="example" class="rating" value="1" />
                                                                        <input type="radio" name="example" class="rating" value="2" />
                                                                        <input type="radio" name="example" class="rating" value="3" />
                                                                        <input type="radio" name="example" class="rating" value="4" />
                                                                        <input type="radio" name="example" class="rating" value="5" />
                                                                               </div>
                                                                 </div>
                                                             </div>
                                                             <div class="review">
                                                               <div class="row clearfix">
                                                                    <div class="col-xs-6 review_name">Cleanliness</div>
                                                                    <div class="star_rating col-xs-6">
                                                                        <input type="radio" name="example" class="rating" value="1" />
                                                                        <input type="radio" name="example" class="rating" value="2" />
                                                                        <input type="radio" name="example" class="rating" value="3" />
                                                                        <input type="radio" name="example" class="rating" value="4" />
                                                                        <input type="radio" name="example" class="rating" value="5" />
                                                                                    </div>
                                                                 </div>
                                                             </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <div class="col-sm-4"><div class="h3">Add a Review</div></div>
                                                            <div class="col-sm-8">
                                                                    <textarea></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <div class="col-sm-4"></div>
                                                            <div class="col-sm-8">
                                                                    <input class="btn" type="submit" value="Submit Review">
                                                            </div>
                                                        </div>
                                                    </div>
                                                 </div>
                                             </div>-->
                            <div class="content_section">
                                <h2 class="sub_title">Cancellation Policy</h2>
                                <p>
                                    <?php echo $limo[0]->veh_policy ?>
                                </p>
                            </div>
                            <div class="content_section">
                                <h2 class="sub_title">Rules</h2>
                                <p>
                                    <?php echo $limo[0]->veh_rules ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-3">
                            <div id="sidebar_limo">
                                <div class="price">
                                    <div class="fl">$<?php echo $limo[0]->veh_price ?> Per Hour</div>
                                    <div class="fr">Minium Hours <?php echo $limo[0]->veh_booking_mintime?></div>
                                    <div class="clear"></div>

                                </div>
                                <div id="feat_vehicle" class="sidebar_section">

                                    <div class="company_name">
                                        <?php echo $company[0]->comp_name ?>
                                    </div>

                                    <div class="text-center"><div class="vehicle_img"><img src="<?php if ($company[0]->comp_image) {
                                            echo asset('images/comp_images/' . $company[0]->comp_image);
                                        } else {
                                            echo asset('images/comp_images/demo.png');
                                        } ?>"  alt=""/></div></div>
                                    <div class="contact"></div>
                                    <a class="btn medium fancybox" href="#inquire_now">INQUIRE NOW</a>
                                    <div id="inquire_now" class="light" style="display:none">
                                        <a style="position:absolute; top:0; right:0; cursor:pointer" onclick="$.fancybox.close()" ><img src="<?php echo asset('images/close_btn.png') ?>" alt=""></a>
                                        <h3 class="litbox_title">
                                            Send Message
                                            <small>Please Type Message For  <?php echo $limo[0]->veh_name ?></small>
                                        </h3>
                                        <form method="post" >
                                            <div class="clearfix">
                                                <div class="col-sm-6">
                                                    <input value="<?php echo Session::get('v_fname') ?>" type="hidden" placeholder="Please Enter your First Name"  name="fname" id="fname">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input value="<?php echo Session::get('v_lname') ?>" type="hidden" placeholder="Please Enter your Last Name"  name="lname"id="lname">

                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="col-sm-12">
                                                    <input value="<?php echo Session::get('v_email') ?>" type="hidden" placeholder="Email Address ( Don't worry, we keep this private)*"  name="email"id="email">

                                                </div>
                                            </div>
                                            <div class="clearfix">
                                                <div class="col-sm-12">
                                                    <textarea  name="message" id="message"></textarea>
                                                </div>
                                            </div>

                                            <div class="clearfix">
                                                <div class="col-sm-12">
                                                    <input class="btn large" type="button" value="Send" onclick="sendmessage('<?php echo $limo[0]->user_id ?>')">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <span class="alert success" id="success_message" style="display: none">Thank You! Your message has been sent. If You like what you see we recommend you give them a call. Everything is better #InaLimo </span>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="spacer"></div>
                                    <div class="text-center"><strong>Like what you see ?</strong></div>
                                    <!--<div class="text-center"><strong>Email.</strong>  <?php echo $company[0]->comp_email ?></div>-->
                                    <!--<div class="text-center"><strong>Or Call.</strong>  <?php echo $company[0]->comp_phone ?></div>-->
                                    <!--<div class="text-center"><strong>Contact No.</strong>  <?php echo $company[0]->comp_mobile ?></div>-->
                                </div>
                                <div  class="sidebar_section other_vehicle clearfix text-center">
                                    <div class="name">People also liked...</div>
<?php
foreach ($related_limo as $r_limo):
    if (isset($r_limo->limo_id)) {
        ?>
                                            <div class="vehicle">
                                                <div class="vehicle_img ">
                                                    <a href="<?php echo asset('showsearchpage/' . $r_limo->user_id) . '/' . $r_limo->limo_id ?>"><img src="<?php echo asset('images/limo_image/' . $r_limo->image_name) ?>"  alt=""/></a>
                                                    <div class="price clearfix">
                                                        <div class="fl">$<?php echo $r_limo->veh_price ?></div>
                                                        <div class="fr"><a href="<?php echo asset('showsearchpage/' . $r_limo->user_id) . '/' . $r_limo->limo_id ?>"><?php echo $r_limo->veh_name ?></a></div>

                                                    </div>
                                                </div>
                                            </div>
    <?php } endforeach; ?>
                                    <form id="seeallsearch" method="post" action="<?php echo asset('get_all_related') ?>"> 
                                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                                        <input type="hidden" name="location" value="<?php echo $limo[0]->owner_city ?>">
                                        <input type="hidden" name="limo_id" value="<?php echo $limo[0]->limo_id ?>">
                                        <!--<a class="btn medium" href="#" onclick="seeallsearch()">SEE MORE</a>-->   
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
<?php require('include/footer.php'); ?>
        <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=tpum1kil17iao7d248vk&retina=true" type="text/javascript"></script>
        <script type="text/javascript">
                                            function seeallsearch() {
                                                $('#seeallsearch').submit();
                                            }
                                            function sendmessage(user_id) {
                                                fname = $('#fname').val();
                                                lname = $('#lname').val();
                                                email = $('#email').val();
                                                message = $('#message').val();
//                                                if (fname === '') {
//                                                    $('#fname').css('border-color', 'red');
//                                                }
//                                                else if (lname === '') {
//                                                    $('#lname').css('border-color', 'red');
//                                                    $('#fname').css('border-color', 'black');
//
//                                                }
//                                                else if ((!IsEmail(email))) {
//                                                    $('#email').css('border-color', 'red');
//                                                     $('#lname').css('border-color', 'black');
//                                                    $('#fname').css('border-color', 'black');
//                                                }
                                                 if (message === '') {
                                                    $('#message').css('border-color', 'red');
//                                                    $('#email').css('border-color', 'black');
//                                                     $('#lname').css('border-color', 'black');
//                                                    $('#fname').css('border-color', 'black');
                                                }
                                                else {

                                                    $.ajax({
                                                        type: "GET",
                                                        data: {"message": message, "user_id": user_id, "sender_email": email, "sender_name": fname + ' ' + lname},
                                                        url: "<?php echo asset('save_message'); ?>",
                                                        success: function(data) {
//                                                            $('#fname').css('border-color', 'gray');
//                                                            $('#fname').val('');
//                                                            $('#lname').css('border-color', 'gray');
//                                                            $('#lname').val('');
//                                                            $('#email').css('border-color', 'gray');
//                                                            $('#email').val('');
                                                            $('#message').css('border-color', 'gray');
                                                            $('#message').val('');
                                                            $('#success_message').show().fadeOut(5000);
                                                        }});

                                                }
                                            }
                                            function IsEmail(email) {
                                                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                                return regex.test(email);
                                            }
//post_widget('#javascriptWidget', {
//  buttons: ['twitter', { id: 'post', counter: 'show' }],
//  url: 'ijaz'
//
//});
        </script>


    </body>
</html>