 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>Revenue Detail</h2>
                                <span>Here is the list of revenue generated.</span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <section class="panel">
        <!--                                <header class="panel-heading">Search By Category Name or By Date</header>-->
                                        <div class="panel-body">
                                            <form id='history' action="<?php echo asset('get_revenue_details_byDate') ?>" class="ride_history" method="post">
                                                <div class="form-group input-group search_cat">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Start Date:</label>
                                                                <input type="date" id="from" name="from" <?php if(isset($fromDate)) { ?> value="<?php echo $fromDate; } ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" > 
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label">End Date:</label>
                                                                <input type="date" id="to" name="to" <?php if(isset($toDate)) { ?> value="<?php echo $toDate; }?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" > 
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <span class="input-group-btn">
<!--                                                            <button class="btn btn-primary btn-sm go_btn" id="search" type="submit">Search</button>-->
                                                        <input type="submit" value="Search" class="btn btn-primary btn-sm go_btn">
                                                    </span>
                                                </div> 
                                            </form>

                                        </div>
                                    </section>
                                    <a href="<?php echo asset('get_revenue_details')?>" class="btn payment_detailanchor">All</a>
                                </div>
                            </div>
                            <div id="demo">
<!--                                <div class="filter-search">
                                    <input class="search" type="search" data-column="0" placeholder="Search By Date">
                                </div>-->
                                <div class="table-responsive">
                                    <table class="tablesorter">
                                    <thead>
                                        <tr>
<!--                                            <th width="50" class="sorter-false">Sr #</th>-->
                                            <th>Date</th>
                                            <th>Number of Leeds</th>
                                            <th>Total Revenue</th>
                                            
<!--                                            <th width="150" class="cennter sorter-false">Delete</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($dates as $date): ?>
                                        <tr>
                                           
                                            <td><a style="color: blue" href="<?php echo asset('search_details_date?date='.$date->date)?>"><?php echo $date->date?></a></td>
                                           
                                                <td><?php echo $date->views?></td>
                                                
                                                <td><?php echo $date->views*1.99 ?></td>
                                            
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                </div>
                                <div id="pager" class="pager">
                                    <form>
                                        <input type="button" value="&lt;" class="prev" />
                                        <input type="text" class="pagedisplay" readonly/>
                                        <input type="button" value="&gt;" class="next" />
                                        <select class="pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <a href="<?php if(isset($fromDate) && isset($toDate)) { Session::put('fromDate', $fromDate); Session::put('toDate', $toDate);} echo asset('export_revenue_details')?>" class="btn payment_detailanchor">Export</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    
    </body>
</html>



