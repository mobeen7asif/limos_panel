 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>Search Results</h2>
                                <span>Here is the detail of search results.</span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <section class="panel">
            <!--                                <header class="panel-heading">Search By Category Name or By Date</header>-->
                                            <div class="panel-body">
                                                <form id='history' action="<?php echo asset('get_search_results_byDate') ?>" class="ride_history" method="post">
                                                    <div class="form-group input-group search_cat">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Start Date:</label>
                                                                    <input type="date" id="from" name="from" <?php if(isset($fromDate)) { ?> value="<?php echo $fromDate; } ?>" max="<?php echo date("Y-m-d"); ?>" class="form-control" > 
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">End Date:</label>
                                                                    <input type="date" id="to" name="to" <?php if(isset($toDate)) { ?> value="<?php echo $toDate; }?>"  max="<?php echo date("Y-m-d"); ?>" class="form-control" > 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <span class="input-group-btn">
<!--                                                            <button class="btn btn-primary btn-sm go_btn" id="search" type="submit">Search</button>-->
                                                            <input type="submit" value="Search" class="btn btn-primary btn-sm go_btn">
                                                        </span>
                                                    </div> 
                                                </form>

                                            </div>
                                        </section>
                                        <a href="<?php echo asset('get_search_results')?>" class="btn payment_detailanchor">All</a>
                                    </div>
                                </div>
                            <div id="demo">
<!--                                <div class="filter-search">
                                    <input class="search" type="search" data-column="0" placeholder="Search By Date">
                                </div>-->
                                <div class="table-responsive">
                                    <table class="tablesorter" id="table">
                                    <thead>
                                        <tr>
<!--                                            <th width="50" class="sorter-false">Sr #</th>-->
                                            <th>Date</th>
                                            <th>Number of Searches</th>
                                            <th>Average Number of Results</th>
                                            
<!--                                            <th width="150" class="cennter sorter-false">Delete</th>-->
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyid">
                                    <?php foreach ($searches as $search): ?>
                                        <tr>
                                           
                                            <td><a style="color: blue" href="<?php echo asset('search_details_date?date='.$search->date)?>"><?php echo $search->date?></a></td>
                                           
                                                <td><?php echo $search->views?></td>
                                                
                                                <td><?php echo $search->companies?></td>
                                            
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                </div>
                                <div id="pager" class="pager">
                                    <form>
                                        <input type="button" value="&lt;" class="prev" />
                                        <input type="text" class="pagedisplay" readonly/>
                                        <input type="button" value="&gt;" class="next" />
                                        <select class="pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <a href="<?php if(isset($fromDate) && isset($toDate)) { Session::put('fromDate', $fromDate); Session::put('toDate', $toDate);} echo asset('export_search_results')?>" class="btn payment_detailanchor">Export</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
    $( "#from" ).change(function() {
  
        $("#to").attr('required', true);
//            $("#to").attr('min', $('#from').val(););
    });

    $( "#to" ).change(function() {

        $("#from").attr('required', true);
//            $("#from").attr('min', $('#to').val(););
    });
    
    
//    $( "#history" ).submit(function() {
//
//        to = $('#to').val();
//        from = $('#from').val();
//
//        alert(from);
//        $("#tbodyid").empty();
//        initialize(to, from);
//    }); 

    
    
    
    function initialize(to, from) 
    {
         
        alert (from);
        var to = to;
        var from = from;

        $.ajax({
        url: "<?php echo asset('index.php/get_search_results_byDate') ?>",
        type: "post",
        dataType: "json",
        data:{to: to, from: from}
        }).done(function(data){
            console.log(data.searches[0].date);
            console.log(data.searches.length);
            if(data){
               alert("success");
               drawRow(data);
            }else{

                 alert("error");
            }


        });


        function drawRow(rowData)
        {
            for (var i = 0; i < rowData['searches'].length; i++) {
//                    var a = new Date(rowData['searches'][i].start_time * 1000);
//                    var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
//                    var year = a.getFullYear();
//                    var month = months[a.getMonth()];
//                    var date = a.getDate();
//                    var hour = a.getHours();
//                    var min = a.getMinutes();
//                    var sec = a.getSeconds();
//                    var time = year + '-' + month + '-' + date + ' ' + hour + ':' + min + ':' + sec ;

                var row = $("<tr>");
                $("#table").append(row); //this will append tr element to table... keep its reference for a while since we will add cels into it
                row.append($("<td>" + rowData['searches'][i].date + "</td>"));
                row.append($("<td>" + rowData['searches'][i].views + "</td>"));
                row.append($("<td>" + rowData['searches'][i].companies + "</td>"));


//                    row.append($("<td><a href= '#'>Show Detail</a></td>"));
//                    row.append($("<td class=\"center\"><a href=\"#\"><i class=\"fa fa-trash-o\"></i></a></td>"));
                row.append($("</tr>"));

           //$('#table').dataTable();
            }
        }
      
    }
    
    
    </script>
    
    </body>
</html>



