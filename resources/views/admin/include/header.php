<!DOCTYPE html>
<html lang="en"> 
  
    <head>
    <meta charset="utf-8">
	<title><?php echo $title?></title>
        <meta name="description" content="Holy Limo! Check out this sweet ride I found on instalimos.com">
	<meta name="author" content="instalimo.com">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        
        <!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo asset('images/favicon.ico')?>">
	<link rel="apple-touch-icon" href="<?php echo asset('images/apple-touch-icon.png')?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset('images/apple-touch-icon-72x72.png')?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('images/apple-touch-icon-114x114.png')?>">

	<!-- Mobile Specific Metas  -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <!--   Bootstrap CSS   -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/bootstrap.min.css')?>">
        
        <!-- Custome CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/all.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/simple-sidebar.css')?>">
        <script  src="<?php echo asset('css/admin/js/jquery-latest.min.js') ?>"></script>
        
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/font-awesome.min.css')?>">
        
        <!-- Tablesorter: required -->
        <script  src="<?php echo asset('css/admin/js/jquery-latest.min.js') ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/theme.blue.css')?>">
        <script  src="<?php echo asset('css/admin/js/jquery.tablesorter.js') ?>"></script>
        <script src="<?php echo asset('css/admin/js/widget-filter.js') ?>"></script>
        <script  src="<?php echo asset('css/admin/js/jquery.tablesorter.pager.js') ?>"></script>
        
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/openclose.css')?>">
        <script  src="<?php echo asset('css/admin/js/jquery.openclose.js') ?>"></script>

        
        
        
        <script id="js">
$(function(){
  var pagerOptions = {
    container: $(".pager"),
    output: '{startRow} - {endRow} / {filteredRows} ({totalRows})',
//    fixedHeight: true,
    removeRows: false,
    cssGoto: '.gotoPage'
  };
  $table = $("table").tablesorter({
      theme: 'blue',
                headerTemplate : '{content} {icon}',
                 widthFixed: true,
		widgets: ["zebra", "filter"],
		widgetOptions : {
			filter_external : '.search',
			filter_defaultFilter: { 1 : '~{query}' },
			filter_columnFilters: true,
			filter_placeholder: { search : 'Search...' },
			filter_saveFilters : true,
			filter_reset: '.reset'
		}
    })
    .tablesorterPager(pagerOptions);
	$('button[data-column]').on('click', function(){
		var $this = $(this),
			totalColumns = $table[0].config.columns,
			filter = [];
		filter[ col === 'all' ? totalColumns : col ] = $this.text();
		$table.trigger('search', [ filter ]);
		return false;
	});

});
    </script>
        
 </head>   
 <body class="bg-color">
        
       <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a class="" href="<?php echo asset('admindashboard') ?>"><img src="<?php echo asset('images/logo.png')?>" alt=""></a>
                </li>
                <li>
                    <a href="<?php echo asset('admindashboard') ?>"> 
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo asset('operator_list') ?>"> 
                        <i class="fa fa-user"></i>
                        <span>List Of operators</span>
                    </a>
                </li>
                <?php $user_id = \Illuminate\Support\Facades\Auth::user()->id;
                    if(Auth::user()->role == 1){ ?>
<!--                    <li>
                        <a href="<?php // echo asset('admin_list') ?>"> 
                            <i class="fa fa-user-secret"></i>
                            <span>List Of Admins</span>
                        </a>
                    </li>-->
                    <li class="toggle-block slide">
                        <a href="#"> 
                            <i class="fa fa-pencil-square-o"></i>
                            <span class="opener">Reports</span>
                        </a>
                        <ul class="slide">
                            <li><a href="<?php echo asset('get_search_details') ?>">Search Details</a></li>
                            <li><a href="<?php echo asset('get_search_results') ?>">Search Results</a></li>
                            <li><a href="<?php echo asset('get_revenue_details') ?>">Revenue</a></li>
                        </ul>
                    </li>
                    <?php } ?>
                    <li class="toggle-block slide">
                        <a href="#"> 
                            <i class="fa fa-pencil-square-o"></i>
                            <span class="opener">Admin Account Settings</span>
                        </a>
                        <ul class="slide">
                            <?php $user_id = \Illuminate\Support\Facades\Auth::user()->id;
                    if(Auth::user()->role == 1){ ?>
                            <li><a href="<?php echo asset('admin_list') ?>">List Of Admins</a></li>
                            <li><a href="<?php echo asset('forgetpass') ?>">Account Password</a></li>
                        <li><a href="<?php echo asset('add_admin_view') ?>">Create Admin Account</a></li>

                    <?php } ?>
                        </ul>
                    </li>
                
<!--                <li>
                    <a href="<?php echo asset('forgetpass') ?>"> 
                        <i class="fa fa-user-plus"></i>
                        <span>Accounts</span>
                    </a>
                </li>-->
                <li>
                    <a href="<?php echo asset('adminlogout')?>"> 
                        <i class="fa fa-sign-out"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
