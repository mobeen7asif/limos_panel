 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>List Of Admins</h2>
                                <span>Here is the list of admins</span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div id="demo">
                                <div class="filter-search">
                                    <input class="search" type="search" data-column="1, 2" placeholder="Search By Account Number Or Email">
                                </div>
                                <div class="table-responsive">
                                    <table class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th width="50" class="sorter-false">Sr #</th>
                                            <th>Account Number</th>
                                            <th>Login Email</th>
                                            <th width="150" class="cennter sorter-false">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = 1;
 foreach ($admin as $add):
                                      ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $add->id?></td>
                                        <td><?php echo $add->email?></td>
                                        <td><a href="<?php echo asset('delete_user/'.$add->id) ?> " onclick="return confirm('Are you sure you want to delete this record')"class="link" >Delete</a></td>
                                    </tr>
                                    <?php $i=$i+1;
endforeach;
                                    ?>
                                    </tbody>
                                </table>
                                </div>
                                <div id="pager" class="pager">
                                    <form>
                                        <input type="button" value="&lt;" class="prev" />
                                        <input type="text" class="pagedisplay" readonly/>
                                        <input type="button" value="&gt;" class="next" />
                                        <select class="pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    
    </body>
</html>