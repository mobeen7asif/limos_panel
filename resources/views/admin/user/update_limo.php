<?php require('operator_includes/operator_header.php'); ?>
<div class="col-sm-9 equal_columns">
    <?php if (Session::has('success')) { ?>
        <div class="alert alert-success"><?php echo Session::get('success') ?></div>
    <?php } ?>
    <div id="add_car_form">
        <form id="update_limo_status" method="post" enctype="multipart/form-data" action="<?php echo asset('update_limo_user') ?>">
            <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>">
            <div class="from_head">Booking Type Limit</div>
            <div class="from_section">
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Limo Type<span></span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <label class="limo_label">
                                <select class="limo_type" name="limo_type">
                                    <?php foreach ($types as $type): 
//     print_r($selected_type);exit;
                                        ?>
                                        <option value="<?php echo $type['limo_type_id'] ?>"<?php if ($selected_type[0]->limo_type == $type['limo_type_id']) { ?> selected=""<?php } ?>><?php echo $type['limo_title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="from_head">About Your Limo</div>
            <div class="from_section">
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Name of your vehicle (be fun with it and give it personality)<span>*</span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <input type="text" class="limo_name" name="veh_name" value="<?php echo $limos[0]->veh_name ?>" required>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Vehicle Color<span></span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <label class="limo_label">
                                <select class="limo_type" name="color">
                                        <option value="Black" <?php if($limos[0]->color == 'Black') { ?> selected <?php } ?>>Black</option>
                                        <option value="White" <?php if($limos[0]->color == 'White') { ?> selected <?php } ?>>White</option>
                                        <option value="Blue" <?php if($limos[0]->color == 'Blue') { ?> selected <?php } ?>>Blue</option>
                                        <option value="Brown" <?php if($limos[0]->color == 'Brown') { ?> selected <?php } ?>>Brown</option>
                                        <option value="Gray" <?php if($limos[0]->color == 'Gray') { ?> selected <?php } ?>>Gray</option>
                                        <option value="Green" <?php if($limos[0]->color == 'Green') { ?> selected <?php } ?>>Green</option>
                                        <option value="Orange" <?php if($limos[0]->color == 'Orange') { ?> selected <?php } ?>>Orange</option>
                                        <option value="Pink" <?php if($limos[0]->color == 'Pink') { ?> selected <?php } ?>>Pink</option>
                                        <option value="Purple" <?php if($limos[0]->color == 'Purple') { ?> selected <?php } ?>>Purple</option>
                                        <option value="Red" <?php if($limos[0]->color == 'Red') { ?> selected <?php } ?>>Red</option>
                                        <option value="Yellow" <?php if($limos[0]->color == 'Yellow') { ?> selected <?php } ?>>Yellow</option>
                                        <option value="Gold" <?php if($limos[0]->color == 'Gold') { ?> selected <?php } ?>>Gold</option>
                                        <option value="Silver" <?php if($limos[0]->color == 'Silver') { ?> selected <?php } ?>>Silver</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Description of your vehicle (sell it like you would on the phone)<span>*</span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <textarea class="description" name="veh_des" required><?php echo $limos[0]->veh_des ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Rules</label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <textarea class="rules" name="veh_rules"><?php echo $limos[0]->veh_rules ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="from_head">Amenities</div>
            <div class="from_section">
                <div class="row clearfix">

                    <?php
                    foreach ($aminties as $aminty):
                        $newarray = str_replace(',', '', $limos[0]->amenities);
                        $findamienty = strrchr($newarray, $aminty['amen_title']);
                        ?>
                        <div class="col-md-3 col-xs-6">
                            <div class=" clearfix custom_inputs">
                                <input id="<?php echo $aminty['amen_id'] ?>" type="checkbox" value="<?php echo $aminty['amen_title'] ?>" <?php if ($findamienty == TRUE) { ?> checked="checked" <?php } ?>name="amen_title[]" >
                                <label for="<?php echo $aminty['amen_id'] ?>"><span>&nbsp;</span><div class="inline_block"><?php echo $aminty['amen_title'] ?></div> </label>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
            <div class="from_head">Details</div>
            <div class="from_section">

                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Seats (this refers to maximum number of people allowed)</label>
                    </div>
                    <div class="col-sm-8">
<!--                                <input type="number" placeholder="Passenger" min="1" max="20" name="veh_seats" value="<?php // echo $limos[0]->veh_seats  ?>" id="veh_seats" required>-->
                        <div class="custom_select">
                            <label>
                                <select class="cancellation_policy" name="veh_seats">
                                    <?php for ($i = 1; $i <= 100; $i++) { ?>
                                        <option value="<?php echo $i ?>"  <?php if ($limos[0]->veh_seats == $i) { ?> selected<?php } ?>><?php echo $i ?></option>
                                    <?php } ?>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Hourly Rate Base Rate<span>*</span></label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="price" name="veh_price" value="<?php echo $limos[0]->veh_price ?>" id="veh_price" required>&nbsp;<span id="errmsg" style="color:red"></span>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Minimum hours for rent<span>*</span></label>
                    </div>
                    <div class="col-sm-8">
                        <input required type="text" class="min_rent_hours" name="veh_booking_mintime"value="<?php echo $limos[0]->veh_booking_mintime ?>" id="veh_booking_mintime">&nbsp;<span id="errmsg1" style="color:red"></span>
                    </div>
                </div>
                   <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Cancellation Policy<span>*</span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <textarea required class="rules" name="veh_policy"><?php echo $limos[0]->veh_policy?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Service Area<span>*</span></label>
                    </div>
                    <div class="col-sm-8">
                        <div class="row clearfix">
                            <div class=" col-md-2 custom_inputs">
                                <input id="20miles" type="radio" name="service_area" value="20" <?php if ($limos[0]->service_area == '20') { ?> checked=""<?php } ?> required="">
                                <label for="20miles"><span>&nbsp;</span><div class="inline_block">20 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="30miles" type="radio" name="service_area" value="30" <?php if ($limos[0]->service_area == '30') { ?> checked=""<?php } ?>>
                                <label for="30miles"><span>&nbsp;</span><div class="inline_block">30 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="40miles" type="radio" name="service_area" value="40" <?php if ($limos[0]->service_area == '40') { ?> checked=""<?php } ?>>
                                <label for="40miles"><span>&nbsp;</span><div class="inline_block">40 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="50miles" type="radio" name="service_area" value="50" <?php if ($limos[0]->service_area == '50') { ?> checked=""<?php } ?>>
                                <label for="50miles"><span>&nbsp;</span><div class="inline_block" >50 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="60miles" type="radio" name="service_area" value="60" <?php if ($limos[0]->service_area == '60') { ?> checked=""<?php } ?>>
                                <label for="60miles"><span>&nbsp;</span><div class="inline_block" >60 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="80miles" type="radio" name="service_area" value="80" <?php if ($limos[0]->service_area == '80') { ?> checked=""<?php } ?>>
                                <label for="80miles"><span>&nbsp;</span><div class="inline_block" >80 Miles</div> </label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="from_head">Address</div>
            <div class="from_section">
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Country</label>
                    </div>
                    <div class="col-sm-8">
                        <input required type="text" class="country" name="country_name" readonly="" id="country" value="<?php echo $limos[0]->owner_country ?>">
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>State</label>
                    </div>
                    <div class="col-sm-8" >
                        <input  type="text" class="state" name="owner_state"value="<?php echo $limos[0]->owner_state ?>" id="administrative_area_level_1" readonly="">
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>City</label>
                    </div>
                    <div class="col-sm-8">
                        <input required type="text" class="city" name="owner_city" value="<?php echo $limos[0]->owner_city ?>" id="locality" readonly="">
                        <table id="address" style="display:none;">
                            <tr>
                                <td class="label">Street address</td>
                                <td class="slimField"><input class="field" id="street_number"
                                                             disabled="true"></input></td>
                                <td class="wideField" colspan="2"><input class="field" id="route"
                                                                         disabled="true"></input></td>
                            </tr>
                            <tr>
                                <td class="label">Zip code</td>
                                <td class="wideField"><input class="field" id="postal_code"
                                                             disabled="true"></input></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Address</label>
                    </div>
                    <div class="col-sm-8">
                        <input required type="text" class="address" name="owner_address" value="<?php echo $limos[0]->owner_address ?>" onFocus="geolocate()" id="owner_address_update">
                    </div>
                </div>
            </div>
<!--            <div class="from_head">Photos</div>
            <div class="from_section ">
                <a href="<?php echo asset('gallery/' . $limos[0]->limo_id) ?>"> Click Here For Images</a>
            </div>-->
<div class="from_head">Photos</div>
            <div class="from_section  ">
                <?php foreach ($photos as $photo): ?>
                <div class="gallery_file col-md-4">
                <div class="relv ">
                                <input id="file<?php echo $photo->image_id ?>"  accept=".png, .jpg"  class="<?php echo $photo->image_id ?>" type="file"   name="veh_image" onchange="readURL(this, '<?php echo $photo->image_id ?>')"/>

                                <button class="btn" ><img src="<?php echo asset('images/upload.png')?>"></button>
                                <a href="<?php echo asset('images/limo_image/' . $photo->image_name) ?>" class="btn cancel g_fancybox"><img src="<?php echo asset('images/preview.png')?>"></a>
                                <img id="changeimg<?php echo $photo->image_id ?>" src="<?php echo asset('images/limo_image/' . $photo->image_name) ?>">
                            </div>
                            <div class=" clearfix custom_inputs">
                                <input id="<?php echo asset('images/limo_image/' . $photo->image_id) ?>" type="radio" name="radio" value="<?php echo asset('images/limo_image/' . $photo->image_id) ?>" onclick="makedefault('<?php echo $photo->image_id ?>', '<?php echo $photo->vehicals_id ?>')" <?php if ($photo->main_img == 1) { ?>checked=""<?php } ?>>
                                <label for="<?php echo asset('images/limo_image/' . $photo->image_id) ?>"><span>&nbsp;</span><div class="inline_block">Make Cover Photo</div> </label>
                            </div>
                            <div><span id="error<?php echo $photo->image_id ?>" class="alert danger" style="display:none;"></span></div>
                            <div><span id="success<?php echo $photo->image_id ?>" class="alert success" style="display:none;"></span></div>
                </div>
            
                <?php endforeach ;?>
            </div>
                <div class="clear"></div>
            </div>
            <input type="hidden" value="<?php echo $limos[0]->limo_id ?>" name="limo_id">
            <input style="width:auto;" type="submit" class="fr btn" value="Update Limo">
            <div class="spacer"></div>
            <input type="hidden" value="<?php echo $limos[0]->lat ?>" name="lat" id="lat">
            <input type="hidden" value="<?php echo $limos[0]->lng ?>" name="lng" id="lng">
        </form>

    </div>
</div>
</div>
</div>
</div>
<?php require('operator_includes/operator_footer.php'); ?>
<!--Form validation --> 

<script type="text/javascript">

    $("#veh_price").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $("#veh_price").focusout(function() {
                     if($(this).val() > 500){
                         $("#veh_price").val('');
                       $("#errmsg").html("Max Limit $500").show().fadeOut(5000);
                        return false;  
                     }
                 });
    $("#veh_booking_mintime").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg1").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
    $("#veh_booking_maxtime").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg2").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $(function() {
        country = $('#country_name').val();
        var availableTags = [];
// Get request to get all cities to auto load
        $.ajax({
            type: "GET",
            url: "<?php echo asset('get_selected_city'); ?>",
            data: {"country": country},
            success: function(data) {
                result = JSON.parse(data);
                $.each(result, function(i, item) {
                    availableTags.push(item.city_name)
//                assign city name on autoload
                    availableTags.push(item.city_name)
                });
////                autofill required input
                $("#owner_city").autocomplete({source: availableTags});
//                $("#droploc").autocomplete({source: availableTags});
//                $("#picloc").autocomplete({source: availableTags});
//                $("#searchloc").autocomplete({source: availableTags});
            }
        });
    });
                    
                 $('#owner_address_update').keypress(function(e) {
                if (e.which == 13) {
                e.preventDefault();
                 google.maps.event.addListener(autocomplete, 'place_changed', function() {
                 fillInAddressupdate();
//                setTimeout(function() {
//                    alert('asdasdasdasdads')
//         $('#update_limo_status').submit();
//             }, 5000);
                $('#update_limo_status').submit();
        });
               

        }
            });

function makedefault(id, v_id) {
    $("#updatecoverloader").fadeIn();
        $.ajax({
            type: "GET",
            url: "<?php echo asset('makeimage_default'); ?>",
            data: {"image_id": id, "vehicals_id": v_id},
            success: function(data) {
                $('#success' + id).show().fadeOut(5000);
               $('#success' + id).text('Successfully Upadted');
              $("#updatecoverloader").fadeOut(1000);  
            }});
    }


    function readURL(input, id) {
        file = input.files[0];
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(file);
        reader.onload = function(_file) {
            image.src = _file.target.result;              // url.createObjectURL(file);
            image.onload = function() {
                var w = this.width,
                        h = this.height,
                        s = ~~(file.size / 1024) + 'KB';
                        if(s > 2048 ){
                          $("#file" + id).val("");
                    $('#error' + id).show().fadeOut(5000);
                    $('#error' + id).text('Image size should less than 2mb')  
                            }         
            else {
//                              alert('asdasasda')
                    formData = new FormData();
                    formData.append("veh_image", file);
                    formData.append("imageid", id);
                    $("#imageloader").fadeIn();
                    $.ajax({
                        url: "<?php echo asset('upload_image'); ?>",
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data) {
                                $('#changeimg' + id).attr('src', image.src);
                                $('#error' + id).hide();
                                $("#file" + id).val("");
                                $('#success' + id).show().fadeOut(5000);
                                $('#success' + id).text('Successfully Upadted');
                                $("#imageloader").fadeOut(1000);
                            } else {
                                $("#file" + id).val("");
                                $('#error' + id).show().fadeOut(5000);
                                $('#error' + id).text('Sorry Some Thing Went Wrong');
                                $("#imageloader").fadeOut(1000);
                            }
                        }
                    });

                }
            };
            image.onerror = function() {

                $("#file" + id).val("");
                $('#error' + id).show().fadeOut(5000);
                $('#error' + id).text('Invalid file type: ' + file.type)

            };
        };
    }


</script>
<div id="imageloader" style="display: none;"></div>
<div id="updatecoverloader" style="display: none;"></div>
</body>
</html>