<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
  ================================================== -->
        <meta charset="utf-8">
        <!--Hide   Copy Code-->
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
        <title>InstaLimo| Home</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
  ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
  ================================================== -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/theme.blue.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/base.css') ?>">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        
        <style>
            .pagedisplay{
                width: auto !important;
                display: inline-block !important;
                height: auto !important;
            }
        </style>
        <script>
            function loadCSS(e, t, n) {
                "use strict";
                var i = window.document.createElement("link");
                var o = t || window.document.getElementsByTagName("script")[0];
                i.rel = "stylesheet";
                i.href = e;
                i.media = "only x";
                o.parentNode.insertBefore(i, o);
                setTimeout(function() {
                    i.media = n || "all"
                })
            }

        </script>

        <script>
            loadCSS("<?php echo asset('css/jquery.datepick.css') ?>");
            loadCSS("<?php echo asset('css/fancybox.css') ?>");
            loadCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700");
        </script>
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="<?php echo asset('images/favicon.ico') ?>">
        <link rel="apple-touch-icon" href="<?php echo asset('images/apple-touch-icon.png') ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset('images/apple-touch-icon-72x72.png') ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('images/apple-touch-icon-114x114.png') ?>">
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
        
    </head>
     <?php 
 $id = \Illuminate\Support\Facades\Session::get('user_id');
 $user = \App\User::find($id);
    ?>
    <body onload="initialize()">
        <?php require('googlemaps.php'); ?>
        <div id="dasboard">
            <header id="header" class="relv">
                <div class="logo_navbar clearfix">
                    <div class="container-fluid">
                        <a id="logo" href="<?php echo asset('viewdashboard/'.$id) ?>"><img src="<?php echo asset('images/logo.png') ?>" alt="instalimo" height="38" width="216"></a>
                        <a class="menu_trigger" href="#"><img src="<?php echo asset('images/menu.png') ?>" alt=""></a>
                        <nav id="main_nav">
                            <ul>
                                <li><a href="<?php echo asset('viewdashboard/'.$id)?>">Operator Dashboard</a></li>
                                <li><a href="<?php echo asset('admindashboard') ?>">ADMIN DASHBOARD</a></li>
                                <!--<li><a href="#">BLOG</a></li>-->
                                
                                <li><a href="<?php echo asset('adminlogout')?>">LOGOUT</a></li>
                                <!--<li><a href="<?php // echo asset('viewdashboard/'.$id) ?>">List Your Limo !</a></li>-->
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
            <div class="spacer"></div>
            
            <div class="spacer"></div>
            <div id="dasboard_main" >
                <div class="clearfix">
                    <div class="col-sm-3 equal_columns">
                        <h3 class="col-xs-12 nav_toggle" >Menu</h3>
                        <div class="clear"></div>
                        <div id="sidebar_dasboard">
                            <div class="side_scroll"></div>
                            <ul class="side_menus clearfix">
                                <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'viewdashboard'){ ?>active"<?php } ?>>
                                    <span class="blue_slide"></span>
                                    <a href="<?php echo asset('viewdashboard/'.$id) ?>">
                                        <div class="small_head">Create/Edit</div>
                                        <div class="h2 blue_head">VEHICLES</div>
                                        <?php if($check_limos) { ?>
                                        <span class="status_links"><img src="<?php echo asset('images/done.png')?>"></span>
                                        <?php } else { ?>
                                        <span class="status_links"><img src="<?php echo asset('images/disabled.png')?>"></span>
                                        <?php } ?>
                                        
                                    </a>
                                </li>
                                <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'user_company_profile'){ ?>active"<?php } ?>>
                                    <span class="blue_slide"></span>
                                    <a href="<?php echo asset('user_company_profile') ?>">
                                        <div class="small_head">ADD ABOUT THE COMPANY</div>
                                        <div class="h2 blue_head">COMPANY PROFILE</div>
                                        <?php if($company) { ?>
                                        <span class="status_links"><img src="<?php echo asset('images/done.png')?>"></span>
                                        <?php } else { ?>
                                        <span class="status_links"><img src="<?php echo asset('images/disabled.png')?>"></span>
                                        <?php } ?>
                                    </a>
                                </li>
                               <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'user_billing_address'){ ?>active"<?php } ?>>
                                    <span class="blue_slide"></span>
                                    <a href="<?php echo asset('user_billing_address') ?>">
                                        <div class="small_head">Card Details</div>
                                        <div class="h2 blue_head">BILLING</div>
                                    </a>
                                     <?php if($account && $account[0]->total_amount > 25 && 
                                             $user->stripe_active == 1) { ?>
                                        <span class="status_links"><img src="<?php echo asset('images/done.png')?>"></span>
                                        <?php } else { ?>
                                        <span class="status_links"><img src="<?php echo asset('images/disabled.png')?>"></span>
                                        <?php } ?>
                                </li>
                                <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'user_account_info'){ ?>active"<?php } ?>>
                                    <span class="blue_slide"></span>
                                    <a href="<?php echo asset('user_account_info') ?>">
                                        <div class="small_head">KNOW ABOUT ACCOUNT</div>
                                        <div class="h2 blue_head">ACCOUNT PASSWORD</div>
                                           
                                    </a>
                                </li>
                                
                                <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'user_leads_recieved'){ ?>active"<?php } ?>>
                                    <span class="blue_slide"></span>
                                    <a href="<?php echo asset('user_leads_recieved') ?>">
                                        <div class="small_head">KNow ABout Leads</div>
                                        <div class="h2 blue_head">LEADS RECEIVED</div>
                                    </a>
                                </li>
                                <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'user_inbox'){ ?>active"<?php } ?>>
                                	<span class="blue_slide"></span>
                                    <a href="<?php echo asset('user_inbox') ?>">
                                        <div class="small_head">Received Messages</div>
                                        <div class="h2 blue_head">INBOX</div>
                                    </a>
                                </li>
                            </ul>
                            <ul class="side_menus clearfix logout">
                               <li class="<?php if(\Illuminate\Support\Facades\Request::segment(1) == 'adminlogout'){ ?>active"<?php } ?>>
                                    <span class="blue_slide"></span>

                                    <div class="small_head">Done for now?</div>
                                    <div class="h2 blue_head"><a href="<?php echo asset('adminlogout') ?>">Logout</a></div>

                                </li>
                            </ul>
                        </div>
                    </div>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script  src="<?php echo asset('js/jquery-ui.multidatespicker.js') ?>"></script>
<script  src="<?php echo asset('js/prettify.js') ?>"></script>
<script  src="<?php echo asset('js/lang-css.js') ?>"></script>
<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/prettify.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/pepper-ginder-custom.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo asset('css/mdp.css') ?>">