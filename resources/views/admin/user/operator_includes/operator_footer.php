<footer id="footer">
	<div class="container-fluid">
    	<div class="row clearfix">
    		<div class="col-sm-5 ft_section"><img src="<?php echo asset('images/logo.png')?>" alt=""></div>
	        <div class="col-sm-7 ft_section text-right_sm">
            	<div class="ft_social">
                    <a href="#"><img src="<?php // echo asset('images/fb.png')?>" alt=""></a>
                	<a href="#"><img src="<?php // echo asset('images/twitter.png')?>" alt=""></a>
               		 <a href="#"><img src="<?php // echo asset('images/rss.png')?>" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
                <!--<div>Designed by: <a target="_blank" href="http://vengile.com/">Vengile IT Solution</a></div>-->
            </div>
        </div>
  </div>
</footer>
</div>
    <script src="<?php echo asset('js/bx.js')?>"></script>
    <script defer  src="<?php echo asset('js/jquery.plugin.js')?>"></script>
    <script defer  src="<?php echo asset('js/jquery.datepick.js')?>"></script>
    
    <script defer  src="<?php echo asset('js/jquery.fancybox.pack.js')?>"></script>
    <script defer  src="<?php echo asset('js/jquery.nouislider.all.min.js')?>"></script>
    <script src="<?php echo asset('js/validate.js') ?>"></script>
    <script defer  src="<?php echo asset('js/custom.js')?>"></script>

    <script defer src="<?php echo asset('js/fileinput.js')?>" type="text/javascript"></script>
    <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
 <!--Tablesorter: required--> 
        <!--<script  src="<?php // echo asset('css/admin/js/jquery-latest.min.js') ?>"></script>-->
        
        <script  src="<?php echo asset('css/admin/js/jquery.tablesorter.js') ?>"></script>
        <script src="<?php echo asset('css/admin/js/widget-filter.js') ?>"></script>
        <script  src="<?php echo asset('css/admin/js/jquery.tablesorter.pager.js') ?>"></script>
        
        
