<?php require('operator_includes/operator_header.php'); ?>
<div class="col-sm-9 equal_columns">
    <?php if (Session::has('success')) { ?>
        <div class="alert alert-success"><?php echo Session::get('success') ?></div>
    <?php } ?>                                                     <?php if (Session::has('error')) { ?>
        <div class="alert danger"><?php echo Session::get('error') ?></div>
    <?php } ?>
    <div id="add_car_form">
        <form id="add_limo" method="post" enctype="multipart/form-data" action="<?php echo asset('user_add_limos') ?>" >
            <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>">
            <div class="from_head">Booking Type</div>
            <div class="from_section">
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Limo Type<span></span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <label class="limo_label">
                                <select class="limo_type" name="limo_type">
                                    <?php foreach ($types as $type): ?>
                                        <option value="<?php echo $type['limo_type_id'] ?>"><?php echo $type['limo_title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="from_head">About Your Limo</div>
            <div class="from_section">
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Name of your vehicle (be fun with it and give it personality)<span>*</span></label>
                    </div>
                    <div class="col-sm-8 ">
                       
                            <input type="text" class="limo_name" name="veh_name">
                       
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Vehicle Color<span></span></label>
                    </div>
                    <div class="col-sm-8 ">
                        <div class="custom_select">
                            <label class="limo_label">
                                <select class="limo_type" name="color">
                                        <option value="Black">Black</option>
                                        <!--<option value="">White</option>-->
                                        <option value="Blue">Blue</option>
                                        <option value="Brown">Brown</option>
                                        <option value="Gray">Gray</option>
                                        <option value="Green">Green</option>
                                        <option value="Orange">Orange</option>
                                        <option value="Pink">Pink</option>
                                        <option value="Purple">Purple</option>
                                        <option value="Red">Red</option>
                                        <option value="White">White</option>
                                        <option value="Yellow">Yellow</option>
                                        <option value="Gold">Gold</option>
                                        <option value="Silver">Silver</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Description of your vehicle (sell it like you would on the phone)<span>*</span></label>
                    </div>
                    <div class="col-sm-8 ">
                        
                            <textarea class="description" name="veh_des"></textarea>
                        
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Rules</label>
                    </div>
                    <div class="col-sm-8 ">
                       
                            <textarea class="rules" name="veh_rules"></textarea>
                       
                    </div>
                </div>
            </div>
            <div class="from_head">Amenities</div>
            <div class="from_section">
                <div class="row clearfix">
                    <?php foreach ($aminties as $aminty):
                        ?>
                        <div class="col-md-3 col-xs-6">
                            <div class=" clearfix custom_inputs">
                                <input id="<?php echo $aminty['amen_id'] ?>" type="checkbox" value="<?php echo $aminty['amen_title'] ?>" name="amen_title[]" >
                                <label for="<?php echo $aminty['amen_id'] ?>"><span>&nbsp;</span><div class="inline_block"><?php echo $aminty['amen_title'] ?></div> </label>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="from_head">Details</div>
            <div class="from_section">
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Seats (this refers to maximum number of people allowed)</label>
                    </div>
                    <div class="col-sm-8">
                        <!--<input type="number" min="1" max="20" name="veh_seats" required>-->
                        <div class="custom_select">
                            <label>
                                <select class="cancellation_policy" name="veh_seats">
                                    <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option><option value="100">100</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Hourly Rate Base Rate<span>*</span></label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="price" name="veh_price" id="veh_price">&nbsp;<span id="errmsg" style="color:red"></span>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Minimum hours for rent<span>*</span></label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="max_rent_hours" name="veh_booking_mintime" id="veh_booking_mintime">&nbsp;<span id="errmsg1" style="color:red"></span>
                    </div>
                </div>
                
                <div class="row clearfix">
                    <div class="col-sm-4 ">
                        <label>Cancellation Policy<span>*</span></label>
                    </div>
                    <div class="col-sm-8 ">
                        
                            <textarea class="rules" name="veh_policy"></textarea>
                        
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Service Area<span>*</span></label>
                    </div>
                    <div class="col-sm-8">
                        <div class="row clearfix checkbox">
                            <div class=" col-md-2 custom_inputs">
                                <input id="20miles" type="radio" name="service_area" value="20" required class="checkbox">
                                <label for="20miles"><span>&nbsp;</span><div class="inline_block">20 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="30miles" type="radio" name="service_area" value="30" required class="checkbox">
                                <label for="30miles"><span>&nbsp;</span><div class="inline_block">30 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="40miles" type="radio" name="service_area" value="40" required class="checkbox">
                                <label for="40miles"><span>&nbsp;</span><div class="inline_block">40 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="50miles" type="radio" name="service_area" value="50" required class="checkbox">
                                <label for="50miles"><span>&nbsp;</span><div class="inline_block">50 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="60miles" type="radio" name="service_area" value="60" required class="checkbox">
                                <label for="60miles"><span>&nbsp;</span><div class="inline_block" >60 Miles</div> </label>
                            </div>
                            <div class=" col-md-2 custom_inputs">
                                <input id="80miles" type="radio" name="service_area" value="80" required class="checkbox">
                                <label for="80miles"><span>&nbsp;</span><div class="inline_block">80 Miles</div> </label>
                            </div>
                        </div>
                        <span class="alert danger" id="checkboxerror" style="display: none;">Please Select One</span>
                        
                    </div>
                </div>
            </div>
            <div class="from_head">Address</div>
            <div class="from_section">
                  <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Address</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="address" name="owner_address"required="" id="owner_location" onFocus="user_geolocate()">
                    </div>
                </div>
<div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Country</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="country" name="owner_country" readonly="" id="country" required placeholder="Will be Autofilled Please Enter Location">
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>State</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="state" name="owner_state" readonly="" id="administrative_area_level_1" required placeholder="Will be Autofilled Please Enter Location">
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>City</label>
                    </div>
                    <div class="col-sm-8">
                        <table id="address" style="display:none;">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField"><input class="field" id="street_number"
              disabled="true"></input></td>
        <td class="wideField" colspan="2"><input class="field" id="route"
              disabled="true"></input></td>
      </tr>
      <tr>
        <td class="label">Zip code</td>
        <td class="wideField"><input class="field" id="postal_code"
              disabled="true"></input></td>
      </tr>
    </table>
                        <input type="text" class="city" name="owner_city" id="locality"  required placeholder="Will be Autofilled Please Enter Location" readonly="">
                    </div>
                </div>
              
            </div>
            <div class="from_head">Photos</div>
            <div class="from_section ">
                <div class="col-md-4  gallery_file">
                    <div class="relv">
                    <input accept=".png, .jpg"  class="file" type="file"   name="attachments[]" id="firstimage" />
                    <img id="changeimg1" src="<?php echo asset('no-image.jpg') ?>">
                    </div>
                    <div ><span id="errormsg1" class="alert danger" style="display:none;"></span></div>
                </div>
                <div class="col-md-4  gallery_file">
                     <div class="relv">
                    <input  accept=".png, .jpg"  class="file" type="file"   name="attachments[]" id="secimage"/>
                    <img id="changeimg2" src="<?php echo asset('no-image.jpg') ?>">
                     </div>
                    <div><span id="errormsg2" class="alert danger" style="display:none;"></span></div>
                </div>
                <div class="col-md-4  gallery_file">
                     <div class="relv">
                    <input   accept=".png, .jpg"  class="file" type="file"   name="attachments[]" id="thirldimage"/>
                    <img id="changeimg3" src="<?php echo asset('no-image.jpg') ?>">
                     </div>
                    <div><span id="errormsg3" class="alert danger" style="display:none;"></span></div>
                </div>
                <div class="clear"></div>
            </div>
<input type="hidden" value="" name="lat" id="lat">
            <input type="hidden" value="" name="lng" id="lng">
            <!--<a  class="fr btn" href="#" onclick="submitform()">Add Limo</a>-->
            <input style="width:auto;" type="submit" class="fr btn" value="Add Limo">
            <div class="spacer"></div>



                    <!--<input type="submit" value="ADD LIMOS" class="fr btn" id="1" />--> 
        </form>
        <div id="uploadPreview"></div>
    </div>
</div>
</div>
</div>
</div>

</div>
<footer id="footer">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-5 ft_section"><img src="images/logo.png" alt=""></div>
            <div class="col-sm-7 ft_section text-right_sm">
                <div class="ft_social">
                    <a href="#"><img src="images/fb.png" alt=""></a>
                    <a href="#"><img src="images/twitter.png" alt=""></a>
                    <a href="#"><img src="images/rss.png" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
                <!--<div>Designed by: <a target="_blank" href="http://vengile.com/">Vengile IT Solution</a></div>-->
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo asset('js/bx.js') ?>"></script>
<script defer  src="<?php echo asset('js/jquery.plugin.js') ?>"></script>
<script defer  src="<?php echo asset('js/jquery.datepick.js') ?>"></script>
<script defer  src="<?php echo asset('js/jquery.fancybox.pack.js') ?>"></script>
<script defer  src="<?php echo asset('js/jquery.nouislider.all.min.js') ?>"></script>
<script  src="<?php echo asset('js/validate.js') ?>"></script>
<script defer  src="<?php echo asset('js/custom.js') ?>"></script>


        <style>
            input.error {
                border:solid 1px red !important;
            }
            textarea.error {
                border:solid 1px red !important;
            }
            
            #add_limo label.error {
                width: auto;
                display: inline;
                color:red;
                font-size: 16px;
            }
        </style>

<script type="text/javascript">
                $("#veh_price").keypress(function(e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        //display error message
                        $("#errmsg").html("Digits Only").show().fadeOut("slow");
                        return false;
                    }
                });
                 $("#veh_price").focusout(function() {
                     if($(this).val() > 500){
                         $("#veh_price").val('');
                       $("#errmsg").html("Max Limit $500").show().fadeOut(5000);
                        return false;  
                     }
                 });
                $("#veh_booking_mintime").keypress(function(e) {
                    //if the letter is not digit then display error and don't type anything
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        //display error message
                        $("#errmsg1").html("Digits Only").show().fadeOut("slow");
                        return false;
                    }
                });
                $("#veh_booking_maxtime").keypress(function(e) {
                    //if the letter is not digit then display error and don't type anything
                    
                    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        //display error message
                        $("#errmsg2").html("Digits Only").show().fadeOut("slow");
                        return false;
                    }
                });
                 $(document).ready(function() {
                $('#add_limo').validate({
                       rules: {
                        owner_address: "required",
                        veh_policy:"required",
                        veh_name: "required",
                        veh_des: "required",
//                        veh_rules: "required",
                        veh_price: "required",
                        veh_booking_mintime: "required",
                        owner_country: "required",
                        owner_state: "required",
                        owner_city: "required"

                    },
                    messages: {
                        owner_address: "",
                        veh_policy:"",
                        veh_name: "",
                        veh_des: "",
//                        veh_rules: "",
                        veh_price: "",
                        veh_booking_mintime: "",
                        owner_country: "*",
                        owner_state: "*",
                        owner_city: "*"
                    }
                    
                });
                    });
                $('#add_limo').submit(function(e){
                    
                    if ($('.limo_type').val() == '') {
                        e.preventDefault();
                        $('.limo_label').css("border", "1px solid red");
                    }
//                    else if ($('.limo_name').val() == '') {
//                        e.preventDefault();
//                        $('.limo_label').css("border", "1px solid black");
////                        $('.limo_name').css("border", "1px solid red");
//                    }
                    else if ($('.description').val() == '') {
                        e.preventDefault();
                        $('.limo_label').css("border", "1px solid black");
//                        $('.limo_name').css("border", "1px solid black");
                        $('.description').css("border", "1px solid red");
                    }
//                    else if ($('.rules').val() == '') {
//                        e.preventDefault();
//                        $('.limo_label').css("border", "1px solid black");
//                        $('.limo_name').css("border", "1px solid black");
//                        $('.description').css("border", "1px solid black");
//                        $('.rules').css("border", "1px solid red");
//                    }
                    else if ($('.min_rent_hours').val() == '') {
                        e.preventDefault();
                        $('.limo_label').css("border", "1px solid black");
                        $('.limo_name').css("border", "1px solid black");
                        $('.description').css("border", "1px solid black");
                        $('.rules').css("border", "1px solid black");
                        $('.min_rent_hours').css("border", "1px solid red");
                    }
                    else if ($('.max_rent_hours').val() == '') {
                        e.preventDefault();
                        $('.limo_label').css("border", "1px solid black");
                        $('.limo_name').css("border", "1px solid black");
                        $('.description').css("border", "1px solid black");
                        $('.rules').css("border", "1px solid black");
                        $('.min_rent_hours').css("border", "1px solid black");
                        $('.max_rent_hours').css("border", "1px solid red");
                    }
                    else if ($('.cancellation_policy').val() == '') {
                        e.preventDefault();
                        $('.limo_label').css("border", "1px solid black");
                        $('.limo_name').css("border", "1px solid black");
                        $('.description').css("border", "1px solid black");
                        $('.rules').css("border", "1px solid black");
                        $('.min_rent_hours').css("border", "1px solid black");
                        $('.max_rent_hours').css("border", "1px solid black");
                        $('.cancellation_policy').css("border", "1px solid red");
                    }
                    else if(($('input[name=service_area]:checked').length === 0)){ 
                        e.preventDefault();
                        $('.limo_label').css("border", "1px solid black");
                        $('.limo_name').css("border", "1px solid black");
                        $('.description').css("border", "1px solid black");
                        $('.rules').css("border", "1px solid black");
                        $('.min_rent_hours').css("border", "1px solid black");
                        $('.max_rent_hours').css("border", "1px solid black");
                        $('#checkboxerror').show();
                    }
                    else if ($("#firstimage").val() == '') {
                        e.preventDefault();
                        $('#changeimg1').css("border", "1px solid red");
                         $('#checkboxerror').hide();
                    }
                    else if ($("#secimage").val() == '') {
                        e.preventDefault();
                        $('#changeimg1').css("border", "1px solid black");
                        $('#changeimg2').css("border", "1px solid red");
                         $('#checkboxerror').hide();
                    }
                    else if ($("#thirldimage").val() == '') {
                        e.preventDefault();
                        $('#changeimg1').css("border", "1px solid black");
                        $('#changeimg2').css("border", "1px solid black");
                        $('#changeimg3').css("border", "1px solid red");
                         $('#checkboxerror').hide();
                    }
                    else {
                        $('.limo_label').css("border", "1px solid black");
                        $('.limo_name').css("border", "1px solid black");
                        $('.description').css("border", "1px solid black");
                        $('.rules').css("border", "1px solid black");
                        $('.min_rent_hours').css("border", "1px solid black");
                        $('.max_rent_hours').css("border", "1px solid black");
                        $('.cancellation_policy').css("border", "1px solid black");
                        $('#changeimg3').css("border", "1px solid black");
                         $('#checkboxerror').hide();
//                        $('#add_limo').submit();
                    }
                });
// For first image

                $("#firstimage").change(function(e) {
                    if (this.disabled)
                        return alert('File upload not supported!');
                    var F = this.files;
                    if (F && F[0])
                        for (var i = 0; i < F.length; i++)
                            readImage(F[i]);
                });
                function readImage(file) {
                    var reader = new FileReader();
                    var image = new Image();
                    reader.readAsDataURL(file);
                    reader.onload = function(_file) {
                        image.src = _file.target.result;              // url.createObjectURL(file);
                        image.onload = function() {
                            var w = this.width,
                                    h = this.height,
                            t = file.type, // ext only: // file.type.split('/')[1],
                                    n = file.name,
                                    s = ~~(file.size / 1024);
                            
                            if(s > 2048 ){
                              $('#changeimg1').attr('src', '<?php echo asset('no-image.jpg') ?>');
                                $("#firstimage").val("");
                                $('#errormsg1').show();
                                $('#errormsg1').text('Image size should less than 2mb')  
                            }
                            else {
                               
                                $('#changeimg1').attr('src', image.src);
                                $('#errormsg1').hide();
                            }
                        };
                        image.onerror = function() {
                        $("#firstimage").val("");
                        $('#errormsg1').show();
                        $('#errormsg1').text('Invalid file type: ' + file.type)
                           
                        };
                    };

                }

// for second iamge
                $("#secimage").change(function(e) {
                    if (this.disabled)
                        return alert('File upload not supported!');
                    var F = this.files;
                    if (F && F[0])
                        for (var i = 0; i < F.length; i++)
                            readImage1(F[i]);
                });
                function readImage1(file) {
                    var reader = new FileReader();
                    var image = new Image();
                    reader.readAsDataURL(file);
                    reader.onload = function(_file) {
                        image.src = _file.target.result;              // url.createObjectURL(file);
                        image.onload = function() {
                            var w = this.width,
                                    h = this.height,
                            t = file.type, // ext only: // file.type.split('/')[1],
                                    n = file.name,
                                    s = ~~(file.size / 1024);
                            if(s > 2048 ){
                              $('#changeimg2').attr('src', '<?php echo asset('no-image.jpg') ?>');
                                $("#firstimage").val("");
                                $('#errormsg2').show();
                                $('#errormsg2').text('Image size should less than 2mb')  
                            }
                            else {
                                $('#changeimg2').attr('src', image.src);
                                $('#errormsg2').hide();
                            }
                        };
                        image.onerror = function() {
                        $("#secimage").val("");
                        $('#errormsg2').show();
                        $('#errormsg2').text('Invalid file type: ' + file.type)
                        };
                    };

                }
                
                // for thirld iamge
                $("#thirldimage").change(function(e) {
                    if (this.disabled)
                        return alert('File upload not supported!');
                    var F = this.files;
                    if (F && F[0])
                        for (var i = 0; i < F.length; i++)
                            readImage2(F[i]);
                });
                function readImage2(file) {
                    var reader = new FileReader();
                    var image = new Image();
                    reader.readAsDataURL(file);
                    reader.onload = function(_file) {
                        image.src = _file.target.result;              // url.createObjectURL(file);
                        image.onload = function() {
                            var w = this.width,
                                    h = this.height,
                            t = file.type, // ext only: // file.type.split('/')[1],
                                    n = file.name,
                                    s = ~~(file.size / 1024);
                        if(s > 2048 ){
                              $('#changeimg3').attr('src', '<?php echo asset('no-image.jpg') ?>');
                                $("#firstimage").val("");
                                $('#errormsg3').show();
                                $('#errormsg3').text('Image size should less than 2mb')  
                            }
                            else {
                                $('#changeimg3').attr('src', image.src);
                                $('#errormsg3').hide();
                            }
                        };
                        image.onerror = function() {
                          $("#thirldimage").val("");
                           $('#errormsg3').show();
                           $('#errormsg3').text('Invalid file type: ' + file.type)
                        };
                    };

                }
                
                 $('#owner_address').keypress(function(e) {
                if (e.which == 13) {
                e.preventDefault();
                 google.maps.event.addListener(autocomplete, 'place_changed', function() {
                 fillInAddress();
                $('#add_limo').submit();
        });
               

        }
            });

</script>
</body>
</html>

    