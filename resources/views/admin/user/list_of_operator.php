 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('logout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>List Of Operator's</h2>
                                <span>Here is the list of operator's</span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div class="table-responsive all_list">
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th width="50">Sr #</th>
                                      <th>Company Name</th>
                                      <th>Account Number</th>
                                      <th>Login Email</th>
                                      <th>Last Name</th>
                                      <th>Deactivate</th>
                                      <th width="150" class="cennter">Detail</th>
                                      <th width="150" class="cennter">Delete</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      
                                      <?php $i=1;
                                      foreach ($users as $user):?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php if($user->comp_name) { echo $user->comp_name ;} else { ?>No Company Name<?php } ?></td>
                                        <td><?php echo $user->comp_id?></td>
                                        <td><?php echo $user->email?></td>
                                        <td><?php echo $user->comp_l_name?></td>
                                        <td><input type="checkbox" id="status_<?php echo $user->id?>" onclick="changeuserstatus('<?php echo $user->id?>')" name="status_<?php echo $user->id?>" <?php if($user->deactive_account == 1){ ?> checked<?php } ?>></td>
                                        <td><a href="<?php echo asset('detail/'.$user->id) ?>" class="detail_link">View Detail</a></td>
                                        <td><a href="<?php echo asset('delete_user/'.$user->id) ?> " onclick="return confirm('Are you sure you want to delete this record')"class="link" >Delete</a></td>
                                    </tr>
                                    <?php $i=$i+1; endforeach;?>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    function changeuserstatus(user_id){
    if( $('#status_'+user_id).is(':checked')) {
            $.ajax({
            type: "GET",
            url: "<?php echo asset('update_user_status'); ?>",
            data:{"user_id":user_id,"status":"1"},
            success: function(data) {
            }});
    }else{
        $.ajax({
            type: "GET",
            url: "<?php echo asset('update_user_status'); ?>",
            data:{"user_id":user_id,"status":"0"},
            success: function(data) {
            }}); 
    }
}
    </script>
    
    </body>
</html>