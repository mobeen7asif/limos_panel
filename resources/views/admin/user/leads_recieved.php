<?php require('operator_includes/operator_header.php'); ?>
        <div class="col-sm-9 equal_columns">
        	<div class="dashboard_header clearfix">
            	<div class="from_head fl">LEADS RECEIVED</div>
                
            </div>
            <table id="example" class="stripe" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Event type</th>
                <th>Contact</th>
                <th>Searched Date</th>
                <th>Service Date</th>
                <th>Passengers</th>
                 <th>Pick up address</th>      
            </tr>
        </thead>
       
        <tbody>
            <?php foreach ($leads as $lead): 
                $contact = substr($lead->contact,0,0)."(".substr($lead->contact,0,3).") ".substr($lead->contact,3,3)."-".substr($lead->contact, 6); 
                ?>
            <tr>
                <td><?php echo $lead->firstname. ' '.$lead->lastname?></td>
                <td><?php echo $lead->occian_type ?></td>
                <td><?php if($lead->contact) {echo $contact;}else{echo 'N/A';}?></td>
                <td><?php echo $lead->created_at?></td>
                <td><?php echo $lead->search_date?></td>
                <td><?php echo $lead->passengers?></td>
                <td><?php echo $lead->picklocation?></td>
            </tr>
            <?php endforeach;?>
        </tbody> 
    </table>
        </div>
    </div>
</div>
</div>


<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 4, "desc" ]]
    } );
} );
</script>
<?php // require('operator_includes/operator_footer.php'); ?>
    
</body>
</html>