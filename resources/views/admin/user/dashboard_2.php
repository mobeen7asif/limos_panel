 <?php require('include/header.php');
 ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>Dashboard</h2>
                                <span>Welcome back, Admin</span>
                            </div>
                             <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <a href="<?php echo asset('operators')?>">
                                                <div class="circle-icon">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                                <div>
                                                    <h3 class="no-margin"><?php echo $totaluser?></h3>
                                                    <span>Total Accounts</span>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <a href="<?php echo asset('getactive')?>">
                                                <div class="circle-icon">
                                                    <i class="fa fa-check"></i>
                                                </div>
                                                <div>
                                                    <h3 class="no-margin"><?php echo $active?></h3>
                                                    <span>Active Accounts</span>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                                 <div class="col-md-3 col-sm-6 col-xs-12">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <a href=<?php echo asset('getcanceled')?>>
                                                <div class="circle-icon">
                                                    <i class="fa fa-times"></i>
                                                </div>
                                                <div>
                                                    <h3 class="no-margin"><?php echo $deactive?></h3>
                                                    <span>Cancelled Accounts</span>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <a href="<?php echo asset('getlowbalance')?>">
                                                <div class="circle-icon">
                                                    <i class="fa fa-usd"></i>
                                                </div>
                                                <div>
                                                    <h3 class="no-margin"><?php echo $lowblance?></h3>
                                                    <span>Insufficient Funds Accounts</span>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <a href="<?php echo asset('getpending')?>">
                                                <div class="circle-icon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                                <div>
                                                    <h3 class="no-margin"><?php echo $pending?></h3>
                                                    <span>Accounts Pending</span>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <section class="panel">
                                        <div class="panel-body">
                                            <a href="<?php echo asset('getqueed')?>">
                                                <div class="circle-icon">
                                                    <i class="fa fa-download"></i>
                                                </div>
                                                <div>
                                                    <h3 class="no-margin"><?php echo $inactive?></h3>
                                                    <span>Accounts in Queue</span>
                                                </div>
                                            </a>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="search_account">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 spacer">
                                        <div class="account_holder">
                                            <div class="account_title">Search Account</div>
                                            <div class="account_contant">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Account Number</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="text" class="form-control" placeholder="Search Account Numner">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Account Email</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="text" class="form-control" placeholder="Search Account Email">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Account Name</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="text" class="form-control" placeholder="Search Account Name">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-search"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    
//                                    print_r(Illuminate\Support\Facades\Auth::user());exit;
                                    if(Illuminate\Support\Facades\Auth::user()->role == 1 ) {?>
                                    <div class="col-lg-6 col-md-6 spacer">
                                        <div class="account_holder">
                                            <div class="account_title">Create Admin Account</div>
                                            <form method="post" action="<?php echo asset('createadmin')?>" id="createadmin"> 
                                                <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                            <div class="account_contant">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Email Login</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input id="email" name="email" type="email" class="form-control" placeholder="example@example.com" autocomplete="off">
                                                                <span id="errormessage" class="alert alert-danger" style="display: none">Email is Already With Us</span>
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Password</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="password" class="form-control" placeholder="Enter Password" name="password">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-key"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-3">
                                                            <div class="input-group ">
                                                                <input id="submitadmin" type="submit" value="Create" class="btn custom_btn">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="search_table">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="demo">
                                <div class="table-responsive">
                                    <table class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th>Company Name</th>
                                            <th>Account Number</th>
                                            <th>Login E-mail</th>
                                            <th>Last Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($users as $user) { ?>
                                        <tr>
                                            <td><?php echo $user->comp_name ?></td>
                                            <td><?php echo $user->comp_id ?></td>
                                            <td><?php echo $user->comp_email ?></td>
                                            <td><?php echo $user->comp_l_name ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                </div>
                                <div id="pager" class="pager">
                                    <form>
                                        <input type="button" value="&lt;" class="prev" />
                                        <input type="text" class="pagedisplay" readonly/>
                                        <input type="button" value="&gt;" class="next" />
                                        <select class="pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <!--<script  src="http://code.jquery.com/jquery-1.10.2.js"></script>-->
    <script  src="<?php echo asset('js/validate.js') ?>"></script>
       <style>
            input.error {
                border:solid 1px red !important;
            }
            #createadmin label.error {
                width: auto;
                display: inline;
                color:red;
                font-size: 16px;
                float:right;
            }
        </style>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
     $(document).ready(function() {
                $('#createadmin').validate({
                       rules: {
                        password: {
                            required: true,
//                            alphanumeric:true,
                            minlength: 8
                        },email: {
                            required: true,
                            email: true
                        }

                    },
                    messages: {
                        email: "",
                        password: {
                            required: "",
                            minlength: "Your password must be at least 8 characters long"
                        }
                    }
                    
                });
    $('#email').focusout(function() {
                    email = $(this).val();
                    $('#newadmin').fadeIn();
                    $.ajax({
                        type: "GET",
                        data: {"email": email},
                        url: "<?php echo asset('authenticate_email'); ?>",
                        success: function(data) {
                            $("#newadmin").fadeOut(1000);
                            if (data) {
                                $('#errormessage').hide();
                                $('#submitadmin').attr('disabled', false);
                            } else {
                                $('#errormessage').show();
                                $('#email').css('border-color', 'red');
                                $('#submitadmin').attr('disabled', true);
                            }
                        }
                    });
                });
    });
    </script>
     <div style="display:none;"id="newadmin" ></div>
    </body>
</html>