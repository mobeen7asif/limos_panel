 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('logout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>Forget Password</h2>
                                <span>Create forget password</span>
                            </div>
                            <div class="search_account">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 spacer">
                                        <div class="account_holder">
                                            <div class="account_title">Forget Password</div>
                                            <form method="post" action="<?php echo asset('adminchangepassword')?>" id="changepassword"> 
                                            <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                            <div class="account_contant">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Old Password</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="password" class="form-control" placeholder="Enter Old Password" name="oldpassword" id="oldpassword">
                                                                <span class=" alert alert-danger" id="errormessage" style="display: none">Please Enter Correct password </span>
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-key"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>New Password</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="password" class="form-control" placeholder="Enter New Password" name="password" id="password">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-key"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Confirm Password</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="password" class="form-control" placeholder="Confirm Password" name="confrim_password">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-key"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-3">
                                                            <div class="input-group ">
                                                                <input type="submit" id="submitform" value="Create" class="btn custom_btn">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div></form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <script  src="<?php echo asset('js/validate.js') ?>"></script>
     <style>
            input.error {
                border:solid 1px red !important;
            }
            #changepassword label.error {
                width: auto;
                display: inline;
                color:red;
                font-size: 16px;
                float:right;
            }
        </style>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
                    $('#changepassword').validate({
                       rules: {
                       oldpassword: {
                            required: true
                        }, 
                        password: {
                            required: true,
                            minlength: 8
                        }, confrim_password: {
                            required: true,
                            minlength: 8,
                            equalTo: "#password"
                        }

                    },
                    messages: {
                        oldpassword: "",
                        password: {
                            required: "",
                            minlength: "Your password must be at least 8 characters long"
                        },
                        confrim_password: {
                            required: "",
                            minlength: "Your password must be at least 8 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                    }
                    
                });
    $('#oldpassword').focusout(function() {
                    password = $(this).val();
                    $('#fpasswordloader').fadeIn();
                    $.ajax({
                        type: "GET",
                         url: "<?php echo asset('checkpassword'); ?>",
                        data: {"password": password},
                        success: function(data) {
                            $("#fpasswordloader").fadeOut(1000);
                            if (data) {
                                $('#errormessage').hide();
                                $('#oldpassword').css('border-color', 'gray');
                                $('#submitform').attr('disabled', false);
                            } else {
                                $('#errormessage').show();
                                $('#oldpassword').css('border-color', 'red');
                                $('#submitform').attr('disabled', true);
                            }
                        }
                    });
                });
    </script>
    <div style="display:none;"id="fpasswordloader" ></div>
    </body>
</html>