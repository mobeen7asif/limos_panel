<!DOCTYPE html>
<html lang="en"> 
  
    <head>
    <meta charset="utf-8">
	<title><?php echo $title?></title>
        <meta name="description" content="Holy Limo! Check out this sweet ride I found on instalimos.com">
	<meta name="author" content="instalimo.com">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />

	<!-- Mobile Specific Metas  -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <!--   Bootstrap CSS   -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/bootstrap.min.css')?>">
        
        <!-- Custome CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/all.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/simple-sidebar.css')?>">
        <script  src="<?php echo asset('css/admin/js/jquery-latest.min.js') ?>"></script>
        
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/font-awesome.min.css')?>">
        
        <!-- Tablesorter: required -->
        <script  src="<?php echo asset('css/admin/js/jquery-latest.min.js') ?>"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/theme.blue.css')?>">
        <script  src="<?php echo asset('css/admin/js/jquery.tablesorter.js') ?>"></script>
        <script  src="<?php echo asset('css/admin/js/jquery.tablesorter.pager.js') ?>"></script>
        
        
        <script id="js">
            $(function() {

                // hide child rows
                $('.tablesorter-childRow td').hide();

                $(".tablesorter")
                        .tablesorter({
                                theme : 'blue',
                                // this is the default setting
                                cssChildRow: "tablesorter-childRow"
                        })
                        .tablesorterPager({
                                container: $("#pager"),
                                positionFixed: false
                        });
                // Toggle child row content (td), not hiding the row since we are using rowspan
                // Using delegate because the pager plugin rebuilds the table after each page change
                $('.tablesorter').delegate('.toggle', 'click' ,function(){

                        // use "nextUntil" to toggle multiple child rows
                        // toggle table cells instead of the row
                        $(this).closest('tr').nextUntil('tr:not(.tablesorter-childRow)').find('td').toggle();
                        return false;
                });
            });
        </script>
        
 </head>   
 <body class="bg-color">
        
       <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a class="" href="<?php echo asset('admindashboard') ?>"><img src="<?php echo asset('images/logo.png')?>" alt=""></a>
                </li>
                <li>
                    <a href="<?php echo asset('admindashboard') ?>"> 
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo asset('operator_list') ?>"> 
                        <i class="fa fa-user"></i>
                        <span>List Of operator's</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo asset('admin_list') ?>"> 
                        <i class="fa fa-user-secret"></i>
                        <span>List Of Admin's</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo asset('forgetpass') ?>"> 
                        <i class="fa fa-user-plus"></i>
                        <span>Accounts</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo asset('adminlogout')?>"> 
                        <i class="fa fa-sign-out"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
