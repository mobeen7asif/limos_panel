 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('logout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>List Of Admin's</h2>
                                <span>Here is the list of admin's</span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div class="table-responsive all_list">
                                <table class="table table-striped">
                                  <thead>
                                    <tr>
                                      <th width="50">Sr #</th>
                                      <th>Account Number</th>
                                      <th>Login Email</th>
                                      <th width="150" class="cennter">Delete</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                      <?php $i = 1;
 foreach ($admin as $add):
                                      ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $add->id?></td>
                                        <td><?php echo $add->email?></td>
                                        <td><a href="<?php echo asset('delete_user/'.$add->id) ?> " onclick="return confirm('Are you sure you want to delete this record')"class="link" >Delete</a></td>
                                    </tr>
                                    <?php $i=$i+1;
endforeach;
                                    ?>
                                    </tbody>
                                </table>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    
    </body>
</html>