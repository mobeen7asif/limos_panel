
<?php require('operator_includes/operator_header.php');

//print_r($user->last_four);exit;
?>
<div class="col-sm-9 equal_columns">
            <div class="col-sm-9 equal_columns">
                                                                 <?php if (Session::has('success')){ ?>
    <div class="alert alert-success"><?php echo  Session::get('success') ?></div>
<?php } ?>                                                     <?php if (Session::has('error')){ ?>
    <div class="alert danger"><?php echo  Session::get('error') ?></div>
<?php } ?>
    <div id="add_car_form">
        <div class="from_head">Credit Card Information</div>
        <div class="from_section">
            <form action="<?php echo asset('user_billing_address') ?>" method="POST" id="payment-form">
                <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>">
                <span class="payment-errors" style="color: red"></span>

                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>Card Number</label>
                    </div>
                   <div class="col-sm-8 ">
                        <input required type="text" size="20" name="number" data-stripe="number" value="<?php if($user){if ($user->last_four){ echo '************'.$user->last_four;}}?>"/>
                    </div>
                    
                </div>

                <div class="form-row row clearfix"> 
                    <div class="col-sm-4 ">
                         <label>CVC</label>
                    </div>
                    <div class="col-sm-8 ">
                         <input required type="text" size="4" data-stripe="cvc" name="cvc"/>
                    </div>
                    
                </div>
                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>Email</label>
                    </div>
                    <div class="col-sm-8">
                         <input required type="email"  name="email"/>
                    </div>
                    
                </div>

                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>Expiration</label>
                    </div>
                                      <div class="col-sm-4">
                                          <input required type="text"  name="exp_month" data-stripe="exp-month"  size="2" placeholder="MM"/>
                    </div>
                                   <div class="col-sm-4">
                                       <input required type="text"  data-stripe="exp-year" name="exp_year" size="04" placeholder="YYYY"/>
                    </div>
                </div>

                <button type="submit" class="btn fr">Submit Payment</button>
                <div class="clear"></div>
            </form>
        </div>
        <div class="from_head">Don't Miss out any business. Add a backup Card</div>
        <div class="from_section">

            <form action="<?php echo asset('user_add_other_card') ?>" method="POST" id="payment_form_2">
                <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>">
                <span class="payment-errors" style="color: red"></span>

                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>Card Number</label>
                    </div>
                    <div class="col-sm-8 ">
                         <input required type="text" size="20" data-stripe="number" value="<?php if($sec_card){if($sec_card[0]->c_num_sec){ echo '************'.$sec_card[0]->c_num_sec;}}?>" name="number"/>
                    </div>
                   
                </div>

                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>CVC</label>
                    </div>
                    <div class="col-sm-4 ">
                         <input required type="text" size="4" data-stripe="cvc" name="cvc"/>
                    </div>
           
                </div>
                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>Email</label>
                    </div>
                    <div class="col-sm-4 ">
                         <input required type="email"  name="email"/>
                    </div>
                    
                </div>
                <div class="form-row row clearfix">
                    <div class="col-sm-4 ">
                         <label>Expiration </label>
                    </div>
                     <div class="col-sm-4">
                         <input required type="text"  name="exp_month" data-stripe="exp-month"  size="2" placeholder="MM"/>
                    </div>
                                   <div class="col-sm-4">
                                       <input required type="text"  data-stripe="exp-year" name="exp_year" size="04" placeholder="YYYY"/>
                    </div>
                    
                    
                </div>

                <button type="submit" class="btn fr">Submit Payment</button>
                <div class="clear"></div>
            </form>
            <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Current Account Balance:</label>
                            </div>
                            <div class="col-sm-8 ">
                                <strong>$<?php if($amount){ echo $amount[0]->total_amount;}else{echo '0.00';}?></strong>
                            </div>
                        </div>
        </div>



    </div>
</div>
</div>
</div>
</div>
</div>
<footer id="footer">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-5 ft_section"><img src="images/logo.png" alt=""></div>
            <div class="col-sm-7 ft_section text-right_sm">
                <div class="ft_social">
                    <a href="#"><img src="images/fb.png" alt=""></a>
                    <a href="#"><img src="images/twitter.png" alt=""></a>
                    <a href="#"><img src="images/rss.png" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
                <!--<div>Designed by: <a target="_blank" href="http://vengile.com/">Vengile IT Solution</a></div>-->
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/bx.js"></script>
<script defer  src="js/jquery.plugin.js"></script>
<script defer  src="js/jquery.datepick.js"></script>
<script defer  src="js/jquery.fancybox.pack.js"></script>
<script defer  src="js/jquery.nouislider.all.min.js"></script>
<script defer  src="js/custom.js"></script>

</body>
</html>

<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
    // This identifies your website in the createToken call below
    Stripe.setPublishableKey('pk_live_o8disIUeifWOhMWeKPC2rVFQ');
    // ...
    jQuery(function($) {
        $('#payment-form').submit(function(event) {
            var $form = $(this);

            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true);

            Stripe.card.createToken($form, stripeResponseHandler);

            // Prevent the form from submitting with the default action
            return false;
        });
    });
    function stripeResponseHandler(status, response) {
        var $form = $('#payment-form');

        if (response.error) {
            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
//            console.log(response);
//            alert(response)
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
            
            // and submit
            $form.get(0).submit();
        }
    }





    jQuery(function($) {
        $('#payment_form_2').submit(function(event) {
            var $form = $(this);

            // Disable the submit button to prevent repeated clicks
            $form.find('button').prop('disabled', true);

            Stripe.card.createToken($form, stripeResponseHandler2);

            // Prevent the form from submitting with the default action
            return false;
        });
    });
    function stripeResponseHandler2(status, response) {
        var $form = $('#payment_form_2');

        if (response.error) {
            // Show the errors on the form
            $form.find('.payment-errors').text(response.error.message);
            $form.find('button').prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $form.append($('<input type="hidden" name="stripeToken" />').val(token));
            // and submit
            $form.get(0).submit();
        }
    }


</script>
