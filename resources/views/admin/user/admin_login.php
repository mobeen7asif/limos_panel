<!DOCTYPE html>
<html lang="en"> 
  
    <head>
    <meta charset="utf-8">
	<title><?php echo $title?></title>
        <meta name="description" content="Holy Limo! Check out this sweet ride I found on instalimos.com">
	<meta name="author" content="instalimo.com">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />

	<!-- Mobile Specific Metas  -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
         <!--<link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/bootstrap.min.css')?>">-->
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/all.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/admin/css/style.css')?>">

        
 </head>   
    <body>
        
        <div class="site__container">
            <div class="grid__container">
                <div class="admin_logo">
                    <a id="logo" href="<?php echo asset('/') ?>"><img src="<?php echo asset('images/logo.png') ?>" alt="instalimo" height="38" width="216"></a>
                </div>
                <form action="<?php echo asset('adminlogin');?>" method="post" class="form form--login">
                  <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                <div class="form__field">
                  <label class="fontawesome-user" for="login__username"><span class="hidden">Email</span></label>
                  <input id="email" type="text" class="form__input" placeholder="Email" required name="email">
                </div>

                <div class="form__field">
                  <label class="fontawesome-lock" for="login__password"><span class="hidden">Password</span></label>
                  <input id="password" type="password" class="form__input" placeholder="Password" required name="password">
                </div>

                <div class="form__field">
                  <input type="submit" value="Login">
                </div>

              </form>
            </div>

          </div>
        
    </body>
</html>