<?php include('operator_includes/operator_header.php'); 
?>
        <div class="col-sm-9 equal_columns">
                                                                 <?php if (Session::has('success')){ ?>
    <div class="alert alert-success"><?php echo  Session::get('success') ?></div>
<?php } ?>                                                     <?php if (Session::has('error')){ ?>
    <div class="alert danger"><?php echo  Session::get('error') ?></div>
<?php } ?>
            <div id="add_car_form">
                <form method="post" action="<?php echo asset('user_company_profile');?>" enctype="multipart/form-data">
                                       <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>">
                	<div class="from_head">Company Logo</div>
                    <div class="from_section ">
                        <div class="col-md-4">
                            <img src="<?php if($company){ echo asset('images/comp_images/'.$company[0]->comp_image);} else{echo asset('images/comp_images/demo.png');}?>" alt="">
                        </div>
                        <div id="upload_photos" class="col-md-8 cmprofile">
                            <div class="form-group clearfix">
                                <input id="file-5" class="file" type="file" data-min-file-count="1"  data-preview-file-type="any" data-upload-url="#" data-preview-file-icon="" name="comp_image" <?php if(!$company[0]->comp_image){ ?>required<?php } ?> >
                            </div>
                           
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="from_head">Profile Information</div>
                    <div class="from_section">
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Company Name<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="text" name="comp_name" required value="<?php if($company){ echo $company[0]->comp_name; }?> ">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>First Name<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="text" name="comp_f_name" required value="<?php if($company){ echo $company[0]->comp_f_name; }?> ">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Last Name<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="text" name="comp_l_name" required value="<?php if(($company)){ echo $company[0]->comp_l_name; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Cell Phone<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <!--<input type="tel" name="comp_phone"  pattern='[\(]\d{3}[\)]\d{3}[\-]\d{4}' title='Phone Number (Format:(415)488-7077)'required value="<?php if(($company)){ echo $company[0]->comp_phone; }?> "id="comp_phone">&nbsp;<span id="errmsg" style="color:red"></span>-->
                                    <input type="tel" name="comp_phone" required value="<?php if(($company)){ echo $company[0]->comp_phone; }?> "id="comp_phone">&nbsp;<span id="errmsg" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Sales Phone Line<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="tel" name="comp_mobile" required value="<?php if(($company)){ echo $company[0]->comp_mobile; }?>"id="comp_mobile">&nbsp;<span id="errmsg1" style="color:red"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Email<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="email" name="comp_email" required value="<?php if(($company)){ echo $company[0]->comp_email; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Business URL<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="text" name="comp_biz_url" required value="<?php if(($company)){ echo $company[0]->comp_biz_url; }?>">
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>More info about your Business</label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <textarea name="comp_info" required><?php if(($company)){ echo $company[0]->comp_info;}?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="user_id" value="<?php echo $id ?>">
                    <input type="submit" class="fr btn" value="Update" id="updatecompany">
                </form>
                
            </div>
        </div>
    </div>
</div>
</div>
<footer id="footer">
	<div class="container-fluid">
    	<div class="row clearfix">
    		<div class="col-sm-5 ft_section"><img src="images/logo.png" alt=""></div>
	        <div class="col-sm-7 ft_section text-right_sm">
            	<div class="ft_social">
            		<a href="#"><img src="images/fb.png" alt=""></a>
                	<a href="#"><img src="images/twitter.png" alt=""></a>
               		 <a href="#"><img src="images/rss.png" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
                <!--<div>Designed by: <a target="_blank" href="http://vengile.com/">Vengile IT Solution</a></div>-->
            </div>
        </div>
  </div>
</footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script  src="js/bx.js"></script>
    <script defer  src="js/jquery.plugin.js"></script>
    <script defer  src="js/jquery.datepick.js"></script>
    <script defer  src="js/jquery.fancybox.pack.js"></script>
    <script defer  src="js/jquery.nouislider.all.min.js"></script>
    <script defer src="js/fileinput.js" type="text/javascript"></script>
    <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
    <script defer  src="js/custom.js"></script>
    <script type="text/javascript">
      $("#comp_phone").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
   
         $("#comp_mobile").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg1").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
   
   $("#comp_phone").focusout(function() {
       value = $(this).val();
                     if(value.length < 8){
//                         $("#comp_phone").val('');
                       $("#errmsg").html("Phone Number Isnt Correct").show().fadeOut(5000);
                       $('#updatecompany').attr('disabled', true);
                        return false;  

                     }
                     else if(value.length > 15){
//                         $("#comp_phone").val('');
                       $("#errmsg").html("Phone Number Isnt Correct").show().fadeOut(5000);
                        $('#updatecompany').attr('disabled', true);
                        return false;  
                     }
                     else{
                         $("#errmsg").hide();
                         $('#updatecompany').attr('disabled', false);
                     }
                 });
                 
                 
                    $("#comp_mobile").focusout(function() {
                         value = $(this).val();
                     if(value.length < 8){
//                         $("#comp_phone").val('');
                       $("#errmsg1").html("Phone Number Isnt Correct").show();
                       $('#updatecompany').attr('disabled', true);
                        return false;  

                     }
                     else if(value.length > 15){
//                         $("#comp_phone").val('');
                       $("#errmsg1").html("Phone Number Isnt Correct").show();
                        $('#updatecompany').attr('disabled', true);
                        return false;  
                     }
                     else{
                         $("#errmsg1").hide();
                         $('#updatecompany').attr('disabled', false);
                     }
                 });
    </script>
</body>
</html>