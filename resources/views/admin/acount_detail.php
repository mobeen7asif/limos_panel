 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>Account Detail</h2>
                                <span>Here's is the account detail</span>
                                
                            </div>
                             <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div class="row top-margin">
                                <div class="col-lg-12">
                                    <div class="detail">
                                        <ul>
                                            <li>
                                                <h3>Account No:</h3>
                                                <span><?php echo $user[0]->id?></span>
                                            </li>
                                            <li>
                                                <h3>Limo Count:</h3>
                                                <span><?php echo $vehicals?></span>
                                            </li>
                                            <li>
                                                <h3>Account Status:</h3>
                                                <span><?php if(isset($status)) { echo $status;}?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="detail_contant">
                                        <div class="account_title">Payment Detail</div>
                                        <div class="account_contant">
                                            <div class="form-group payment_detail">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>Jump Into Account:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <a href="<?php echo asset('viewdashboard/'.$id)?>"  class="btn">Go</a>
                                                    </div>
                                                </div>
                                            </div> <?php if(Illuminate\Support\Facades\Auth::user()->role == 1 ) { ?>
                                            <div class="form-group payment_detail">
                                               
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>Refund CC on File:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <!--<input type="number" placeholder="$" name="amount">--> 
                                                        <a href="<?php echo asset('getrefund/'.$id)?>" class="btn payment_detailanchor">Refund Section</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="form-group payment_detail">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>Charge CC on File:</label>
                                                    </div>
                                                    <form method="post" action="<?php echo asset('charge_user')?>">
                                                         <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                                    <div class="col-lg-8">
                                                        <input type="number" placeholder="$" name="amount" required min="1">
                                                        <input type="hidden" value="<?php echo $id;?>" name="user_id">
                                                       
                                                        <input type="submit" value="Charge" class="btn">
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                           <?php if(Illuminate\Support\Facades\Auth::user()->role == 1 ) { ?>
                                            <form method="post" action="<?php echo asset('add_lead_cradit')?>">
                                                         <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                                          <input type="hidden" value="<?php echo $id;?>" name="user_id">
                                            <div class="form-group payment_detail">
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>Add lead Credit:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <input type="number" placeholder="$" name="amount" required min="1">
                                                        <input type="text" placeholder="Reason">
                                                        <input type="submit" value="Add" class="btn">
                                                    </div>
                                                </div>
                                            </div></form>
                                           <?php } ?>
                                            <div class="form-group payment_detail">
                                               
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>Application:</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <!--<input type="number" placeholder="$" name="amount">--> 
                                                        <a href="<?php echo asset('application/'.$id) ?>" class="btn payment_detailanchor">View Application</a>
                                                    </div>
                                                </div>
                                                <!--<a href="<?php echo asset('user_account_info'); \Illuminate\Support\Facades\Session::put('user_id', $id); ?>"  class="btn payment_detailanchor">Change Password</a>-->
                                            </div>
                                             <div class="form-group payment_detail">
                                               
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label>Payments</label>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <!--<input type="number" placeholder="$" name="amount">--> 
                                                        <a href="<?php echo asset('user_payment_detrail/'); \Illuminate\Support\Facades\Session::put('user_id', $id);?>" class="btn payment_detailanchor">View Detail</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <form id="change_password" method="post" action="<?php echo asset('admin_change_password');?>">
                                                         <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                                          <input type="hidden" value="<?php echo $id;?>" name="user_id">
                                                <div class="form-group payment_detail">
                                                    <div class="row">
                                                        <div class="col-lg-2">
                                                            <label>Change Password:</label>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="form-group">
                                                                <input type="password" placeholder="New Password" name="password" id="password" >
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" placeholder="Confirm Password" name="confrim_password" >
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="submit" value="change" class="btn">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="account_holder">
                                        <div class="account_title">Account Notes</div>
                                        <div class="account_contant">
                                            <div class="add_note">
                                                <a href="#" data-toggle="modal" data-target="#addnote">Add Note</a>
                                            </div>
                                            <?php foreach ($notes as $note) :?>
                                            <div class="note-text">
                                                <p><?php echo $note->note?></p>
                                                <span><?php echo $newDate = date("l d-m-Y H:i", strtotime($note->created_on));?></span>&nbsp;<span><?php echo $email?></span>
                                            </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
      <!-- Add Note Modal -->
        <div class="modal fade" id="addnote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Note</h4>
              </div>
              <div class="modal-body">
                  <form method="post" action="<?php echo asset('add_note')?>" id="adminaddnote"> 
                      <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                  <div class="form-group">
                      <label>Add Note Here</label>
                      <textarea placeholder="Add Some Note plaese" id="note" name="note" required></textarea>
                  </div>
                      <input type="hidden" name="user_id" value="<?php echo $id?>">
                  <div class="form-group" style="text-align: right;">
                      <button type="submit" class="btn detail_link">Add Note</button>
                  </div>
                  </form>
              </div>
            </div>
          </div>
        </div>  
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
   
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.js"></script>

    
    <script type="text/javascript">

            $(document).ready(function() {
                $('#change_password').validate({// initialize the plugin
                    rules: {
                        password: {
                            required: true

                        },
                        confrim_password: {
                            required: true,

                            equalTo: "#password"
                        }
                    },
                    messages: {
                        
                        password: {
                            required: ""
                            
                        },
                        
                        confrim_password: {
                            required: "",
                            
                            equalTo: "Please enter the same password"
                        }
                    }


                });
            });
                
    </script>
    
    </body>
</html>