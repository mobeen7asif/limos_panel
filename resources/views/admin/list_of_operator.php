 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>List Of Operators</h2>
                                <span>Here is the list of <strong><?php echo $title ?></strong> </span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div id="demo">
                                <div class="filter-search">
                                    <input class="search" type="search" data-column="1,2,3,4" placeholder="Search By Account # Email Or Last Name">
                                </div>
                                <h4 class="alert alert-info">You can update One account at one time and it will be last opened account</h4>
                                <div class="table-responsive">
                                    <table class="tablesorter">
                                        <thead>
                                            <tr>
                                                <th width="50" class="sorter-false">Sr #</th>
                                                <th>Company Name</th>
                                                <th>Account Number</th>
                                                <th>Login Email</th>
                                                <th>Last Name</th>
                                                <?php if($title != 'Queue Accounts') :?>
                                                <th class="sorter-false">Deactivate</th>
                                                <?php endif;?>
                                                <?php if($title == 'Queue Accounts') :?>
                                                <th >Account Status</th>
                                                <?php endif;?>
                                                <?php // if($title == 'Queue Accounts') :?>
                                                <th >Application</th>
                                                <?php // endif;?>
                                                <th width="150" class="cennter sorter-false">Detail</th>
                                                <th width="150" class="cennter sorter-false">Time out</th>
                                                <th width="150" class="cennter sorter-false">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i=1;
                                            foreach ($users as $user):?>
                                          <tr>
                                              <td><?php echo $i?></td>
                                              <td><a href="<?php echo asset('viewdashboard/'.$user->id)?>" class="link_name"><?php if($user->comp_name) { echo $user->comp_name ;} else { ?>No Company Name<?php } ?></a></td>
                                              <td><?php echo $user->id?></td>
                                              <td><?php echo $user->email?></td>
                                              <td><?php echo $user->comp_l_name?></td>
                                              <?php if($title != 'Queue Accounts') :?>
                                              <td><input type="checkbox" id="status_<?php echo $user->id?>" onclick="changeuserstatus('<?php echo $user->id?>')" name="status_<?php echo $user->id?>" <?php if($user->deactive_account == 1){ ?> checked<?php } ?>></td>
                                             <?php endif;?>
                                               <?php if($title == 'Queue Accounts') :?>
                                              <td><?php if($user->is_active == 1){ ?>Approved<?php } else { ?><a href="<?php echo asset('approve_user/'.$user->id) ?>" class="detail_link">Pending</a><?php } ?></td>
                                              <?php endif;?>
                                              <?php // if($title == 'Queue Accounts') :?>
                                              <td><a href="<?php echo asset('application/'.$user->id) ?>" class="detail_link">View Application</a></td> <?php // endif;?>
                                              <td><a href="<?php echo asset('detail/'.$user->id) ?>" class="detail_link">View Detail</a></td>
                                              <td>
                                                  <div class="onoffswitch">
                                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="<?php echo 'timeout'.$user->id ?>" <?php if($user->time_out == 1){ ?> checked<?php } ?> onclick="changetimeout('<?php echo $user->id?>')">
                                                    <label class="onoffswitch-label" for="<?php echo 'timeout'.$user->id ?>">
                                                        <span class="onoffswitch-inner"></span>
                                                        <span class="onoffswitch-switch"></span>
                                                    </label>
                                                </div>
                                              </td>
                                              <td><a href="<?php echo asset('delete_user/'.$user->id) ?> " onclick="return confirm('Are you sure you want to delete this record')"class="link" >Delete</a></td>
                                          </tr>
                                          <?php $i=$i+1; endforeach;?>
                                          </tbody>
                                    </table>
                                </div>
                                <div id="pager" class="pager">
                                    <form>
                                        <input type="button" value="&lt;" class="prev" />
                                        <input type="text" class="pagedisplay" readonly/>
                                        <input type="button" value="&gt;" class="next" />
                                        <select class="pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    function changeuserstatus(user_id){
    if( $('#status_'+user_id).is(':checked')) {
            $.ajax({
            type: "GET",
            url: "<?php echo asset('update_user_status'); ?>",
            data:{"user_id":user_id,"status":"1"},
            success: function(data) {
            }});
    }else{
        $.ajax({
            type: "GET",
            url: "<?php echo asset('update_user_status'); ?>",
            data:{"user_id":user_id,"status":"0"},
            success: function(data) {
            }}); 
    }
}
function changetimeout(user_id){
    if( $('#timeout'+user_id).is(':checked')) {
        
            $.ajax({
            type: "GET",
            url: "<?php echo asset('update_time_out'); ?>",
            data:{"user_id":user_id,"status":"1"},
            success: function(data) {
            }});
    }else{
        $.ajax({
            type: "GET",
            url: "<?php echo asset('update_time_out'); ?>",
            data:{"user_id":user_id,"status":"0"},
            success: function(data) {
            }}); 
    }
}
    </script>
    
    </body>
</html>