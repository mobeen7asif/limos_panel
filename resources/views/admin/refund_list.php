<?php require('include/header.php'); ?>
<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="top_header">
        <div class="toggle-btn">
            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        <div class="logout-btn">
            <a href="<?php echo asset('adminlogout') ?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="contant_holder">
                    <div class="dashboard_title">
                        <h2>Refund Section</h2>
                        <span>Admin refund for</span>
                        <span><strong><?php echo $user[0]->comp_f_name . ' ' . $user[0]->comp_l_name . ' ' ?></strong>Here </span>

                    </div>
                    <?php if (Session::has('success')) { ?>
                        <Span class="alert alert-success"><?php echo Session::get('success') ?></Span>
                    <?php } if (Session::has('error')) { ?>
                        <Span class="alert alert-danger"><?php echo Session::get('error') ?></Span>
                    <?php } ?>
                    <div id="demo">
                        <div class="filter-search">
                            <input class="search" type="search" data-column="all" placeholder="Match any column">
                        </div>
                        <div class="table-responsive">
                            <table class="tablesorter">
                                <thead>
                                    <tr>
                                        <th width="50">Sr #</th>
                                        <th>Account Number</th>
                                        <th>Transaction Amount</th>
                                        <th>Refundable Amount</th>
                                        <th width="150" class="cennter">Refund</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    if ($payments) {
                                        foreach ($payments as $payment):
                                            ?>
                                            <tr>
                                                <td><?php echo $i ?></td>
                                                <td><?php echo $payment->user_id ?></td>
                                                <td><?php echo $payment->amount ?></td>
                                                <td><?php echo $payment->remaing_amount ?></td>
                                                <td><a href="#"class="link refund-link" <?php if ($payment->remaing_amount == 0) { ?> >Refunded <?php } else { ?>  data-toggle="modal" data-target="#refund_<?php echo $i; ?>" >Refund <?php } ?></a></td>
                                            </tr>
                                        <div id="refund_<?php echo $i; ?>" class="modal fade refund_model" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Enter Amount</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="<?php echo asset('getrefund') ?>" method="post" >
                                                            <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                                            <input type="hidden"  name="transid" value="<?php echo $payment->transaction_id ?>" id="transid_<?php echo $i ?>">
                                                            <input type="hidden"  name="id" value="<?php echo $id ?>" >
                                                            <input onchange="checkpayment('<?php echo $i ?>')" type="number" placeholder="$" name="amount" required id="refund_amount_<?php echo $i ?>" min="1">
                                                            <input type="submit" value="Submit" class="btn" id="submitrefund" >
                                                            <span style="display: none" id="displayerror_<?php echo $i ?>" class="alert alert-danger">You Can Only Refund <?php echo $payment->remaing_amount; ?></span>
                                                        </form>

                                                    </div>
                                                </div>

                                            </div>
                                        </div> 
                                        <?php
                                        $i = $i + 1;
                                    endforeach;
                                } else {
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td colspan="4">Sorry Nothing to refund</td>
                                    <tr>
<?php }
?>
                                    </tbody>
                            </table>
                        </div>
                        <div id="pager" class="pager">
                            <form>
                                <input type="button" value="&lt;" class="prev" />
                                <input type="text" class="pagedisplay" readonly/>
                                <input type="button" value="&gt;" class="next" />
                                <select class="pagesize">
                                    <option selected="selected" value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->
</div>
<!-- Bootstrap Core JavaScript -->
<script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
<script>
                                                      $("#menu-toggle").click(function(e) {
                                                          e.preventDefault();
                                                          $("#wrapper").toggleClass("toggled");
                                                      });
                                                      function checkpayment(id) {
                                                          amount = $('#refund_amount_' + id).val();
                                                          trnsid = $('#transid_' + id).val();
                                                          $('#refundloader').fadeIn();
                                                          $.ajax({
                                                              type: "GET",
                                                              data: {"amount": amount, "trnsid": trnsid},
                                                              url: "<?php echo asset('checkrefund'); ?>",
                                                              success: function(data) {
                                                                  $("#refundloader").fadeOut(1000);
                                                                  if (data) {
                                                                      $('#displayerror_' + id).hide();
                                                                      $('#submitrefund').attr('disabled', false);
                                                                  } else {
                                                                      $('#displayerror_' + id).show();
                                                                      $('#submitrefund').attr('disabled', true);
                                                                  }

                                                              }});
                                                      }
</script>
<div style="display:none;"id="refundloader" ></div>
</body>
</html>