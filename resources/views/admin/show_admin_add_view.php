<?php require('include/header.php');
 ?>
<div id="page-content-wrapper">
    <div class="top_header">
        <div class="toggle-btn">
            <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
        </div>
        <div class="logout-btn">
            <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
        </div>
    </div>
    <div class="container-fluid">
                           <div class="row" style="margin: 0px;">
                                    <?php
//                                    print_r(Illuminate\Support\Facades\Auth::user());exit;
                                    if(Illuminate\Support\Facades\Auth::user()->role == 1 ) {?>
                                    <div class="col-lg-6 col-md-6 col-sm-12 loginWidget spacer">
                                        <div class="account_holder">
                                            <div class="account_title">Create Admin Account</div>
                                            <form method="post" action="<?php echo asset('createadmin')?>" id="createadmin"> 
                                                <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>" >
                                            <div class="account_contant">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Email Login</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input id="email" name="email" type="email" class="form-control" placeholder="example@example.com" autocomplete="off">
                                                               
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-envelope-o"></i>
                                                                </span>
                                                            </div> <span id="errormessage" class="alert alert-danger error" style="display: none;">Email is Already With Us</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Password</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="input-group ">
                                                                <input type="password" class="form-control" placeholder="Enter Password" name="password">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-key"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-8 col-md-offset-3">
                                                            <div class="input-group ">
                                                                <input id="submitadmin" type="submit" value="Create" class="btn custom_btn">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
        </div>
    </div>
