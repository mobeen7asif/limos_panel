 <?php require('include/header.php'); ?>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2>Searches Detail</h2>
                                <span>Here is the detail of searches</span>
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <div id="demo">
                                <div class="filter-search">
                                    <input class="search" type="search" data-column="1,2,3,9" placeholder="Search By First Name Or Last Name Or Email">
                                </div>
                                <div class="table-responsive">
                                    <table class="tablesorter">
                                    <thead>
                                        <tr>
                                            <th width="50" class="sorter-false">Sr #</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th>Service/Event Type</th>
                                            <th>Number of Passengers</th>
                                            <th>Pickup Location</th>
                                            <th>Service Date</th>
                                            <th>Companies</th>
                                            <th>Number of Results</th>
<!--                                            <th width="150" class="cennter sorter-false">Delete</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; foreach ($search_details as $search):
//                                        echo '<pre>';
//                                        print_r($search);exit;
                                        ?>
                                        <tr>
                                            <td><?php echo $i?></td>
                                            <td><?php echo $search->firstname?></td>
                                            <td><?php echo $search->lastname?></td>
                                            <td><?php echo $search->email?></td>
                                            <?php if($search->contact == ""){ ?>
                                                <td>Not Available</td>
                                            <?php } else{?>
                                                <td><?php echo $search->contact?></td>
                                            <?php }?>
                                            <td><?php echo $search->occian_type?></td>
                                            <td><?php echo $search->passengers?></td>
                                            <td><?php echo $search->location?></td>
                                            <td><?php echo $search->pickdate?></td>
                                            <td>
                                                <?php $companies= getCompanies($search->vister_id);
                                                         
                                            foreach($companies as $company){ 
                                                ?>
                                                <a style="color: blue" href="<?php echo asset('viewdashboard/'.$company->user_id);?>"><?php echo $company->comp_name;?></a>,
                                            <?php } ?></td>
                                            
                                            <td><?php echo $search->compnies_searched ?></td>
                                            <!--<td><a href="<?php // echo asset('delete_user/'.$add->id) ?> " onclick="return confirm('Are you sure you want to delete this record')"class="link" >Delete</a></td>-->
                                        </tr>
                                    <?php $i=$i+1; endforeach; ?>
                                    </tbody>
                                </table>
                                </div>
                                <div id="pager" class="pager">
                                    <form>
                                        <input type="button" value="&lt;" class="prev" />
                                        <input type="text" class="pagedisplay" readonly/>
                                        <input type="button" value="&gt;" class="next" />
                                        <select class="pagesize">
                                            <option selected="selected" value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <a href="<?php echo asset('export_search_details')?>" class="btn payment_detailanchor">Export</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
    
    </body>
</html>

