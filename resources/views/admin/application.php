 <?php require('include/header.php'); 
// echo '<pre>';
// print_r($user);
// exit;
// print_r($company);exit;
 ?>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="top_header">
                <div class="toggle-btn">
                    <a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="fa fa-bars"></i></a>
                </div>
                <div class="logout-btn">
                    <a href="<?php echo asset('adminlogout')?>" class="btn btn-default"><i class="fa fa-sign-out"></i></a>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 admin_application">
                        <div class="contant_holder">
                            <div class="dashboard_title">
                                <h2><?php echo $title?></h2>
                                
                                <!--<span>Here is the list of <strong>some text</strong> </span>-->
                            </div>
                            <?php if (Session::has('success')){ ?>
                            <Span class="alert alert-success"><?php echo  Session::get('success') ?></Span>
                             <?php } if (Session::has('error')){ ?>
                            <Span class="alert alert-danger"><?php echo  Session::get('error') ?></Span>
                             <?php } ?>
                            <h2>Company &amp; Contact Information</h2>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Company legal name: </strong>
                                                <span><?php echo $company[0]->comp_name?></span>
                                            </li>
                                            <li>
                                                <strong>Public Display name (DBA name): </strong>
                                                <span><?php echo $company[0]->dba_name?></span>
                                            </li>
                                            <li>
                                                <strong>Number of insured vehicles: </strong>
                                                <span><?php echo $company[0]->insured_vehicals?></span>
                                            </li>
                                            <li>
                                                <strong>Your business started: </strong>
                                                <span><?php echo $company[0]->biz_date?></span>
                                            </li>
                                            <li>
                                                <strong>Company Email: </strong>
                                                <span><?php echo $company[0]->comp_email?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Preferred contact's last name: </strong>
                                                <span><?php echo $company[0]->comp_l_name?></span>
                                            </li>
                                            <li>
                                                <strong>Preferred contact's phone Number: </strong>
                                                <span><?php echo $company[0]->comp_phone?></span>
                                            </li>
                                            <li>
                                                <strong>Reservation Phone number: </strong>
                                                <span><?php echo $company[0]->comp_mobile?></span>
                                            </li>
                                            <li>
                                                <strong>Company Website: </strong>
                                                <span><?php echo $company[0]->comp_biz_url?></span>
                                            </li>
                                            <li>
                                                <strong>Preferred contact's first name: </strong>
                                                <span><?php echo $company[0]->comp_f_name?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h2>Transportaion Permit Information</h2>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Permit Number: </strong>
                                                <span><?php echo $company[0]->permit_number?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Name of the issuing jurisdicion/authority: </strong>
                                                <span><?php echo $company[0]->autherity_name?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h2>Business Address & Preferred Contact Info</h2>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Address Line 1: </strong>
                                                <span><?php echo $company[0]->address_1?></span>
                                            </li>
                                            <li>
                                                <strong>Country: </strong>
                                                <span><?php echo $company[0]->country_1?></span>
                                            </li>
                                            <li>
                                                <strong>State/Province: </strong>
                                                <span><?php echo $company[0]->state_1?></span>
                                            </li>
                                            <li>
                                                <strong>Do you/have you advertise(d) anywhere else online? If So where.: </strong>
                                                <span><?php if($company[0]->advertised_bef != '0') { echo $company[0]->advertised_bef; }else{echo 'NO';}?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Address Line 2: </strong>
                                                <span><?php echo $company[0]->address_2?></span>
                                            </li>
                                            <li>
                                                <strong>Zip/Postal: </strong>
                                                <span><?php echo $company[0]->zipcode?></span>
                                            </li>
                                            <li>
                                                <strong>City: </strong>
                                                <span><?php echo $company[0]->city?></span>
                                            </li>
                                            <li>
                                                <strong>How did you hear about Instalimos: </strong>
                                                <span><?php echo $company[0]->aboutus?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <h2> Account Information</h2>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="application_form">
                                        <ul>
                                            <li>
                                                <strong>Account Login Email: </strong>
                                                <span><?php echo $user->email?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <?php if($user->is_active == '0'){ ?>
                                    <a class="detail_link" href="<?php echo asset('approve_user/'.$user->id)?>"> Approve</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

     
        
        
    <!-- Bootstrap Core JavaScript -->
    <script  src="<?php echo asset('css/admin/js/bootstrap.min.js') ?>"></script>
    </body>
</html>