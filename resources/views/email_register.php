<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
    <!--<![endif]-->

 Hi <?php echo $name?>,<br>
 <br>
Welcome to the Instalimos network. It's our mission in life to help you grow your business and to help you discover local customers searching for your service.<br>
What next? To start you can login to your account  <a href="http://www.instalimos.com">here</a> Once in your account you can fill out your profile, add your vehicles, and tell us more about your business.<br>
In the next 24 hours one of our team members will call you to answer any questions you may have and to review your account profile to ensure you get the best results!<br>
Cheers to new business! We look forward to helping you grow your bottom line.<br>
 <br>
 <strong>The Instalimos team</strong>
</html>