<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>

        <!-- Basic Page Needs
  ================================================== -->
        <meta charset="utf-8">
        <title>InstaLimo| Home</title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Mobile Specific Metas
  ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- CSS
  ================================================== -->
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/base.css') ?>">
        <script>
            function loadCSS(e, t, n) {
                "use strict";
                var i = window.document.createElement("link");
                var o = t || window.document.getElementsByTagName("script")[0];
                i.rel = "stylesheet";
                i.href = e;
                i.media = "only x";
                o.parentNode.insertBefore(i, o);
                setTimeout(function() {
                    i.media = n || "all"
                })
            }

        </script>
        <script>
            
            loadCSS("<?php echo asset('css/fancybox.css') ?>");
            loadCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700");
        </script>
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">




    </head>
<body>
<div id="lightbox_layout">
<a style="display:none" class="fancybox_auto" href="#light_box" >Inline</a>
<div id="light_box">
<style>
	#loing_sinup .btn {
			border:none;
			width:50%;
			float:left;
			border-radius:0;
			font-weight:bold;
		}
	#loing_sinup {
		margin-top:-20px;
		margin-bottom:20px;
		border-bottom:solid 1px #ddd;
		}
	#loing_sinup .btn.active {
			background:none;
			color:#ddd;
			box-shadow:none;
		}
</style>
<div class="col-sm-12">
    <div id="loing_sinup" >
        <a href="<?php echo asset('auth/login')?>" class="btn active">Login</a><span href="#" class="btn">Register</span>
        <div class="clear"></div>
    </div>
</div>


<form class="form-horizontal clearfix" role="form" method="POST" action="{{ url('/auth/register') }}">
    @if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-8">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-8">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-8">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-8">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
                                                    <div class="col-sm-4"><a href="<?php echo asset('/')?>"><img src="<?php echo asset('/')?>images/logo.png"></a></div>
							<div class="col-sm-8">
								<button type="submit" class="btn btn-primary">
									Register Now
								</button>
							</div>
						</div>
                        
					</form>
	</div>
</div>





  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script defer  src="<?php echo asset('js/jquery.fancybox.pack.js') ?>"></script>
        <script defer>
         $(window).load(function() {
               jQuery(".fancybox_auto").fancybox({padding: 0, modal: true, helpers: {overlay: {closeClick: false}}}).last().trigger('click');
         
        });
        </script>
    
</body>
</html>