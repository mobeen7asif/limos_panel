<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <?php // require('include/top.php'); ?>
    @extends ('auth/include/top')
    <body>
        <div id="lightbox_layout">
            <a style="display:none" class="fancybox_auto" href="#light_box" >Inline</a>
            <div id="light_box">
           
                <h3 class="litbox_title">
                    <a href="<?php echo asset('/')?>" style="position:absolute; top:0; right:0; cursor:pointer" onclick="$.fancybox.close()"><img src="<?php echo asset('images/close_btn.png') ?>" alt=""></a>
                    Recover Password
                    <small>Just one more step to recover your password</small>
                </h3>
                @if (count($errors) > 0)
						<div class="alert alert-danger">
							<!--<strong>Whoops!</strong> There were some problems with your input.<br><br>-->
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                                       <form class="form-horizontal" role="form" method="POST" action="<?php echo asset('password/reset')?>">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">
						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Reset Password
								</button>
							</div>
						</div>
					</form>
            </div>
        </div>
            @extends ('auth/include/footer')
        <?php // require('include/footer.php'); ?>
        <style>
            #userdetail input.error {
                border-color:red;
            }
        </style>
        <script type="text/javascript">

            function submitform() {
                $('#userdetail').submit();
            }
            $(document).ready(function() {

                $('#userdetail').validate({// initialize the plugin
                    rules: {
                        firstname: "required", lastname: "required",
                        email: {required: true,
                            email: true
                        }
                    },
                    messages: {
                        firstname: "",
                        email: "",
                        lastname: "",
                    }


                });

            });
        </script>

    </body>
</html>
