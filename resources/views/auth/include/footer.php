<footer id="footer">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-5 ft_section"><img src="<?php echo asset('images/logo.png') ?>" alt=""></div>
            <div class="col-sm-7 ft_section text-right_sm">
                <div class="ft_social">
                    <a href="#"><img src="<?php echo asset('images/fb.png') ?>" alt=""></a>
                    <a href="#"><img src="<?php echo asset('images/twitter.png') ?>" alt=""></a>
                    <a href="#"><img src="<?php echo asset('images/rss.png') ?>" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
            </div>
        </div>
    </div>
</footer>
<script src="<?php echo asset('js/jquery.js') ?>"></script>
<script  src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script  src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script  src="<?php echo asset('js/bx.js') ?>"></script>
<script  src="<?php echo asset('js/jquery.plugin.js') ?>"></script>
<script  src="<?php echo asset('js/jquery.datepick.js') ?>"></script>
<script  src="<?php echo asset('js/jquery.fancybox.pack.js') ?>"></script>
<script  src="<?php echo asset('js/custom.js') ?>"></script>
<script  src="<?php echo asset('js/validate.js') ?>"></script>
<script  src="<?php echo asset('js/jquery.nouislider.all.min.js') ?>"></script>
<script  src="<?php echo asset('js/jquery.datetimepicker.js') ?>"></script>


<script type="text/javascript">
//on ready function
    $(function() {
        var availableTags = [];
// Get request to get all cities to auto load
        $.ajax({
            type: "GET",
            url: "<?php echo asset('get_cities'); ?>",
            success: function(data) {
                result = JSON.parse(data);
                $.each(result, function(i, item) {
                    availableTags.push(item.city_name)
//                assign city name on autoload
//                    availableTags.push(item.city_name)
                });
//                autofill required input
//                $("#picklocation").autocomplete({source: availableTags});
//                $("#droploc").autocomplete({source: availableTags});
//                $("#picloc").autocomplete({source: availableTags});
//                $("#searchloc").autocomplete({source: availableTags});
            }});
    });

</script>