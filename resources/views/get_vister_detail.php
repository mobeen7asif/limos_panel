<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <?php require('include/top.php'); ?>
    <body>
        <?php require('include/script.php'); ?>
       <script> 
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o ),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-63470315-1', 'auto');
            ga('send', 'pageview', ':/vp-email-pop-over');

        </script>
        <div id="lightbox_layout">
            <a style="display:none" class="fancybox_auto" href="#light_box" >Inline</a>
            <div id="light_box">
            <!--<a style="position:absolute; top:0; right:0;" href="#"><img src="images/close_btn.png" alt=""></a>-->
                <h3 class="litbox_title">
                    Hooray !!... Limos Found
                    <small>Just one more step and we are good to go</small>
                </h3>
                <form id="userdetail" method="post" action="<?php echo asset('savevister') ?>">
                    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                    <div class="clearfix">
                        <div class="col-sm-6">
                            <input type="text" placeholder="Please Enter your First Name" name="firstname">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" placeholder="Please Enter your Last Name" name="lastname">
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-12 email_space">
                            <input name="email" type="email" placeholder="Email Address">
                            <span>(When limos compete, you win! They'll privately bid for your business- giving you the best rate!)</span>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-12">
                            <input type="number" placeholder="Phone Number (Optional)" name="contact">
                        </div>
                    </div>
                    <div class="clearfix form_row">
                        <div class="col-sm-12">
                            <div class="inline_block c_label">Subscribe to our Newsletter</div>
                            <div class="onoffswitch inline_block">
                                <input type="checkbox" name="notification" class="onoffswitch-checkbox" id="onoffswitch1" value="1" checked>
                                <label class="onoffswitch-label" for="onoffswitch1">
                                    <div class="onoffswitch-inner"></div> 
                                    <span class="onoffswitch-switch"></span></label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-12">
                            <a  class="btn large see_result" href="#" onclick="submitform()">See My Results</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php require('include/footer.php'); ?>
        <style>
            #userdetail input.error {
                border-color:red;
            }
            .email_space input[type="email"]{
                margin-bottom: 0;
            }
            .email_space span{
                display: block;
                margin-bottom: 15px;
                font-size: 14px;
                    color: #4a80ec;
            }
        </style>
        <script type="text/javascript">

            function submitform() {
                $('#userdetail').submit();
            }
            $(document).ready(function() {

                $('#userdetail').validate({// initialize the plugin
                    rules: {
                        firstname: "required", lastname: "required",
                        email: {required: true,
                            email: true
                        }
                    },
                    messages: {
                        firstname: "",
                        email: "",
                        lastname: "",
                    }


                });

            });
        </script>

    </body>
</html>