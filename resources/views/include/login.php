<a style="position:absolute; top:0; right:0; cursor:pointer" onclick="$.fancybox.close()" ><img src="<?php echo asset('images/close_btn.png')?>" alt=""></a>
<div class="col-sm-12">
    <div id="loing_sinup" >
        <a class="login_trigger  btn" href="#" >Login</a>
        <a  href="#" class="btn active  register_trigger">Register</a>
        <div class="clear"></div>
    </div>
</div>
<form id="login" class="form-horizontal clearfix" role="form" method="POST" action="<?php echo asset('auth/login')?>" >
    <span class="alert danger " id="errormessage" style="display:none"> Invalid Email Or Password</span>
    <span class="alert danger " id="errorinactive" style="display:none">Your account is in process to approve</span>
    <input type="hidden" name="_token" value="<?php echo csrf_token() ?>" id ="logintoken">
    <div class="form-group"> 
        <label class="col-md-4 control-label">E-Mail Address</label>
        <div class="col-md-8">
            <input type="email" class="form-control" name="email" value="<?php
            if (isset($_POST['email'])) {
                echo $_POST['email'];
            }
            ?>" id="email_auth">
        </div> </div>
<div style="display:none;"id="loader1" ></div>
    <div class="form-group">
        <label class="col-md-4 control-label">Password</label>
        <div class="col-md-8">
            <input type="password" class="form-control" name="password" id="password_auth">
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" checked> Remember Me
                </label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-primary"  onclick="authenticate()">Login</button>

            <a class="btn btn-primary" href="<?php echo asset('password/email')?>">Forgot Your Password?</a>
        </div>
    </div>
</form>
<div id="register">
    <div class="register_contant form-horizontal clearfix">
        <p>Grow your business with Instalimos!</p>
        <p>Connect directly with customers online looking for limo service in your local market.</p>
        <p>Advertise your brand, your phone number and your email because it is your business and they are your customers!</p>
        <p>Don't get  caught up in expensive pay-per-clicks and other over priced advertising. We created this as an affordable alternative for you to acquire customers so you can focus on your business and grow your bottom line.</p>
        <p>Discover why operators are calling Instalimos "the best source for quality sales leads" today</p>
        <p>Let us help you grow your business.</p>
    </div>
    <div class="register_now">
         <a href="<?php echo asset('register')?>" class="btn btn-primary" >Register Now</a>
    </div>
</div>


<script type="text/javascript">
    function authenticate() {
    token =$('#logintoken').val();
    //alert(token);
        email = $('#email_auth').val();
        password = $('#password_auth').val();
        if ((!IsEmail(email))) {
            $('#email_auth').css('border-color', 'red');
        }
        else if (password == '') {
            $('#email_auth').css('border-color', 'black');
            $('#password_auth').css('border-color', 'red');
            password = $('#password_auth').val('');
        } else {
            $('#email_auth').css('border-color', 'black');
            $('#password_auth').css('border-color', 'black');
             $('#loader1').fadeIn();
            $.ajax({
                type: "GET",
                data: {"email": email, "password": password},
                url: "<?php echo asset('authenticate'); ?>",
                success: function(data) {
                    $("#loader1").fadeOut(1000);
                    if (data == 1) {
                  $('#logintoken').val(token);
                    
                    $("#login").submit();
                    }else if(data == 2){
                        $('#errorinactive').show();
                        $('#errormessage').hide();
                    }
                    else {
                         $('#errorinactive').hide();
                        $('#errormessage').show()
                    }
                }
            });

        }
    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function register() {
        name = $('#rname').val();
        password = $('#rpassword').val();
        cpassword = $('#rcpassword').val();
        email = $('#remail').val();

        if (name === '') {
            $('#rname').css('border-color', 'red');
        }
        else if ((!IsEmail(email))) {
            $('#rname').css('border-color', 'black');
            $('#remail').css('border-color', 'red');
        } else {
             $('#loader1').fadeIn();
            $.ajax({
                type: "GET",
                data: {"email": email},
                url: "<?php echo asset('authenticate_email'); ?>",
                success: function(data) {
                    $("#loader1").fadeOut(1000);
                    if (data) {
                        $('#errormessage2').hide();
                        if (password === '' || password.length < 6) {
                            $('#rname').css('border-color', 'black');
                            $('#remail').css('border-color', 'black');
                            $('#rpassword').css('border-color', 'red');
                        }
                        else if (cpassword.lenght < 6 || cpassword !== password) {
                            $('#rname').css('border-color', 'black');
                            $('#remail').css('border-color', 'black');
                            $('#rpassword').css('border-color', 'black');
                            $('#rcpassword').css('border-color', 'red');
                        } else {
                            $("#register").submit();
                        }

                    } else {
                        $('#errormessage2').show();
                        $('#remail').css('border-color', 'red');
                    }
                }
            });
        }


    }
</script> 