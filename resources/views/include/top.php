<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title><?php echo $title?></title>
	<!--<title></title>-->
        <meta name="description" content="Holy Limo! Check out this sweet ride I found on instalimos.com">
	<meta name="author" content="instalimo.com">
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
        <!-- Hotjar Tracking Code for instalimos.com -->
<script>
(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:94836,hjsv:5};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
        
        <script src="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&types=food&name=harbour&key=AIzaSyC19W1VRfIhBov7bbj6cfC6Ko_j0i7OYdw"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/base.css')?>">
         <link rel="stylesheet" type="text/css" href="<?php echo asset('css/fancybox.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/jquery.datepick.css')?>">
        <link rel="stylesheet" type="text/css" href="<?php echo asset('css/jquery.datetimepicker.css')?>">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo asset('images/favicon.ico')?>">
	<link rel="apple-touch-icon" href="<?php echo asset('images/apple-touch-icon.png')?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset('images/apple-touch-icon-72x72.png')?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('images/apple-touch-icon-114x114.png')?>">
	
        <!-- custom scrollbar stylesheet -->
        <link rel="stylesheet" href="<?php echo asset('css/jquery.mCustomScrollbar.css')?>">
        
         
        
        <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1657114144532523');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1657114144532523&ev=PageView&noscript=1"/>
    </noscript>
<!-- End Facebook Pixel Code -->

<script>
// ViewContent
// Track key page views (ex: product page, landing page or article)
fbq('track', 'ViewContent');


// Search
// Track searches on your website (ex. product searches)
fbq('track', 'Search');


// AddToCart
// Track when items are added to a shopping cart (ex. click/landing page on Add to Cart button)
fbq('track', 'AddToCart');


// AddToWishlist
// Track when items are added to a wishlist (ex. click/landing page on Add to Wishlist button)
fbq('track', 'AddToWishlist');


// InitiateCheckout
// Track when people enter the checkout flow (ex. click/landing page on checkout button)
fbq('track', 'InitiateCheckout');


// AddPaymentInfo
// Track when payment information is added in the checkout flow (ex. click/landing page on billing info)
fbq('track', 'AddPaymentInfo');


// Purchase
// Track purchases or checkout flow completions (ex. landing on "Thank You" or confirmation page)
fbq('track', 'Purchase', {value: '1.00', currency: 'USD'});


// Lead
// Track when a user expresses interest in your offering (ex. form submission, sign up for trial, landing on pricing page)
fbq('track', 'Lead');


// CompleteRegistration
// Track when a registration form is completed (ex. complete subscription, sign up for a service)
fbq('track', 'CompleteRegistration');


// CustomConversion
// 
fbq('track', 'CustomConversion');
</script>



    
</head>