<?php require('operator_includes/operator_header.php'); ?>
<div class="col-sm-9 equal_columns">
    <?php if (Session::has('success')) { ?>
        <div class="alert alert-success"><?php echo Session::get('success') ?></div>
    <?php } ?>                                                     <?php if (Session::has('error')) { ?>
        <div class="alert alert-success"><?php echo Session::get('error') ?></div>
    <?php } ?>
    <div class="clearfix">

        <div id="add_car_form">
            <form method="post" action="<?php echo asset('changepassword') ?>" id="passwordform">
                <input type="hidden" class="token" name="_token" value="<?php echo csrf_token() ?>">
                <div class="from_head">Account information</div>
                <div class="from_section">
                    <div class="row clearfix">
                        <div class="col-sm-4 ">
                            <label>Login Email<span>*</span></label>
                        </div>
                        <div class="col-sm-8 ">
                            <div class="custom_select">
                                <input type="email" readonly value="<?php echo $email ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="from_head">Change Password</div>
                <div class="from_section">
                    <div class="row clearfix">
                        <div class="col-sm-4 ">
                            <label>Current Password<span>*</span></label>
                        </div>
                        <div class="col-sm-8 ">
                            <div class="custom_select">
                                <input type="password" name="cpassword" id="cpassword">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-4 ">
                            <label>New Password<span>*</span></label>
                        </div>
                        <div class="col-sm-8 ">
                            <div class="custom_select">
                                <input type="password" name="password" id="password">
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-4 ">
                            <label>Confirm Password<span>*</span></label>
                        </div>
                        <div class="col-sm-8 ">
                            <div class="custom_select" >
                                <input type="password" name="passwordSecond" id="passwordSecond">
                            </div>
                        </div>
                    </div>
                    <button id="submitt" type="button" class="btn fr" >Save</button>
                    <div class="clear"></div>
                </div>

            </form>

        </div>
    </div>
</div>
</div>
</div>
</div>
<?php require('operator_includes/operator_footer.php'); ?>
<script type="text/javascript">
    $('#cpassword').focusout(function() {
        password = $('#cpassword').val();
        $.ajax({
            type: "GET",
            url: "<?php echo asset('checkpassword'); ?>",
            data: {"password": password},
            success: function(data) {
                if (data) {
                    $('#cpassword').css("border", "1px solid black");
                }
                else {
                    $('#cpassword').css("border", "1px solid red");
                }
            }
        });

    });




    $('#password').focusout(function() {
        password = $('#password').val();
        if (password.length >= 4) {
            $('#password').css("border", "1px solid black");
        } else {
            $('#password').css("border", "1px solid red");
        }
    });


    $('#passwordSecond').focusout(function() {
        password = $('#password').val();
        cpassword = $('#passwordSecond').val();
        if (password === cpassword && password.length >= 4) {

            $('#passwordSecond').css("border", "1px solid black");
        } else {
            $('#passwordSecond').css("border", "1px solid red");
        }
    });



    $('#submitt').click(function() {
        password = $('#cpassword').val();
        $.ajax({
            type: "GET",
            url: "<?php echo asset('checkpassword'); ?>",
            data: {"password": password},
            success: function(data) {
                if (data) {
                    password = $('#password').val();
                    if (password.length < 4) {
                        $('#password').css("border", "1px solid red");
                    } else {
                        cpassword = $('#passwordSecond').val();
                        if (password === cpassword) {
                            $('#passwordform').submit();
                        }
                        else {
                            $('#passwordSecond').css("border", "1px solid red");
                        }
                    }
                }
                else {
                    $('#cpassword').css("border", "1px solid red");
                }
            }
        });
    });



</script>
</body>
</html>