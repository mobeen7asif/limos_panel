<?php require('operator_includes/operator_header.php');
//echo '<pre>'; 
//print_r($messages);exit;
?>

        <div class="col-sm-9 equal_columns">
        	<div class="dashboard_header clearfix">
            	<div class="from_head fl">MESSAGES RECEIVED</div>
                
            </div>
            <div class="table_theme">
            	<table class="rwd-table">
                  <tr>
                    <th width="120">Name</th>
                    <th width="250">Email</th>
                    <th width="120">Contact</th>
                    <th>Message</th>
                    <th width="180">Date Searched</th>
                  </tr>
                  <?php foreach ($messages as $message):
                      $contact = substr($message->contact,0,0)."(".substr($message->contact,0,3).") ".substr($message->contact,3,3)."-".substr($message->contact, 6); 
                      ?>
                  <tr>
                      <td data-th="Name"><span><?php echo $message->sender_name?></span></td>
                    <td data-th="Email"><span><?php echo $message->sender_email?></span></td>
                    <td data-th="Contact"><span><?php if($message->contact) {echo $contact;}else{echo 'N/A';}?></span></td>
                    <td data-th="Message"><span><?php if($message->message) {echo $message->message;}else{echo 'N/A';}?></span></td>
                    <td data-th="Date Searched"><span><?php echo $message->message_date?></span></td>
                  </tr>
                  <?php endforeach;?>
                </table>
            </div>
            <?php echo $messages->render(); ?>
        </div>
    </div>
</div>
</div>
<?php require('operator_includes/operator_footer.php'); ?>
    
</body>
</html>