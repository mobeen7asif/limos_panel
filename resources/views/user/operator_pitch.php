<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>InstaLimo| Home</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" type="text/css" href="css/base.css">
    <script>
		function loadCSS(e,t,n){"use strict";var i=window.document.createElement("link");var o=t||window.document.getElementsByTagName("script")[0];i.rel="stylesheet";i.href=e;i.media="only x";o.parentNode.insertBefore(i,o);setTimeout(function(){i.media=n||"all"})}
		
    </script>
    <script>
		loadCSS("css/jquery.datepick.css");
		loadCSS("css/fancybox.css");
		loadCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700");
    </script>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	
   
    
    
</head>
<body>
<div id="lightbox_layout" class="grow_bg">
<a style="display:none" class="fancybox_auto" href="#light_box" >Inline</a>
<div id="light_box" class="grow_popup">
<a style="position:absolute; top:0; right:0;" href="#"><img src="images/close_btn.png" alt=""></a>
		<h3 class="litbox_title">
        	Grow Your Business With InstaLimos
            <small>Connect with customers online looking for limo service in your local market!</small>
        </h3>
        <form>
            <ul class="circle_list">
            	<li>
                	<span class="number">1</span>
                    <div class="inline_block">Advertise your brand, your email, and your phone number because it’s your company and they are your customers!</div>
                </li>
                <li>
                	<span class="number">2</span>
                    <div class="inline_block">Discover why operators are quickly calling Instalimos
       <span class="quote">“The best source for qualified sales leads.”</span></div>
                </li>
       			<li>
                	<span class="number">3</span>
                    <div class="inline_block">Let us help you grow your business now.</div>
                </li>
            </ul>
            <div class="clearfix">
            	<div class="col-sm-12">
                	<a class="btn large" href="#">JOIN NOW !!</a>
                </div>
            </div>
        </form>
	</div>
</div>





  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bx.js"></script>
    <script defer  src="js/jquery.plugin.js"></script>
    <script defer  src="js/jquery.datepick.js"></script>
    <script defer  src="js/jquery.fancybox.pack.js"></script>
    <script defer  src="js/jquery.nouislider.all.min.js"></script>
    <script defer  src="js/custom.js"></script>
    
</body>
</html>