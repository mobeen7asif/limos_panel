<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>InstaLimo| Home</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" type="text/css" href="css/base.css">
    <script>
		function loadCSS(e,t,n){"use strict";var i=window.document.createElement("link");var o=t||window.document.getElementsByTagName("script")[0];i.rel="stylesheet";i.href=e;i.media="only x";o.parentNode.insertBefore(i,o);setTimeout(function(){i.media=n||"all"})}
    </script>
    <script>
		loadCSS("css/jquery.datepick.css");
		loadCSS("css/fancybox.css");
		loadCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700");
    </script>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
</head>
<body>
<div id="dasboard">
<header id="header" class="relv">
  <div class="logo_navbar clearfix">
    	<div class="container-fluid">
    	   <a id="logo" href="#"><img src="images/logo.png" alt="instalimo" height="38" width="216"></a>
            <a class="menu_trigger" href="#"><img src="images/menu.png" alt=""></a>
            <nav id="main_nav">
            	<ul>
               	  <li><a href="#">BLOG</a></li>
                    <li><a href="#">LOGIN</a></li>
                    <li><a href="#">List Your Limo !</a></li>
                </ul>
            </nav>
    	</div>
  </div>
</header>
<div id="dasboard_main">
	<div class="clearfix">
    	<div class="col-sm-3 equal_columns">
        	<div id="sidebar_dasboard">
            	<div class="side_scroll"></div>
            	<ul class="side_menus clearfix">
                	<li class="active">
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">Create/Edit</div>
                            <div class="h2 blue_head">VEHICLES</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">ADD ABOUT THE COMPANY</div>
                            <div class="h2 blue_head">COMPANY PROFILE</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">KNow ABout Leads</div>
                            <div class="h2 blue_head">LEADS RECIEVED</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">Recieved Messages</div>
                            <div class="h2 blue_head">INBOX</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">SEnt Messages</div>
                            <div class="h2 blue_head">OUTBOX</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">KNOW ABOUT ACCOUNT</div>
                            <div class="h2 blue_head">ACCOUNT INFORMATION</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">DEtails of Bills</div>
                            <div class="h2 blue_head">BILLING</div>
                       </a>
                    </li>
                </ul>
                <ul class="side_menus clearfix logout">
                    <li>
                    	<span class="blue_slide"></span>
                        <a href="#">
                                <div class="small_head">Done for now?</div>
                                <div class="h2 blue_head">LOG OUT</div>
                       </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-9 equal_columns">
        	<div class="dashboard_header clearfix">
            	<div class="from_head fl">LEADS RECEIVED</div>
                <a class="fr btn" href="#"><strong>SAVE</strong></a>
            </div>
            <div class="table_theme">
            	<table class="rwd-table">
                  <tr>
                    <th>Name</th>
                    <th>Pax</th>
                    <th>Service</th>
                    <th>Date Searched</th>
                  </tr>
                  <tr>
                    <td data-th="Name">Abdullah Ali</td>
                    <td data-th="Pax">23,000</td>
                    <td data-th="Service">Hussnain Raza</td>
                    <td data-th="Date Searched">12/04/2015</td>
                  </tr>
                  <tr>
                    <td data-th="Name">Imran Shahid</td>
                    <td data-th="Pax">8,000</td>
                    <td data-th="Service">Abdullah Ali</td>
                    <td data-th="Date Searched">12/04/2015</td>
                  </tr>
                  <tr>
                    <td data-th="Name">Abdullah Ali</td>
                    <td data-th="Pax">23,000</td>
                    <td data-th="Service">Imran Shahid</td>
                    <td data-th="Date Searched">12/04/2015</td>
                  </tr>
                  <tr>
                    <td data-th="Name">Sulaiman Naeem</td>
                    <td data-th="Pax">29,000</td>
                    <td data-th="Service">Abdullah Ali</td>
                    <td data-th="Date Searched">12/04/2015</td>
                  </tr>
                  <tr>
                    <td data-th="Name">Hussnain Raza</td>
                    <td data-th="Pax">23,000</td>
                    <td data-th="Service">Sulaiman Naeem</td>
                    <td data-th="Date Searched">12/04/2015</td>
                  </tr>
                  <tr>
                    <td data-th="Name">Sulaiman Naeem</td>
                    <td data-th="Pax">3,000</td>
                    <td data-th="Service">Hussnain Raza</td>
                    <td data-th="Date Searched">12/04/2015</td>
                  </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<footer id="footer">
	<div class="container-fluid">
    	<div class="row clearfix">
    		<div class="col-sm-5 ft_section"><img src="images/logo.png" alt=""></div>
	        <div class="col-sm-7 ft_section text-right_sm">
            	<div class="ft_social">
            		<a href="#"><img src="images/fb.png" alt=""></a>
                	<a href="#"><img src="images/twitter.png" alt=""></a>
               		 <a href="#"><img src="images/rss.png" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
                <!--<div>Designed by: <a target="_blank" href="http://vengile.com/">Vengile IT Solution</a></div>-->
            </div>
        </div>
  </div>
</footer>
</div>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bx.js"></script>
    <script defer  src="js/jquery.plugin.js"></script>
    <script defer  src="js/jquery.datepick.js"></script>
    <script defer  src="js/jquery.fancybox.pack.js"></script>
    <script defer  src="js/jquery.nouislider.all.min.js"></script>
    <script defer  src="js/custom.js"></script>
    
</body>
</html>