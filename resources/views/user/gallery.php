<?php require('operator_includes/operator_header.php'); ?>

<div class="col-sm-9">
    <?php if (Session::has('success')) { ?>
        <div class="alert alert-success"><?php echo Session::get('success') ?></div>
    <?php } ?>                                                     <?php if (Session::has('error')) { ?>
        <div class="alert  danger"><?php echo Session::get('error') ?></div>
    <?php } ?>
    <div class="clearfix">
        <div id="gallery">
            <div class="from_head">Photos</div>
            <div class="from_section ">
                <div class="from_section ">
                    <?php foreach ($photos as $photo): ?>
                        <div class="col-md-4  gallery_file">
                            <div class="relv">
                                <input id="file<?php echo $photo->image_id ?>" required accept=".png, .jpg"  class="<?php echo $photo->image_id ?>" type="file"   name="veh_image" onchange="readURL(this, '<?php echo $photo->image_id ?>')"/>

                                <button class="btn" ><img src="<?php echo asset('images/upload.png')?>"></button>
                                <a href="<?php echo asset('images/limo_image/' . $photo->image_name) ?>" class="btn cancel g_fancybox"><img src="<?php echo asset('images/preview.png')?>"></a>
                                <img id="changeimg<?php echo $photo->image_id ?>" src="<?php echo asset('images/limo_image/' . $photo->image_name) ?>">
                            </div>
                            <div class=" clearfix custom_inputs">
                                <input id="<?php echo asset('images/limo_image/' . $photo->image_id) ?>" type="radio" name="radio" value="<?php echo asset('images/limo_image/' . $photo->image_id) ?>" onclick="makedefault('<?php echo $photo->image_id ?>', '<?php echo $photo->vehicals_id ?>')" <?php if ($photo->main_img == 1) { ?>checked=""<?php } ?>>
                                <label for="<?php echo asset('images/limo_image/' . $photo->image_id) ?>"><span>&nbsp;</span><div class="inline_block">Make Cover Photo</div> </label>
                            </div>
                            <div><span id="error<?php echo $photo->image_id ?>" class="alert danger" style="display:none;"></span></div>
                        </div>

                    <?php endforeach; ?>
                    <div class="clear"></div>


                </div>
                <div class="clear"></div>
            </div>
            <?php // foreach ($photos as $photo):?>
            <!--          <div class="g_img">
                          <a class="g_fancybox"  rel="gallery<?php // echo asset('images/limo_image/'.$photo->image_id) ?>" href="<?php // echo asset('images/limo_image/'.$photo->image_name) ?>" >
                              <img src="<?php // echo asset('images/limo_image/'.$photo->image_name) ?>">
                        </a>
                          <a href="<?php // echo asset('delete_image/'.$photo->image_id.'/'.$photo->vehicals_id) ?>" class="delete" id="delete" onclick="return confirm('Are You sure to delete')">X</a>
                        <div class=" clearfix custom_inputs">
                            <input id="<?php // echo asset('images/limo_image/'.$photo->image_id) ?>" type="radio" name="radio" value="<?php // echo asset('images/limo_image/'.$photo->image_id) ?>" onclick="makedefault('<?php // echo $photo->image_id ?>','<?php // echo $photo->vehicals_id ?>')" <?php // if($photo->main_img == 1){ ?>checked=""<?php // } ?>>
                            <label for="<?php // echo asset('images/limo_image/'.$photo->image_id) ?>"><span>&nbsp;</span><div class="inline_block">Make Cover Photo</div> </label>
                        </div>
                    </div>-->
            <?php // endforeach;?>

        </div>
    </div>
</div>
</div>
</div>
</div>
<?php require('operator_includes/operator_footer.php'); ?>
<script type="text/javascript">
    function makedefault(id, v_id) {
        $.ajax({
            type: "GET",
            url: "<?php echo asset('makeimage_default'); ?>",
            data: {"image_id": id, "vehicals_id": v_id},
            success: function(data) {
            }});
    }


    function readURL(input, id) {
        file = input.files[0];
        var reader = new FileReader();
        var image = new Image();
        reader.readAsDataURL(file);
        reader.onload = function(_file) {
            image.src = _file.target.result;              // url.createObjectURL(file);
            image.onload = function() {
                var w = this.width,
                        h = this.height,
                        s = ~~(file.size / 1024) + 'KB';
              if (h < 650 || w < 1500) {
//                                
                    $("#file" + id).val("");
                    $('#error' + id).show();
                    $('#error' + id).text('Image size should greater than 650 * 1500')
                } else {
//                              alert('asdasasda')
                    formData = new FormData();
                    formData.append("veh_image", file);
                    formData.append("imageid", id);
                    $("#imageloader").fadeIn();
                    $.ajax({
                        url: "<?php echo asset('upload_image'); ?>",
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            if (data) {
                                $('#changeimg' + id).attr('src', image.src);
                                $('#error' + id).hide();
                                $("#file" + id).val("");
                                $("#imageloader").fadeOut(1000);
                            } else {
                                $("#file" + id).val("");
                                $('#error' + id).show();
                                $('#error' + id).text('Sorry Some Thing Went Wrong');
                                $("#imageloader").fadeOut(1000);
                            }
                        }
                    });

                }
            };
            image.onerror = function() {

                $("#file" + id).val("");
                $('#error' + id).show();
                $('#error' + id).text('Invalid file type: ' + file.type)

            };
        };
    }


</script>
<div id="imageloader" style="display: none;"></div>
</body>
</html>