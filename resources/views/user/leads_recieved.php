<?php require('operator_includes/operator_header.php'); ?>
        <div class="col-sm-9 equal_columns">
        	<div class="dashboard_header clearfix">
            	<div class="from_head fl">LEADS RECEIVED</div>
                
            </div>
            <div class="table_theme">
            	<table class="rwd-table">
                  <tr>
                    <th width="120">Name</th>
                    <th width="120">Event type</th>
                    <th width="100">Contact</th>
                    <th width="180">Searched Date</th>
                    <th width="180">Service Date</th>
                    <th>Passengers</th>
                    <th>Pick up address</th>
                    
                  </tr>
                  <?php foreach ($leads as $lead):
                      $contact = substr($lead->contact,0,0)."(".substr($lead->contact,0,3).") ".substr($lead->contact,3,3)."-".substr($lead->contact, 6); 
                      ?>
                  <tr>
                    <td data-th="Name"><?php echo $lead->firstname. ' '.$lead->lastname?></td>
                    <td data-th="Email"><?php echo $lead->occian_type ?></td>
                    <td data-th="Contact"><?php if($lead->contact) {echo $contact;}else{echo 'N/A';}?></td>
                   <td data-th="Date Searched"><?php echo $lead->created_at?></td>
                   <td data-th="Date Searched"><?php echo $lead->search_date?></td>
                    <td data-th="Date Searched"><?php echo $lead->passengers?></td>
                     <td data-th="Date Searched"><?php echo $lead->picklocation?></td>
                      
                  </tr>
                  <?php endforeach;?>
                </table>
            </div>
            <?php echo $leads->render(); ?>
        </div>
    </div>
</div>
</div>
<?php require('operator_includes/operator_footer.php'); ?>
    
</body>
</html>