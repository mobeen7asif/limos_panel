<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

 Hi <?php echo $name?>,<br>
 <br>
Thank you for applying to Instalimos. We're excited to help you grow your business and bring more customers your way. Please hang tight as someone from our team reviews your application. <br>
This should only take one to two business days. Once approved we'll send you another email and we'll reach out by phone.<br>
Talk soon,<br>
 <br>
 <strong>The Instalimos team</strong>
</html>