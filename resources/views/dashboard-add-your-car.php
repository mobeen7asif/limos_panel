<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>InstaLimo| Home</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
	<link rel="stylesheet" type="text/css" href="css/base.css">
    <script>
		function loadCSS(e,t,n){"use strict";var i=window.document.createElement("link");var o=t||window.document.getElementsByTagName("script")[0];i.rel="stylesheet";i.href=e;i.media="only x";o.parentNode.insertBefore(i,o);setTimeout(function(){i.media=n||"all"})}
		
    </script>
    <script>
		loadCSS("css/jquery.datepick.css");
		loadCSS("css/fancybox.css");
		loadCSS("http://fonts.googleapis.com/css?family=PT+Sans:400,700");
    </script>
    
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	
   
    
    
</head>
<body>
<div id="dasboard">
<header id="header" class="relv">
  <div class="logo_navbar clearfix">
    	<div class="container-fluid">
    		<a id="logo" href="#"><img src="images/logo.png" alt="instalimo" height="38" width="216"></a>
            <a class="menu_trigger" href="#"><img src="images/menu.png" alt=""></a>
            <nav id="main_nav">
            	<ul>
               	  <li><a href="#">BLOG</a></li>
                    <li><a href="#">LOGIN</a></li>
                    <li><a href="#">List Your Limo !</a></li>
                </ul>
            </nav>
    	</div>
  </div>
</header>
<div id="dasboard_main">
	<div class="clearfix">
    	<div class="col-sm-3 side_shadow equal_columns">
        	<div id="sidebar_dasboard">
            	<div class="side_scroll"></div>
            	<ul class="side_menus clearfix">
                	<li class="active">
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">Create/Edit</div>
                            <div class="h2 blue_head">VEHICLES</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">ADD ABOUT THE COMPANY</div>
                            <div class="h2 blue_head">COMPANY PROFILE</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">KNow ABout Leads</div>
                            <div class="h2 blue_head">LEADS RECIEVED</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">Recieved Messages</div>
                            <div class="h2 blue_head">INBOX</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">SEnt Messages</div>
                            <div class="h2 blue_head">OUTBOX</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">KNOW ABOUT ACCOUNT</div>
                            <div class="h2 blue_head">ACCOUNT INFORMATION</div>
                       </a>
                    </li>
                    <li>
                    	<span class="blue_slide"></span>
                    	<a href="#">
                        	<div class="small_head">DEtails of Bills</div>
                            <div class="h2 blue_head">BILLING</div>
                       </a>
                    </li>
                </ul>
                <ul class="side_menus clearfix logout">
                    <li>
                    	<span class="blue_slide"></span>
                        <a href="#">
                                <div class="small_head">Done for now?</div>
                                <div class="h2 blue_head">LOG OUT</div>
                       </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-9 equal_columns">
            <div id="add_car_form">
                <form>
                    <div class="from_head">Booking Type Limit</div>
                    <div class="from_section">
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Limo Type<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <label>
                                        <select>
                                            <option selected> Select Type </option>
                                            <option>Select Type</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="from_head">About Your Limo</div>
                    <div class="from_section">
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Name of your vehicle (Be fun with it and give it personality)<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="spacer_20"></div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Description of your vehicle (sell it like you would on the phone)<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <textarea></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 ">
                                <label>Rules<span>*</span></label>
                            </div>
                            <div class="col-sm-8 ">
                                <div class="custom_select">
                                    <textarea></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="from_head">Specifications</div>
                    <div class="from_section">
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6">
                                <div class=" clearfix custom_inputs">
                                    <input id="1" type="checkbox" name="radio" value="1" >
                                    <label for="1"><span>&nbsp;</span><div class="inline_block">iPhone Hookup</div> </label>
                                </div>
                                <div class=" clearfix custom_inputs">
                                    <input id="2" type="checkbox" name="radio" value="1" >
                                    <label for="2"><span>&nbsp;</span><div class="inline_block">Dance Poles</div> </label>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class=" clearfix custom_inputs">
                                    <input id="3" type="checkbox" name="radio" value="1" >
                                    <label for="3"><span>&nbsp;</span><div class="inline_block">Wifi</div> </label>
                                </div>
                                <div class=" clearfix custom_inputs">
                                    <input id="4" type="checkbox" name="radio" value="1" >
                                    <label for="4"><span>&nbsp;</span><div class="inline_block">Bar/Beverages <small>(Upon Request)</small></div> </label>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class=" clearfix custom_inputs">
                                    <input id="5" type="checkbox" name="radio" value="1" >
                                    <label for="5"><span>&nbsp;</span><div class="inline_block">Monoroof/Sunroof</div> </label>
                                </div>
                                <div class=" clearfix custom_inputs">
                                    <input id="6" type="checkbox" name="radio" value="1" >
                                    <label for="6"><span>&nbsp;</span><div class="inline_block">Child Carseat <small>(Upon Request)</small></div> </label>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div class=" clearfix custom_inputs">
                                    <input id="7" type="checkbox" name="radio" value="1" >
                                    <label for="7"><span>&nbsp;</span><div class="inline_block">TV</div> </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="from_head">Details</div>
                    <div class="from_section">
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                 <label>Booking Mode</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                 <label>Seats (this refers to maximum number of people allowed)</label>
                            </div>
                            <div class="col-sm-8">
                                 <div class="custom_select">
                                    <label>
                                        <select>
                                            <option selected> 3 Passengers </option>
                                            <option> 4 Passengers</option>
                                            <option> 5 Passengers</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                 <label>Price (the price will be in USD*</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                 <label>Service Hours</label>
                            </div>
                            <div class="col-sm-8">
                                <div class="row clearfrix mini_slect relv">
                                    <div class="col-sm-5 text-right-sm">
                                        <div class="custom_select">
                                            <label>
                                                <select>
                                                    <option selected>AM </option>
                                                    <option> 12:00</option>
                                                    <option> 12:00</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="custom_select">
                                            <label>
                                                <select>
                                                    <option selected>AM </option>
                                                    <option> 12:00</option>
                                                    <option> 12:00</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 divider">-</div>
                                    <div class="col-sm-5  text-right-sm fr_sm">
                                        <div class="custom_select">
                                            <label>
                                                <select>
                                                    <option selected>AM </option>
                                                    <option> 12:00</option>
                                                    <option> 12:00</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="custom_select">
                                            <label>
                                                <select>
                                                    <option selected>AM </option>
                                                    <option> 12:00</option>
                                                    <option> 12:00</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>Minimum hours for rent<span>*</span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>Maximum hours for rent<span>*</span></label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>Cancellation Policy<span>*</span></label>
                            </div>
                            <div class="col-sm-8">
                                <div class="custom_select">
                                    <label>
                                        <select>
                                            <option selected> </option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="from_head">Address</div>
                    <div class="from_section">
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>Country</label>
                            </div>
                            <div class="col-sm-8">
                                <div class="custom_select">
                                    <label>
                                        <select>
                                            <option selected> United States of America </option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>State</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>City</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                         <div class="row clearfix">
                            <div class="col-sm-4">
                                <label>Address</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text">
                            </div>
                        </div>
                    </div>
                    <div class="from_head">Photos</div>
                    <div class="from_section ">
                        <div id="upload_photos" class="clearfix col-md-offset-4">
                            <div class="form-group clearfix">
                                <input id="file-5" class="file" type="file" multiple data-preview-file-type="any" data-upload-url="#" data-preview-file-icon="">
                            </div>
                           
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>
</div>
<footer id="footer">
	<div class="container-fluid">
    	<div class="row clearfix">
    		<div class="col-sm-5 ft_section"><img src="images/logo.png" alt=""></div>
	        <div class="col-sm-7 ft_section text-right_sm">
            	<div class="ft_social">
            		<a href="#"><img src="images/fb.png" alt=""></a>
                	<a href="#"><img src="images/twitter.png" alt=""></a>
               		 <a href="#"><img src="images/rss.png" alt=""></a>
                </div>
                <div>Copyright © 2015  ALL rights reserved.</div>
                <!--<div>Designed by: <a target="_blank" href="http://vengile.com/">Vengile IT Solution</a></div>-->
            </div>
        </div>
  </div>
</footer>
</div>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bx.js"></script>
    <script defer  src="js/jquery.plugin.js"></script>
    <script defer  src="js/jquery.datepick.js"></script>
    <script defer  src="js/jquery.fancybox.pack.js"></script>
    <script defer  src="js/jquery.nouislider.all.min.js"></script>
    <script defer src="js/fileinput.js" type="text/javascript"></script>
    <script defer src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
    <script defer  src="js/custom.js"></script>
    
</body>
</html>