<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <?php
    require('include/top.php');
    
    ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script type="text/javascript">
   var componentForm = {
            country: 'long_name'
        };
    function initializeloc() {
  // Create the autocomplete object, restricting the search
  // to geographical location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('address1')),
      { types: ['(cities)'] });
      google.maps.event.addListener(autocomplete, 'place_changed', function() {
                fillInAddress();
            });
 autocomplete2 = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('address2')),
      { types: ['(cities)'] });
}
function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
    function geolocateaddress1() {
  if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
          var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
function geolocateaddress2() {
  if (navigator.geolocation) {
         navigator.geolocation.getCurrentPosition(function(position) {
          var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
          var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
    </script>
    <body onload="initializeloc()">
        <?php require('include/script.php'); ?>
        <div id="dasboard">
            <header id="header" class="relv">
                <div class="logo_navbar clearfix">
                    <div class="container-fluid clearfix">
                        <a id="logo" href="<?php echo asset('/')?>"><img src="<?php echo asset('images/logo.png') ?>" alt="instalimo" height="38" width="216"></a>
                        <a class="menu_trigger" href="#"><img src="images/menu.png" alt=""></a>
                        <nav id="main_nav">
                            <ul>
                                <li><a href="#">BLOG</a></li>
                                <?php if (Illuminate\Support\Facades\Auth::User()) { ?>
                                    <li><a href="<?php echo asset('logout') ?>">Logout</a></li>
                                    <li><a href="<?php echo asset('dashboard') ?>">List Your Limo !</a></li>   
                                <?php } else { ?>
                                    <li><a class="fancybox "  href="#light_boxs">LOGIN</a></li>
                                    <li><a class="fancybox "  href="#light_boxs">List Your Limo !</a></li>   
                                <?php } ?>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
            <div class="show_register">
                <h3 class="litbox_title">Instalimos Operator Application</h3>
                <form method="post" action="<?php echo asset('register') ?>" id="registerpage">
                    <div class="register_panel">
                        <h2>Company & Contact Information</h2>
                        <hr>
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Company legal name</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="comp_name" value=""> <em>*</em>
                                    </div>
                                </div>
                             </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Preferred contact's last name</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="comp_l_name" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">    
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Public Display name (DBA name)</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="dba_name" value="">
                                        <span>(If different from legal name)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> 
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Preferred contact's phone Number</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="comp_mobile" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Reservation Phone number</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="comp_phone" value=""> <em>*</em>
                                    </div>
                                </div>
                             </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Number of insured vehicles</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="insured_vehicals" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Company Website</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="comp_biz_url" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Your business started</label>
                                    <div class="col-md-7">
                                        <input id="datepicker" readonly="" type="text" class="form-control" name="biz_date" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Preferred contact's first name</label> 
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="comp_f_name" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Company Email</label>
                                    <div class="col-md-7">
                                        <input type="email" class="form-control" name="comp_email" value=""> <em>*</em>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="register_panel">
                        <h2>Transportaion Permit Information</h2>
                        <hr>
                        <h3>Please provide your permits: (National, State/Provider, and/or Local)</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Permit Number</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="permit_number" value=""> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Name of the issuing jurisdicion/authority</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="autherity_name" value="">
                                        <span>Ex: US DOT/Colorado/Quebec</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="register_panel">
                        <h2>Business Address & Preferred Contact Info</h2>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Address Line 1</label> 
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="address_1" value="" id="address1" onFocus="geolocateaddress1()">  <em>*</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Country</label> 
                                    <div class="col-md-7">
                                        <input type="hidden" class="form-control" name="countr" value="" id="country" readonly >
                                        <select class="limo_type" name="country_1">
                                            <?php foreach ($countries as $country):?>
                                            <option value="<?php echo $country->country_name?>" <?php if($country->country_name == 'United States') {?> selected <?php }?>><?php echo $country->country_name?></option>
                                            <?php endforeach;?>
                                        </select> <em>*</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">State/Province</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="state_1" value=""> <em>*</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Do you/have you advertise(d) anywhere else online? If So where.</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="advertised_bef" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Address Line 2</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="address_2" value="" id="address2" onFocus="geolocateaddress2()">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Zip/Postal</label> 
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="zipcode" value=""> <em>*</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">City</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="city" value=""> <em>*</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">How did you hear about Instalimos</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="aboutus" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="register_panel">
                        <h2>Create Your Account & Password</h2>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account Login Email</label> 
                                    <div class="col-md-7">
                                        <input type="email" class="form-control" name="email" value="" id="email">  <em>*</em>
                                        <span class="alert danger" id="errormessage" style="display:none">The Given Email was already with Us</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Create Password</label> 
                                    <div class="col-md-7">
                                        <input type="password" class="form-control" name="password" value="" id="password"> <em>*</em>
                                        <span>At least: 8 chars, 1 special char & 1 number</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Confirm Password</label> 
                                    <div class="col-md-7">
                                        <input type="password" class="form-control" name="confrim_password" value="" id="confrim_password"> <em>*</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">I agree to the Terms Of Services</label> 
                                    <div class="col-md-7">
                                        <input id="" type="checkbox" name="agree"> 
                                        <span style="display: inline-block;"><a href="<?php echo asset('terms') ?>" target="_blank">Read Terms and Services</a></span>
                                        <!--<label class="checkbox" for="Option"> &nbsp </label>-->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-7 col-md-offset-4">
                                        <input type="submit" class="btn btn-primary" value="Submit" id="submitform">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="light_boxs" class="light" style="display:none">
                <?php include 'include/login.php'; ?>
            </div>
        </div>
        <div class="clear"></div>
        <?php require('include/footer.php'); ?> 
        <style>
            input.error {
                border:solid 1px red !important;
            }
            #registerpage checkbox.error {
               background-color: red;
            }
            #registerpage label.error {
                width: auto;
                display: inline;
                color:red;
                font-size: 16px;
                float:right;
            }
        </style>
        <script type="text/javascript">

            $(document).ready(function() {
                $('#registerpage').validate({// initialize the plugin
                    rules: {
                        comp_f_name: "required",
                        biz_date: "required",
//                        insured_vehicals: "required",
                        comp_l_name: "required",
                        comp_biz_url: "required",
                        comp_name: "required",
                        address_1: "required",
                        agree: "required",
                        country_1: "required",
                        city:"required",
                        state_1:"required",
                        email: {
                            required: true,
                            email: true
                        },
                        comp_email:{
                            required: true,
                            email: true 
                        },
                        comp_mobile: {
                            required: true,
                            digits: true,
                            minlength: 8,
                            maxlength: 14
                        },
                        insured_vehicals: {
                            required: true,
                            digits: true
                        },
                        comp_phone: {
                            required: true,
                            digits: true,
                            minlength: 8,
                            maxlength: 14
                        },
                        zipcode: {
                            required: true,
                            digits: true,
                            minlength: 5,
                            maxlength: 5
                        },
                        password: {
                            required: true,
//                            alphanumeric:true,
                            minlength: 8
                        },
                        confrim_password: {
                            required: true,
                            minlength: 8,
                            equalTo: "#password"
                        },
                    },
                    messages: {
                        comp_f_name: "",
                        city: "",
                        biz_date: "",
                        state_1:"",
                        insured_vehicals: "Should be Number",
                        comp_mobile: "must be a number and minimun 8 digits",
                        comp_l_name: "",
                        comp_biz_url: "",
                        comp_phone: "must be a number and minimun 8 digits",
                        comp_name: "",
                        address_1: "",
                        country_1: "",
                        zipcode: "Should be 5 digits",
                        agree: "*",
                        comp_email:"",
                        password: {
                            required: "",
                            minlength: "Your password must be at least 8 characters long"
                        },
                        email: "",
                        confrim_password: {
                            required: "",
                            minlength: "Your password must be at least 8 characters long",
                            equalTo: "Please enter the same password as above"
                        }
                    }


                });
                $('#email').focusout(function() {
                    email = $(this).val();
                    $('#regloader').fadeIn();
                    $.ajax({
                        type: "GET",
                        data: {"email": email},
                        url: "<?php echo asset('authenticate_email'); ?>",
                        success: function(data) {
                            $("#regloader").fadeOut(1000);
                            if (data) {
                                $('#errormessage').hide();
                                $('#submitform').attr('disabled', false);
                            } else {
                                $('#errormessage').show();
                                $('#email').css('border-color', 'red');
                                $('#submitform').attr('disabled', true);
                            }
                        }
                    });
                });
            });
            $('#password').focusout(function() {
                passowrd2 = $(this).val();
                if (! passwordre (passowrd2)) {
                    $('#submitform').attr('disabled', true);
                    $('#password').css("border","solid 1px red");
                } else {
                    $('#submitform').attr('disabled', false);
                    $('#password').css("border","solid 1px gray");
                }
            }); 
            function passwordre(password) {
                       var regex =/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
                        return regex.test(password);
                    }
            
        </script>
        <div style="display:none;"id="regloader" ></div>
    </body>
</html>