<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <?php require('include/top.php'); ?>
    <body>
        <?php require('include/script.php'); ?>
        <div id="lightbox_layout">
            <a style="display:none" class="fancybox_auto" href="#light_box" >Inline</a>
            <div id="light_box">
            <!--<a style="position:absolute; top:0; right:0;" href="#"><img src="images/close_btn.png" alt=""></a>-->
                <h3 class="litbox_title">
                    Forget Password
                    <small>Just one more step Recover Password</small>
                </h3>
                <form class="form-horizontal" role="form" method="POST" action="<?php echo asset('password/email')?>">
						<input type="hidden" name="_token" value="<?php echo csrf_token() ?>">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Send Password Reset Link
								</button>
							</div>
						</div>
					</form>
            </div>
        </div>
        <?php require('include/footer.php'); ?>
        <style>
            #userdetail input.error {
                border-color:red;
            }
        </style>
        <script type="text/javascript">

            function submitform() {
                $('#userdetail').submit();
            }
            $(document).ready(function() {

                $('#userdetail').validate({// initialize the plugin
                    rules: {
                        firstname: "required", lastname: "required",
                        email: {required: true,
                            email: true
                        }
                    },
                    messages: {
                        firstname: "",
                        email: "",
                        lastname: "",
                    }


                });

            });
        </script>

    </body>
</html>