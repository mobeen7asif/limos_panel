<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <?php require('include/top.php'); ?>
    <?php require('include/googlelocation.php'); ?>
    <body onload="initialize()">
        <div id="home_slider">
            <div class="bxslider">
                <div class="slide" style="background-image:url(images/slide.jpg);"></div>
                <div class="slide" style="background-image:url(images/slide2.jpg);"></div>
                <div class="slide" style="background-image:url(images/slide5.jpg);"></div>
                <div class="slide" style="background-image:url(images/slide4.jpg);"></div>
                <div class="slide" style="background-image:url(images/slide3.jpg);"></div>
                <div class="slide" style="background-image:url(images/slide2.jpg);"></div>
            </div>
        </div>
        <header id="header" class="relv">
            <div class="logo_navbar">
                <div class="container-fluid">
                    <a id="logo" href="#"><img src="images/logo.png" alt="instalimo" height="38" width="216"></a>
                    <a class="menu_trigger" href="#"><img src="images/menu.png" alt=""></a>
                    <nav id="main_nav">
                        <ul>
                            <li><a href="#">BLOG</a></li>
                            <?php if (Illuminate\Support\Facades\Auth::User()) { ?>
                                <li><a href="<?php echo asset('logout') ?>">LOGOUT</a></li>
                                <li><a href="<?php echo asset('dashboard') ?>">List Your Limo !</a></li>   
                            <?php } else { ?>

                                <li><a class="fancybox "  href="#light_boxs">LOGIN</a></li>
                                <li><a class="fancybox "  href="#light_boxs">List Your Limo !</a></li>   

                            <?php } ?>

                        </ul>
                    </nav>
                </div>
            </div>

            <div id="light_boxs" class="light" style="display:none">
                <?php include 'include/login.php'; ?>
            </div>
            <div id="find_limo_form">

                <div class="container">
                    <form id="searchquery" class="clearfix" method="post" action="<?php echo asset('/') ?>">
                        <input type="hidden" class="field" id="street_number" disabled="true" name="street_number"></input>
                        <input type="hidden" class="field" id="route" disabled="true" name="route"></input>   
                        <input type="hidden" class="field" id="locality"  disabled="true" name="locality"></input>
                        <input type="hidden" class="field" id="administrative_area_level_1" disabled="true" name="administrative_area_level_1"></input>
                        <input type="hidden" class="field" id="postal_code" disabled="true" name="postal_code"></input></td>
                        <input  type="hidden" class="field" id="country" disabled="true" name="country"></input>
                        <input  type="hidden"  id="lat1"  name="lat" value="">
                        <input  type="hidden"  id="lng1"  name="lng" value="">
                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="seach_inputs">
                            <div class="heading">FIND YOUR DREAM LIMO NOW !!</div>
                            <div class="clearfix">
                                <div class="inputfield Bachelorette">
                                    <div id="part_trigger" class="drop_trigger" name="occians" ><span><img src="images/clink.png" alt=""></span>What`s the occasion?</div>
                                    <ul id="party_drp" class="scrollor">
                                        <input type="hidden" name="occian_type" id="occian_type" value="">
                                        <li><span><img src="images/wedding-icon.jpg" alt=""></span>Wedding</li>
                                        <li><span><img src="images/dance_icon.png" alt=""></span>Prom/School Dance</li>
                                        <li><span><img src="images/gift-128.png" alt=""></span>Birthday</li>
                                        <li><span><img src="images/icon_cake.png" alt=""></span>Anniversary</li>
                                        <li><span><img src="images/night.png" alt=""></span>Night on the Town</li>
                                        <li><span><img src="images/clink.png" alt=""></span>Bachelor(ette) Party</li>
                                        <li><span><img src="images/concert-icon.png" alt=""></span>Concert/Sporting Event</li>
                                        <li><span><img src="images/wine-icon.png" alt=""></span>Wine Tour</li>
                                        <li><span><img src="images/historical.png" alt=""></span>Historical/Other Tours</li>
                                        <li><span><img src="images/airport-icon.png" alt=""></span>Airport Service</li>
                                        <li><span><img src="images/business.png" alt=""></span>Business Travel</li>
                                        <li><span><img src="images/point-to-point.png" alt=""></span>Point to Point/Transfer</li>
                                        <li><span><img src="images/cruise-icon.png" alt=""></span>Cruise Port Service</li>
                                        <li><span><img src="images/funeral-icon.png" alt=""></span>Funerals</li>
                                        <li><span><img src="images/other.png" alt=""></span>Other (Just Drive)</li>
                                        <?php // foreach ($events as $event):?>
                                        <?php // echo $event['event_title']?>
                                        <?php // endforeach;?>
                                    </ul>
                                  <span class="alert danger" id="occianerror" style="display: none;">Required</span>  
                                </div>
                                
                                <div class="inputfield passenger">

                                        <span class="alert danger" id="passengerserror" style="display: none;">Required</span>
                                        <span class="alert danger" id="passengerserrorlimit" style="display: none;">max 100 passengers</span>
                                        <input type="number" placeholder="No. of passengers" name="passengers" pattern="[0-9]*" min="1" max="100" autocomplete="off" id="passengers">
                                    <div class="inc button">+</div>
                                    <div class="dec button">-</div>



                                </div>
                                <div class="inputfield location">
                                    <span class="alert danger" id="locationerror" style="display: none;"> Complete Address Required</span>
                                    <input type="text" placeholder="Pickup Location" name="picklocation" id="searchform" onFocus="geolocate()" >
                                </div>
                                <div class="inputfield time">
                                    <span class="alert danger" id="dateerror" style="display: none;">Required</span>
                                    <input id="datetimepicker"  type="text" placeholder="Date & Time" name="daterequired" tabindex="-1" autocomplete="off" readonly>
                                </div>
                               
                            </div>

                        </div>
                        <div class="search_limo">
                            <img src="images/locallimo.png" alt="">            
                            <div class="find_limo">
                                <input type="submit" value="FIND LIMOS!">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div id="testimonial_bar">
                <div class="container">
                    <div class="testimonial">
                        <div class="user"><img src="images/user.png" alt=""></div>
                        <div class="tesimonial_text">Some testimonial will come here. Was not in wireframes bd.</div>
                    </div>
                </div>
            </div>
        </header>
        <section id="howit_work">
            <div class="container">
                <h3 class="section_title">How It Works <span>It's as simple as 1 2 3</span></h3>
                <div class="row clearfix inline_container">
                    <div class="col-md-4 inline_sm">
                        <img src="images/limo.png" alt="">
                        <span class="next_step"><img src="images/next.png" alt=""></span>        	
                        <div class="step_name">Search Local Limos</div>
                    </div>
                    <div class="col-md-4 inline_sm">
                        <img src="images/comparelimo.png" alt="">
                        <span class="next_step"><img src="images/next.png" alt=""></span>
                        <div class="step_name">Compare Limos & Rates</div>
                    </div>
                    <div class="col-md-4 inline_sm">
                        <img src="images/bestprice.png" alt="">
                        <div class="step_name">Book Direct For Best Rates</div>
                    </div>
                </div>

            </div>
        </section>
        <section id="main_content">
            <div class="container">
                <h3 class="section_title">Popular Cities <span>See where people are going on limos, all around the world.</span></h3>
                <div class="locations">
                    <div class="row clearfix">
                        <div class="col-sm-8 text-center">
                            <img src="images/newyor.jpg" alt="">
                            <div class="location">
                                <div class="table">
                                    <div class="table_cell">
                                        NEW YORK
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 text-center">
                            <img src="images/dubai.jpg" alt="">
                            <div class="location">
                                <div class="table">
                                    <div class="table_cell">
                                        DUBAI
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix second">
                        <div class="col-sm-4 text-center">
                            <img src="images/paris.jpg" alt="">
                            <div class="location">
                                <div class="table">
                                    <div class="table_cell">
                                        PARIS
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 text-center">
                            <img src="images/barcelona.jpg" alt="">
                            <div class="location">
                                <div class="table">
                                    <div class="table_cell">
                                        BARCELONA
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 text-center">
                            <img src="images/denmark.jpg" alt="">
                            <div class="location">
                                <div class="table">
                                    <div class="table_cell">
                                        DENMARK
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 class="section_title">As seen on<span>Here is a look at some of the places where our limos have been featured!</span></h3>
                <div class="associated_logos text-center">
                    <div class="logo"><img src="images/charity.png" alt=""></div>
                    <div class="logo"><img src="images/datering.png" alt=""></div>
                    <div class="logo"><img src="images/officecleaner.png" alt=""></div>
                    <div class="logo"><img src="images/gamblingking.png" alt=""></div>
                    <div class="logo"><img src="images/windpower.png" alt=""></div>
                </div>
                <!--              <div class="pw-widget pw-size-medium pw-counter-show">   
                                    <a class="pw-button-facebook"></a>   
                                    <a class="pw-button-twitter"></a>   
                                    <a class="pw-button-email"></a>   
                                    <a class="pw-button-post"></a>  
                                </div>
                                <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=tpum1kil17iao7d248vk&retina=true" type="text/javascript"></script>-->
            </div>
        </section>
        <?php require('include/footer.php'); ?>

        <style>
            input.error {
                border:solid 2px red !important;
            }
            #searchquery label.error {
                margin-left: 20px;
                width: auto;
                display: inline;
                color:red;
                font-size: 20px;
            }
        </style>
        <script type="text/javascript">
 var check =0;
            $(document).ready(function() {
                $('#searchquery').validate({// initialize the plugin
                    rules: {
                        occians: "required", passengers: "required",
                        picklocation: "required", daterequired: "required", test: "required"
                    },
                    messages: {
                        occians: "",
                        passengers: "",
                        picklocation: "",
                        daterequired: "",
                        test: ""
                    }
                });
                $('#searchquery').submit(function(event) {
                  //  google.maps.event.trigger(autocomplete, 'place_changed');
                    occian = $('#part_trigger').text();
                   
                    city = $('#locality').val();
                    passenger = $('#passengers').val();
                    date = $('#datetimepicker').val();
                    if (passenger === '') {
                        $('#passengerserror').show().fadeOut(5000);
                        event.preventDefault();
                    }
                    if (passenger > 100) {
                        $('#passengerserrorlimit').show().fadeOut(5000);
                        event.preventDefault();
                    }
                    if (occian === 'What`s the occasion?') {
                        $('#occianerror').show().fadeOut(5000);
                        event.preventDefault();
                    }
                    if (date === '') {
                        $('#dateerror').show().fadeOut(5000);
                        event.preventDefault();
                    }
                      if (city === '') {
                        $('#locationerror').show().fadeOut(5000);
                        event.preventDefault();
                    }
                    
                    else {
                        $('#occian_type').val(occian);
//                        $('#searchquery').submit();
                    }
                });
            });
                function geolocate() {
           if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = new google.maps.LatLng(
                        position.coords.latitude, position.coords.longitude);
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
            $('#searchform').keypress(function(e) {
                if (e.which == 13) {
                e.preventDefault();
               google.maps.event.addListener(autocomplete, 'place_changed', function() {
                 fillInAddress();
                $('#searchquery').submit();
        }); } });
        </script>
    </body>
</html>