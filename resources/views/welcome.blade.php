<html>
	<head>
		<title><?php echo $title?></title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">InstaLimos</div>
				<div class="quote">{{ Inspiring::quote() }}</div>
                                <div class="quote">Coming Soon</div>
			</div>
		</div>
	</body>
</html>




<!--<form action="" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_GXtHDftCD8jdHzDLbZoCjhHg"
    data-amount="900"
    data-name="Limos"
    data-description="First Time Charge"
    >
  </script>
</form>-->


