<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicals extends Model {

    protected $table = 'vehicles';
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
