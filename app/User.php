<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class User extends \Illuminate\Database\Eloquent\Model implements AuthenticatableContract, CanResetPasswordContract ,BillableContract {

	use Authenticatable, CanResetPassword;
        use Billable;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
        
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['namename', 'email', 'password'];
 	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
        
        protected $dates = ['trial_ends_at', 'subscription_ends_at'];
	protected $hidden = ['password', 'remember_token'];

        public function vehicals() {
            return $this->hasMany('App\Vehicals');
        }
        public function images() {
            return $this->hasMany('App\Vehimages');
        }
        public function company() {
            return $this->hasMany('App\Company');
        }
        public function review() {
            return $this->hasMany('App\Reviews');
        }
}




