<?php
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
/*
  |--------------------------------------------------------------------------
  | Main Route
  |--------------------------------------------------------------------------
  |
  | Load Main homepage

 */
Route::get('/', 'WelcomeController@index');
Route::get('terms', 'WelcomeController@terms');

/*
  |--------------------------------------------------------------------------
  | Main Route
  |--------------------------------------------------------------------------
  |
  | Get post data from homepage and take to user data page

 */
Route::post('/', array('before' => 'csrf', function() {
Session::put('searchdata', $_POST);
return View::make('get_vister_detail', array('title' => 'Instalimos'));
}));


/*
  |--------------------------------------------------------------------------
  | Main Route
  |--------------------------------------------------------------------------
  |
  | Get controller to show register page

 */

Route::get('register',function(){
    $data = Illuminate\Support\Facades\DB::table('cities')->groupBy('country_name')->get();
  return View::make('show_register',array('title' => 'Instalimos','countries'=>$data));
});
/*
  |--------------------------------------------------------------------------
  | Main Route
  |--------------------------------------------------------------------------
  |
  | Get post data from register and take to user data page

 */
Route::post('register', 'WelcomeController@register');
/*
  |--------------------------------------------------------------------------
  | Main Route
  |--------------------------------------------------------------------------
  |
  | Show Registration Success Message

 */
Route::get('suucess',function(){
  return View::make('registration_success_message',array('title' => 'Instalimos'));
});
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the post controller get user data and save to database and search the required query and display record
  save_message
 */
Route::post('savevister', 'WelcomeController@save_vister');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller authenticate user via jquery
 */
Route::get('authenticate', 'WelcomeController@authenticate');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to check email via jqury
 */
Route::get('authenticate_email', 'WelcomeController@authenticate_email');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the post controller get the related limos.
 */
Route::post('get_all_related', 'WelcomeController@get_all_related');
Route::get('get_all_related', function() {
    Session::flush();
    return Redirect::to('/');
});
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to display the selected limo reciord
 */

Route::get('showsearchpage/{id}/{limoid}', 'WelcomeController@showsearchpage');

/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to save the visters message
 */
Route::get('save_message', 'WelcomeController@save_message');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller get user data on ajax base changes
 */
Route::get('getjssearch', 'WelcomeController@getjssearch');

/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to saev the rating
 */
Route::get('save_rating', 'WelcomeController@save_rating');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller get custom data on user search
 */
Route::post('getcustomsearch', 'WelcomeController@getcustomsearch');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to flush session in case of expire form data
 */
Route::get('savevister', function() {
    Session::flush();
    return Redirect::to('/');
});
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to flush session in case of expire form data
 */
Route::get('getcustomsearch', function() {
    Session::flush();
    return Redirect::to('/');
});
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the sorting on search page
 */
Route::get('getjsorting', 'WelcomeController@getjsorting');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the get controller to flush session in case of expire form data
 */
Route::get('get_all_relatedvengile', function() {
    Session::flush();
    return Redirect::to('/');
});
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Shows the selected record data of company and also detect 2 doller from selected user

 */

/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Get the record of all cities to load on autocomplete

 */
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its test function for distance calculation
 */
Route::get('distance', 'WelcomeController@distance');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters dashboard
 */
Route::get('calculateDistance', 'WelcomeController@calculateDistance');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its test function for distance calculation
 */
Route::get('get_cities', 'WelcomeController@get_cities');
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its operator login
 */
Route::get('login', 'HomeController@index');
/*
  |--------------------------------------------------------------------------
  |  Vister Section Ends Here
  |--------------------------------------------------------------------------
 */
Route::get('home',function() {
    Illuminate\Support\Facades\Cache::flush();
    return Redirect::to('dashboard');
});
/*
  |--------------------------------------------------------------------------
  |  Operator Section Start Here
  |--------------------------------------------------------------------------
 */


/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters dashboard
 */
Route::get('dashboard', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@dashboard',
));

Route::get('add_limo', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@add_limos'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters save limo
 */
Route::post('add_limo', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@addlimos'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters show company page
 */
Route::get('company_profile', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@company_profile'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters save company page
 */
Route::post('company_profile', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@update_company_profile'));

/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters leads function
 */
Route::get('leads_recieved', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@leads_recieved'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters account info function
 */
Route::get('account_info', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@account_info'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters billing updates display view
 */
Route::get('billing_address', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@billing_address'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters updaete billing post function
 */
Route::post('billing_address', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@billing_address_get'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters add secondry cardit cart post function
 */
Route::post('add_other_card', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@add_other_card'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters getting second card details function
 */
Route::get('add_other_card', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@add_other_get'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters deleting a limo function
 */
Route::get('delete_limo/{id}', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@delete_limo'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters updateing limo status
 */
Route::get('update_limo_status', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@update_limo_status'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to update limo 
 */
Route::get('update/{id}', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@update_limo_view'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters post function to update limo 
 */
Route::post('update_limo_status', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@update_limo'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to get list of cities and countries from db
 */
Route::get('get_selected_city', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@get_selected_city'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to get billing info 
 */


Route::get('add_billing_info', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@add_billing_info_get'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters post function to update billing 
 */
Route::post('add_billing_info', array('middleware' => 'auth','after' =>'middleware', 'uses' => 'HomeController@add_billing_info'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to display images of a limo
 */
Route::get('gallery/{id}', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@gallery'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to delete limo image
 */
Route::get('delete_image/{id}/{limoid}', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@delete_image'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters post function to update limo image
 */
Route::post('upload_image', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@upload_image'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to update limo image to default
 */
Route::get('makeimage_default', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'HomeController@makeimage_default'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to change password checking
 */
Route::get('checkpassword', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@checkpassword'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters get function to display operator inbox
 */
Route::get('inbox', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@inbox'));
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters post function to update operator password 
 */
Route::post('changepassword', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'HomeController@changepassword'));


Route::get('email', function() {
    $data['text'] = 'new';
    Mail::send('tet_email', $data, function($message) {
        $message->to('ijaz.ahmed@vengile.com', 'John Smith')
                ->from('vengile@vengile.com','Vengile')
                ->replyTo('ijaz.ahmed@vengile.com', 'Reply Guy')
                ->subject('Welcome!');
    });
});
/*
  |--------------------------------------------------------------------------
  |  Route
  |--------------------------------------------------------------------------
  |
  | Its the opraters logout
 */

Route::get('logout', function() {
    Auth::logout();
    Illuminate\Support\Facades\Cache::flush();
    Session::flush();
    return Redirect::to(\Illuminate\Support\Facades\URL::previous());
});
Route::get('adminlogout', function() {
    Auth::logout();
    Illuminate\Support\Facades\Cache::flush();
    Session::flush();
    return Redirect::to('adminlogin');
});
/*
  |--------------------------------------------------------------------------
  |  Operator Section Ends Here
  |--------------------------------------------------------------------------
 */

/*
  |--------------------------------------------------------------------------
  |  Admin Section Starts Here 
  |--------------------------------------------------------------------------
 */

Route::get('adminlogin',array(    'middleware' =>'nocache','uses' => 'WelcomeController@login_view',));
Route::get('admindashlogin', array(    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@index',));
Route::get('admindashboard', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@admindashboard',
));


Route::get('add_admin_view', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@showAddAdminView',
));


Route::post('createadmin', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@createadmin',
));
Route::post('adminlogin', 'WelcomeController@adminlogin');
Route::get('operators',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@gettotal',
));

Route::post('add_note', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@add_note',
));
Route::post('add_lead_cradit', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@add_lead_cradit',
));
Route::post('charge_user', array(
    'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@charge_user',
));
Route::get('getlowbalance',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@getlowbalance',
));
Route::get('getactive',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@getactive',
));
Route::get('getpending',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@getpending',
));
Route::get('getcanceled',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@getcanceled',
));
Route::get('getqueed',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@getqueed',
));
Route::get('detail/{id}',array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@userdetails',
));
Route::get('update_user_status',
        array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'AdminController@update_user_status'
            ));
Route::get('gettimeout',
        array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'AdminController@gettimeout'
            ));
Route::get('update_time_out',
        array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'AdminController@update_time_out'
            ));

Route::get('delete_user/{id}', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'AdminController@delete_user'));
Route::get('getrefund/{id}', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'AdminController@getrefund'));
Route::post('getrefund', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'AdminController@refund'));
Route::get('forgetpass',function(){
  return View::make('admin/forget_password',array('title' => 'Instalimos'));
});

Route::post('adminchangepassword', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@adminChangePassword',
));
Route::get('operator_list', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@gettotal',
));
Route::get('visitor_list', array(  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@visitor_list',));

Route::get('search_list', array(  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_list',));
//Route::get('admin_list',function(){
//  return View::make('admin/list_of_admin',array('title' => 'Instalimos'));
//});
Route::get('admin_list', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@admin_list',
));

Route::get('get_search_details', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_details',
));
Route::get('search_details_date', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_details_date',
));

Route::get('export_search_details', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_details_csv',
));

Route::get('get_search_results', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_results',
));

Route::post('get_search_results_byDate', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_results_byDate',
));

Route::get('export_search_results', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@search_results_csv',
));

//Route::post('get_search_results_byDate', 'AdminController@search_results_byDate');

Route::get('get_revenue_details', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@revenue_details',
));

Route::post('get_revenue_details_byDate', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@revenue_details_byDate',
));

Route::get('export_revenue_details', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@revenue_detail_csv',
));

Route::get('checkrefund', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@checkrefund',
));

Route::get('approve_user/{id}', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@approve_user',
));
Route::get('application/{id}', array(
  'middleware' => 'auth', 'middleware' =>'nocache','uses' => 'AdminController@application',
));
/*
  |--------------------------------------------------------------------------
  |  Admin Section Ends Here
  |--------------------------------------------------------------------------
 */

/*
  |--------------------------------------------------------------------------
  |  Admin  view users Section Starts Here 
  |--------------------------------------------------------------------------
 */

Route::get('viewdashboard/{id}', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@viewdashboard'));

Route::get('delete_user_limo/{id}', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@delete_limo'));

Route::get('update_limo/{id}', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@update_limo_view'));

Route::get('gallery_user/{id}', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@gallery'));

Route::get('user_add_limo', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@add_limos'));

Route::get('user_company_profile', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@company_profile'));

Route::get('user_billing_address', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@billing_address'));

Route::get('user_account_info', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@account_info'));

Route::get('user_payment_detrail', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@payment_detrail'));

Route::get('user_leads_recieved', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@leads_recieved'));

Route::get('user_inbox', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@inbox'));

Route::post('user_add_limos', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@addlimos'));

Route::post('update_limo_user', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@update_limo'));

Route::post('user_company_profile', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@update_company_profile'));

Route::post('user_billing_address', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@billing_address_get'));

Route::post('user_add_other_card', array('middleware' => 'auth','middleware' =>'nocache', 'uses' => 'Adminviewusers@add_other_card'));

Route::get('user_checkpassword', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@checkpassword'));

Route::post('user_changepassword', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@changepassword'));

Route::post('admin_change_password', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@change_password'));

Route::get('update_calendar', array('middleware' => 'auth', 'middleware' =>'nocache','uses' => 'Adminviewusers@update_calendar'));