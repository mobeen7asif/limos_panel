<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Stripe;
use Stripe_Error;

class HomeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return \Illuminate\Support\Facades\Redirect::to('dashboard');
    }

    public function dashboard() {
        $user = Auth::user();
        if($user->role == 3){
        $data['reviews'] = \Illuminate\Support\Facades\DB::table('reviews')
                ->join('visters', 'visters.id', '=', 'reviews.reviewed_by_id')
                ->where('reviews.user_id', '=', $user->id)
                ->orderBy('reviews.created_at', 'desc')
                ->get();
        $data['title'] = 'Dashboard';
        $data['limos'] = \Illuminate\Support\Facades\DB::table('vehicles')
                ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                ->where('vehimages.main_img', 1)
                ->where('vehicles.user_id', '=', $user->id)
                ->where('vehimages.main_img', 1)
                ->select('vehicles.*', 'vehimages.*')
                ->get();
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user->id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user->id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $user->id)->get();
        return view('/user/dashboard', $data);
        } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
        
        }

    public function delete_limo() {
        
        $id = \Illuminate\Support\Facades\Request::segment(2);
        
        \App\Vehicals::where('limo_id', '=', $id)->delete();
        \App\Vehimages::where('user_id','=',$id)->delete();
//        \Illuminate\Support\Facades\File::delete();
        \Illuminate\Support\Facades\Session::flash('success', 'Deleted successfully');
        return \Illuminate\Support\Facades\Redirect::to('dashboard');
    }

    public function update_limo_view() {
        $user = Auth::user();
        if($user->role == 3){
        $id = \Illuminate\Support\Facades\Request::segment(2);
        $data['limos'] = \App\Vehicals::where('limo_id', '=', $id)->get();
        $data['types'] = \App\Limotypes::all()->toArray();
        $data['aminties'] = \App\Limoamen::all()->toArray();
        $data['countries'] = \Illuminate\Support\Facades\DB::table('cities')
                ->groupBy('country_name')
                ->get();
        $data['selected_type'] = \Illuminate\Support\Facades\DB::table('vehicles')
                ->join('limo_type', 'limo_type.limo_type_id', '=', 'vehicles.limo_type')
                ->where('vehicles.limo_id', '=', $id)
                ->get();
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user->id)->get();
        $data['photos'] = \Illuminate\Support\Facades\DB::table('vehimages')->where('vehicals_id', '=', $id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user->id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $user->id)->get();
        $data['title'] = 'Dashboard';
        return view('/user/update_limo', $data);
           } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

    function update_limo_status() {
        $affectedRows = \App\Vehicals::where('limo_id', '=', $_GET['limo_id'])->update(['is_active' => $_GET['status']]);
    }

    function get_selected_city() {
        $cities = \Illuminate\Support\Facades\DB::table('cities')
                ->where('country_name', '=', $_GET['country'])
                ->get();
        print_r(json_encode($cities));
    }

    function update_limo() {
        $amin = '';
        if(isset($_POST['amen_title'])){
        foreach ($_POST['amen_title'] as $test) {
            $amin = $test . ',' . $amin;
        }}
        $vehicals = \App\Vehicals::where('limo_id', '=', $_POST['limo_id'])->update([
            'veh_name' => $_POST['veh_name'],
            'veh_rules' => $_POST['veh_rules'],
            'amenities' => $amin,
            'veh_des' => $_POST['veh_des'],
            'veh_seats' => $_POST['veh_seats'],
            'veh_price' => $_POST['veh_price'],
            'lat' => $_POST['lat'],
            'lng' => $_POST['lng'],
            'color' => $_POST['color'],
            'veh_policy' => $_POST['veh_policy'],
            'veh_booking_mintime' => $_POST['veh_booking_mintime'],
            'service_area' => $_POST['service_area'],
            'owner_country' => $_POST['country_name'],
            'owner_state' => $_POST['owner_state'],
            'owner_city' => $_POST['owner_city'],
            'owner_address' => $_POST['owner_address'],
            'limo_type' => $_POST['limo_type']
        ]);
        \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
        return \Illuminate\Support\Facades\Redirect::to('update/' . $_POST['limo_id']);
    }

    public function company_profile() {
        $user_id = Auth::user()->id;
        if(Auth::user()->role == 3){
//        \App\User::find($user_id)->company()->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user_id)->get();
        $data['id'] = Auth::user()->id;
        $data['title'] = 'Company Info';
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user_id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user_id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $user_id)->get();
        return View('/user/company_profile', $data);
          } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

    public function update_company_profile() {

        if ($_FILES['comp_image']['name']) {
            if (\Illuminate\Support\Facades\Input::file('comp_image')->isValid()) {
                $destinationPath = 'images/comp_images'; // upload path
                $extension = \Illuminate\Support\Facades\Input::file('comp_image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                \Illuminate\Support\Facades\Input::file('comp_image')->move($destinationPath, $fileName); // uploading file to given path
                $company = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $_POST['user_id'])->get();
                if ($company) {
                    $company_update = \App\Company::where('user_id', '=', $_POST['user_id'])->update([
                        'comp_name' => $_POST['comp_name'],
                        'comp_f_name' => $_POST['comp_f_name'],
                        'comp_image' => $fileName,
                        'comp_l_name' => $_POST['comp_l_name'],
                        'comp_phone' => $_POST['comp_phone'],
                        'comp_mobile' => $_POST['comp_mobile'],
                        'comp_email' => $_POST['comp_email'],
                        'comp_biz_url' => $_POST['comp_biz_url'],
                        'comp_info' => $_POST['comp_info']
                    ]);
                    \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                    return \Illuminate\Support\Facades\Redirect::to('company_profile');
                } else {
                    $company_update = new \App\Company;
                    $company_update->comp_name = $_POST['comp_name'];
                    $company_update->comp_f_name = $_POST['comp_f_name'];
                    $company_update->comp_image = $fileName;
                    $company_update->comp_l_name = $_POST['comp_l_name'];
                    $company_update->comp_phone = $_POST['comp_phone'];
                    $company_update->comp_mobile = $_POST['comp_mobile'];
                    $company_update->comp_email = $_POST['comp_email'];
                    $company_update->comp_biz_url = $_POST['comp_biz_url'];
                    $company_update->comp_info = $_POST['comp_info'];
                    $company_update->user_id = $_POST['user_id'];
                    $company_update->save();
                    \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                    return \Illuminate\Support\Facades\Redirect::to('company_profile');
                }
            } else {
                \Illuminate\Support\Facades\Session::flash('error', 'Invalid File Type');
                return \Illuminate\Support\Facades\Redirect::to('company_profile');
            }
        } else {
            $company = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $_POST['user_id'])->get();
            if ($company) {
                $company_update = \App\Company::where('user_id', '=', $_POST['user_id'])->update([
                    'comp_name' => $_POST['comp_name'],
                    'comp_f_name' => $_POST['comp_f_name'],
                    'comp_l_name' => $_POST['comp_l_name'],
                    'comp_phone' => $_POST['comp_phone'],
                    'comp_mobile' => $_POST['comp_mobile'],
                    'comp_email' => $_POST['comp_email'],
                    'comp_biz_url' => $_POST['comp_biz_url'],
                    'comp_info' => $_POST['comp_info']
                ]);
                \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                return \Illuminate\Support\Facades\Redirect::to('company_profile');
            } else {
                $company_update = new \App\Company;
                $company_update->comp_name = $_POST['comp_name'];
                $company_update->comp_f_name = $_POST['comp_f_name'];
                $company_update->comp_l_name = $_POST['comp_l_name'];
                $company_update->comp_phone = $_POST['comp_phone'];
                $company_update->comp_mobile = $_POST['comp_mobile'];
                $company_update->comp_email = $_POST['comp_email'];
                $company_update->comp_biz_url = $_POST['comp_biz_url'];
                $company_update->comp_info = $_POST['comp_info'];
                $company_update->user_id = $_POST['user_id'];
                $company_update->save();
                \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                return \Illuminate\Support\Facades\Redirect::to('company_profile');
            }
        }
    }

    function leads_recieved() {
        $user_id = Auth::user()->id;
        if(Auth::user()->role == 3){
        $data['title'] = 'Leads';
        $data['leads'] = \Illuminate\Support\Facades\DB::table('leeds_meesage')
                ->join('visters', 'visters.id', '=', 'leeds_meesage.searched_by_id')
                ->join('vehicles', 'vehicles.limo_id', '=', 'leeds_meesage.lemo_searched')
                ->where('leeds_meesage.user_id', '=', $user_id)
                ->orderBy('leeds_meesage.search_date', 'desc')
                ->select( 'visters.firstname','visters.lastname', 'visters.contact', 'leeds_meesage.occian_type', 'leeds_meesage.search_date', 'leeds_meesage.passengers', 'leeds_meesage.picklocation', 'leeds_meesage.created_at')
                ->paginate(15);
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user_id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user_id)->get();
         $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $user_id)->get();
        return view('/user/leads_recieved', $data);
          } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

    public function add_limos() {
        
        $user_id = Auth::user()->id;
        if(Auth::user()->role == 3){
        $data['title'] = 'Add Limo';
        $data['types'] = \App\Limotypes::all()->toArray();
        $data['aminties'] = \App\Limoamen::all()->toArray();
        $data['countries'] = \Illuminate\Support\Facades\DB::table('cities')
                ->groupBy('country_name')
                ->get();
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user_id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user_id)->get();
         $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $user_id)->get();
        return view('/user/add_limo', $data);
          } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

    public function addlimos() {

        $amin = '';
        if (isset($_POST['amen_title'])) {
            foreach ($_POST['amen_title'] as $test) {
                $amin = $test . ',' . $amin;
            }
        }
        $user_id = Auth::user()->id;

        $files = \Illuminate\Support\Facades\Input::file('attachments');
        foreach ($files as $file) {
            if ($file->isValid()) {
                $destinationPath = 'images/limo_image'; // upload path
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $name = rand(11111, 99999) . '.' . $extension;
                $fileName[] = $name;

                $file->move($destinationPath, $name); // uploading file to given path   
            }
        }
        if ($fileName) {
            $array = [
                "user_id" => $user_id,
                "limo_type" => $_POST['limo_type'],
                "veh_name" => $_POST['veh_name'],
                "veh_des" => $_POST['veh_des'],
                "veh_rules" => $_POST['veh_rules'],
                "color" => $_POST['color'],
                "veh_seats" => $_POST['veh_seats'],
                "veh_price" => $_POST['veh_price'],
                "veh_booking_mintime" => $_POST['veh_booking_mintime'],
                "service_area" => $_POST['service_area'],
                'lat' => $_POST['lat'],
                'lng' => $_POST['lng'],
                 "veh_policy" => $_POST['veh_policy'],
                "owner_country" => $_POST['owner_country'],
                "owner_state" => $_POST['owner_state'],
                "owner_city" => $_POST['owner_city'],
                "owner_address" => $_POST['owner_address'],
                "is_active" => 1,
                "amenities" => $amin
            ];
            $id = \Illuminate\Support\Facades\DB::table('vehicles')->insertGetId($array);
            $i = 1;
            foreach ($fileName as $image_name):
                $saveimage = new \App\Vehimages;
                $saveimage->user_id = $user_id;
                $saveimage->vehicals_id = $id;
                $saveimage->image_name = $image_name;
                if ($i == 1) {
                    $saveimage->main_img = '1';
                }
                $saveimage->save();
                $i++;
            endforeach;
            \Illuminate\Support\Facades\Session::flash('success', 'Inserted successfully');
            return \Illuminate\Support\Facades\Redirect::to('add_limo');
        } else {
            \Illuminate\Support\Facades\Session::flash('error', 'Ivalid Files');
            return \Illuminate\Support\Facades\Redirect::to('add_limo');
        }
    }

//}

    function billing_address() {
        if(Auth::user()->role == 3){
            
        $data['title'] = 'Add Billing';
        $id = Auth::user()->id;
        $data['user'] = \Illuminate\Support\Facades\DB::table('users')->where('id', '=', $id)->get();
        $data['amount'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $id)->get();
        $data['sec_card'] = \Illuminate\Support\Facades\DB::table('craditcard_details')->where('user_id', '=', $id)->get();
        $data['billing_info'] = \Illuminate\Support\Facades\DB::table('save_billing_info')->where('user_id', '=', $id)->get();
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $id)->get();
        return view('/user/billing_address', $data);
          } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

    function billing_address_get() {

         

        $id = Auth::user()->id;
        $creditCardToken = $_POST['stripeToken'];
        $user = \App\User::find($id);
       
  \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');      
 
        if ($user->subscribed()) {
           
              

           //   \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
//              \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');     


     
            $rp = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
//            $rp = \Laravel\Cashier\Customer::all();
//            dd($rp);
            if ($user->default_card == 0) {
               
                $user->default_card = $rp->data[1]->id;
                \App\User::where('id', '=', $id)->update(['default_card' => 1]);
            }
            
            $source = array(
                'cvc' => $_POST['cvc'],
                'number' => $_POST['number'],
                'exp_month' => $_POST['exp_month'],
                'exp_year' => $_POST['exp_year'],
                'object' => 'card'
            );
            $rp->source = $source;
            $rp->save();
            \App\User::where('id', '=', $id)->update(['account_status' => 1, 'last_four' => $rp->sources->data[0]->last4]);
            $payments = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $id)->get();
            if ($payments[0]->total_amount < 25) {
                
                $details = $user->charge(10000);
                if ($details['status'] != 'succeeded') {
                    \App\User::where('id', '=', $id)->update('account_status', '=', 0);
                } else { 
                                  $savepayment = new \App\Payment_details;
                                  $savepayment->user_id = $id;
                                  $savepayment->transaction_id = $details->id;
                                  $savepayment->amount = 100;
                                  $savepayment->remaing_amount = 100;
                                  $savepayment->save();
                    $amount = \App\Savepayments::where('user_id', '=', $id);
                    $amount->increment('total_amount', 100);
                }
            }
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('billing_address');
        } else 

  
 {
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
     
    //  dd($user); 
        try{  
            $user->subscription('Instalimos2')->create($creditCardToken);
      
        } catch(\Stripe\Error\Card $e) {

//            dd("Card declined");
        } catch (\Stripe\Error\InvalidRequest $e) {
  //          dd("Invalid parameters were supplied to Stripe's API");
        } catch (\Stripe\Error\Authentication $e) {
    //        dd("Authentication with Stripe's API failed");
        } catch (\Stripe\Error\ApiConnection $e) {
           // dd("Network communication with Stripe failed");
        } catch (\Stripe\Error\Base $e) {
        //    dd("Display a very generic error to the user");
        } catch (Exception $e) {
          //  dd("Something else happened, completely unrelated to Stripe");
        }
           $details = $user->charge(10000);
          // dd($details);       
           
 \App\User::where('id', '=', $id)->update(['account_status' => 1]);
            \App\User::where('id', '=', $id)->update(['default_card' => 1]);
                                  $savepayment = new \App\Payment_details;
                                  $savepayment->user_id = $id;
                                  $savepayment->transaction_id = $details->id;
                                  $savepayment->amount = 100;
                                  $savepayment->remaing_amount = 100;
                                  $savepayment->save();
            \Illuminate\Support\Facades\DB::table('save_payments')->insert(
                    ['user_id' => $id,
                        'total_amount' => '100']);
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('billing_address');
        }
    }

    function add_other_card() {
        $data['title'] = 'Add Billing';
        $id = Auth::user()->id;
        $user = \App\User::find($id);
        $sec_card = \Illuminate\Support\Facades\DB::table('craditcard_details')->where('user_id', '=', $id)->get();
        if ($sec_card) {
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
            $customer = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
            $customer->sources->retrieve($sec_card[0]->stripe_c_id)->delete();
            $card = $customer->sources->create(array("source" => $_POST['stripeToken']));
            \Illuminate\Support\Facades\DB::table('craditcard_details')->where('user_id', '=', $id)->delete();
            $savecard = new \App\Craditcard;
            $savecard->user_id = $id;
            $savecard->c_num_sec = $card->last4;
            $savecard->c_cvc_sec = $_POST['cvc'];
            $savecard->c_exp_sec = $card->exp_month . '-' . $card->exp_year;
            $savecard->stripe_c_id = $card->id;
            $savecard->save();
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('billing_address');
        } else {
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
            $cu = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
            $card = $cu->sources->create(array("source" => $_POST['stripeToken']));
            $savecard = new \App\Craditcard;
            $savecard->user_id = $id;
            $savecard->c_num_sec = $card->last4;
            $savecard->c_cvc_sec = $_POST['cvc'];
            $savecard->c_exp_sec = $card->exp_month . '-' . $card->exp_year;
            $savecard->stripe_c_id = $card->id;
            $savecard->save();
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('billing_address');
        }
    }

    function add_other_get() {
        return \Illuminate\Support\Facades\Redirect::to('billing_address');
    }

    function add_billing_info_get() {
        return \Illuminate\Support\Facades\Redirect::to('billing_address');
    }

    function add_billing_info() {
        $id = Auth::user()->id;
        $billingdetail = \Illuminate\Support\Facades\DB::table('save_billing_info')->where('user_id', '=', $id)->get();
        if ($billingdetail) {
            \App\Savebilling::where('user_id', '=', $id)->update(
                    ['comp_name' => $_POST['comp_name'],
                        'user_id' => $id,
                        'fname' => $_POST['fname'],
                        'lname' => $_POST['lname'],
                        'mobile' => $_POST['mobile'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'biz_url' => $_POST['biz_url'],
                        'about_comp' => $_POST['about_comp']]);
        } else {
            \Illuminate\Support\Facades\DB::table('save_billing_info')->insert(
                    ['comp_name' => $_POST['comp_name'],
                        'fname' => $_POST['fname'],
                        'user_id' => $id,
                        'lname' => $_POST['lname'],
                        'mobile' => $_POST['mobile'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'biz_url' => $_POST['biz_url'],
                        'about_comp' => $_POST['about_comp']]);
        }
        \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
        return \Illuminate\Support\Facades\Redirect::to('billing_address');
    }

    function gallery() {
        if(Auth::user()->role == 3){
        $user = Auth::user();
        $limo_id = \Illuminate\Support\Facades\Request::segment(2);
        $data['photos'] = \Illuminate\Support\Facades\DB::table('vehimages')->where('vehicals_id', '=', $limo_id)->get();
        $data['title'] = 'Gallery';
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user->id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user->id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=',  $user->id)->get();
        return view('/user/gallery', $data);
         } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
        
    }

    function makeimage_default() {
        \App\Vehimages::where('vehicals_id', '=', $_GET['vehicals_id'])->update(['main_img' => 0]);
        \App\Vehimages::where('image_id', '=', $_GET['image_id'])->update(['main_img' => 1]);
//               return TRUE;
    }

    function delete_image() {
        $imageid = \Illuminate\Support\Facades\Request::segment(2);
        $limoid = \Illuminate\Support\Facades\Request::segment(3);
        $photo = \Illuminate\Support\Facades\DB::table('vehimages')->where('image_id', '=', $imageid)->get();
        if ($photo[0]->main_img == 1) {
            \Illuminate\Support\Facades\Session::flash('error', 'Can not delete main image');
            return \Illuminate\Support\Facades\Redirect::to('gallery/' . $limoid);
        } else {
            \App\Vehimages::where('image_id', '=', $imageid)->delete();
            \Illuminate\Support\Facades\Session::flash('success', 'Deleted successfully');
            return \Illuminate\Support\Facades\Redirect::to('gallery/' . $limoid);
        }
    }

    function upload_image() {

        $files = \Illuminate\Support\Facades\Input::file('veh_image');
        $imageid = $_POST['imageid'];
        if (\Illuminate\Support\Facades\Input::file('veh_image')->isValid()) {

            $destinationPath = 'images/limo_image'; // upload path
            $extension = \Illuminate\Support\Facades\Input::file('veh_image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            \Illuminate\Support\Facades\Input::file('veh_image')->move($destinationPath, $fileName); // uploading file to given path
            \App\Vehimages::where('image_id', '=', $imageid)->update(['image_name' => $fileName]);
            echo TRUE;
        } else {
            echo FALSE;
        }
    }

    function account_info() {
        if(Auth::user()->role == 3){
        $user = Auth::user();
        $data['title'] = 'Account Setting';
        $data['email'] = Auth::user()->email;
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user->id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user->id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=', $user->id)->get();
        return view('/user/account_info', $data);
         } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

    function checkpassword() {
        $password = Auth::user()->password;

        if (\Illuminate\Support\Facades\Hash::check($_GET['password'], $password)) {
            echo TRUE;
        } else {
            echo False;
        }
    }

    function changepassword() {
        $id = Auth::user()->id;
        $newpass = \Illuminate\Support\Facades\Hash::make($_POST['password']);
        \App\User::where('id', '=', $id)->update(['password' => $newpass]);
        \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
        return \Illuminate\Support\Facades\Redirect::to('account_info');
    }

    function inbox() {
        if(Auth::user()->role == 3){
        $user_id = Auth::user()->id;
        $data['title'] = 'Leads';
        $data['messages'] = \Illuminate\Support\Facades\DB::table('message')
                ->join('visters', 'visters.id', '=', 'message.message_from')
                ->where('message.message_to', '=', $user_id)
                ->orderBy('message.message_date', 'desc')
                ->paginate(15);
        $data['account'] = \Illuminate\Support\Facades\DB::table('save_payments')->where('user_id', '=', $user_id)->get();
        $data['company'] = \Illuminate\Support\Facades\DB::table('company')->where('user_id', '=', $user_id)->get();
        $data['check_limos']= \Illuminate\Support\Facades\DB::table('vehicles')->where('user_id', '=', $user_id)->get();
        return view('/user/inbox_messages', $data);
        } else {
        return \Illuminate\Support\Facades\Redirect::to('logout');
    }
    }

}
