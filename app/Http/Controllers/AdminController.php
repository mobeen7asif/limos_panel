<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
class AdminController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }
    public function index() {
        return \Illuminate\Support\Facades\Redirect::to('admindashboard');
    }

    public function admindashboard() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['totaluser'] = \DB::table('users')->where('role', '=', '3')->count();
            $data['timeout'] = \DB::table('users')->where(array('role' => '3','time_out'=>1))->count();
            $data['active'] = \DB::table('users')
                    ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                    ->where('users.is_active', '=','1')
                    ->where('users.deactive_account', '=', '0')
                    ->where('users.stripe_active', '=', '1')
                    ->where('users.role', '=', '3')
                    ->where('users.time_out', '=', '0')
                    ->where('save_payments.total_amount', '>=', '25')
                    ->count();

            $data['deactive'] = \DB::table('users')
                    ->where('role', '=', '3')
                    ->where('deactive_account', '=', '1')
                    ->count();
            $data['pending'] = \DB::table('users')
                    ->where('role', '=', '3')
                    ->where('stripe_active', '=', '0')
                    ->where('is_active', '=', '1')
                    ->where('time_out', '=', '0')
                    ->count();
            $data['inactive'] = \DB::table('users')->where('role', '=', '3')->where('is_active', '=', '0')->count();
            $data['lowblance'] = \DB::table('users')
                    ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                    ->where('users.role', '=', '3')
                    ->where('users.stripe_active', '=', '1')
                    ->where('save_payments.total_amount', '<=', '25')
                    ->count();
            $data['title'] = 'Admin Dashboard';
            $data['users'] = \DB::table('users')
                            ->leftJoin('company', 'company.user_id', '=', 'users.id')
                            ->where('role', '=', '3')->get();
            return view('admin/dashboard', $data);
        } else {
//            print_r($admin);
//            exit;
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    public function showAddAdminView(){
        $data['title'] = 'Admin Dashboard';
        return view('admin/show_admin_add_view',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function createadmin() {
        $getvister = \DB::table('users')->where('email', '=', $_POST['email'])->get();
        if ($getvister) {
            \Illuminate\Support\Facades\Session::flash('error', 'Given Email is Already With Us');
            return \Illuminate\Support\Facades\Redirect::to('admindashboard');
        } else {
            $admin = new \App\User;
            $admin->email = $_POST['email'];
            $admin->password = bcrypt($_POST['password']);
            $admin->role = 2;
            $admin->is_active = 1;
            $admin->save();
            \Illuminate\Support\Facades\Session::flash('success', 'Admin Created Successfully');
            return \Illuminate\Support\Facades\Redirect::to('admindashboard');
        }
    }

    public function gettotal() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                            ->leftJoin('company', 'company.user_id', '=', 'users.id')
                            ->where('role', '=', '3')->get();
            $data['title'] = 'All Operators';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    public function gettimeout(){
         $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                            ->leftJoin('company', 'company.user_id', '=', 'users.id')
                            ->where('role', '=', '3')->where('time_out','1')->get();
            $data['title'] = 'Time Out Operators';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function getlowbalance() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                    ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                    ->leftJoin('company', 'company.user_id', '=', 'users.id')
                    ->where('users.role', '=', '3')
                    ->where('users.stripe_active', '=', 1)
                    ->where('save_payments.total_amount', '<=', 25)
                    ->get();
            $data['title'] = 'Low Balance';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function getactive() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                    ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                    ->leftJoin('company', 'company.user_id', '=', 'users.id')
                    ->where('users.is_active', '=', 1)
                    ->where('users.deactive_account', '=', 0)
                    ->where('users.stripe_active', '=', 1)
                    ->where('users.role', '=', '3')
                    ->where('save_payments.total_amount', '>=', 25)
                    ->get();
            $data['title'] = 'Active Accounts';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function getpending() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                            ->leftJoin('company', 'company.user_id', '=', 'users.id')
                            ->where('role', '=', '3')->where('stripe_active', '=', '0')
                            ->where('is_active', '=', '1')->get();
            $data['title'] = 'Pending Accounts';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function getcanceled() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                            ->leftJoin('company', 'company.user_id', '=', 'users.id')
                            ->where('role', '=', '3')->where('deactive_account', '=', '1')->get();
            $data['title'] = 'Cancelled Accounts';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function getqueed() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['users'] = \DB::table('users')
                            ->leftJoin('company', 'company.user_id', '=', 'users.id')
                            ->where('role', '=', '3')->where('is_active', '=', '0')->get();
            $data['title'] = 'Queue Accounts';
            return view('admin/list_of_operator', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function userdetails($id) {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $active = \DB::table('users')
                    ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                    ->where('users.id', '=', $id)
                    ->where('users.is_active', '=', 1)
                    ->where('users.stripe_active', '=', 1)
                    ->where('users.role', '=', '3')
                    ->where('save_payments.total_amount', '>=', 25)
                    ->count();
            if ($active) {
                $data['status'] = 'Active Account';
            }
            $deactive = \DB::table('users')->where('role', '=', '3')->where('deactive_account', '=', '1')->where('id', '=', $id)->count();
            if ($deactive) {
                $data['status'] = 'Cancelled Account';
            }
            $pending = \DB::table('users')->where('role', '=', '3')->where('stripe_active', '=', 0)->where('id', '=', $id)->where('is_active', '=', '1')->count();
            if ($pending) {
                $data['status'] = 'Pending Account';
            }
            $inactive = \DB::table('users')->where('role', '=', '3')->where('id', '=', $id)->where('is_active', '=', '0')->count();
            if ($inactive) {
                $data['status'] = 'Accounts in Queue';
            }
            $lowblance = \DB::table('users')
                    ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                    ->where('users.id', '=', $id)
                    ->where('users.role', '=', '3')
                    ->where('users.stripe_active', '=', '1')
                    ->where('save_payments.total_amount', '<=', '25')
                    ->count();
            if ($lowblance) {
                $data['status'] = 'Insufficient Funds Account';
            }
            $data['notes'] = \DB::table('admin_notes')->where('admin_id', '=', $admin->id)->where('user_id', '=', $id)->orderBy('created_on', 'desc')->get();
            $data['user'] = \App\User::where('id', '=', $id)->get();
            $vehicals=  \DB::table('vehicles')->where('user_id', '=', $id)->get();
            $data['vehicals']=  count($vehicals);
            $data['title'] = 'Operator Detail';
            $data['email'] = $admin->email;
            $data['id'] = $id;
            return view('admin/acount_detail', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    function update_user_status() {
        \App\User::where('id', '=', $_GET['user_id'])->update(['deactive_account' => $_GET['status']]);
    }

    function delete_user($id) {
        \App\User::where('id', '=', $id)->delete();
        \App\Vehicals::where('user_id', '=', $id)->delete();
        \App\Messages::where('message_to', '=', $id)->delete();
        \App\Savepayments::where('user_id', '=', $id)->delete();
        \App\Vehimages::where('user_id', '=', $id)->delete();
        \App\Company::where('user_id', '=', $id)->delete();
        \App\Craditcard::where('user_id', '=', $id)->delete();
        \App\Leeds::where('user_id', '=', $id)->delete();
        \App\Craditcard::where('user_id', '=', $id)->delete();
        \App\Savebilling::where('user_id', '=', $id)->delete();
        \Illuminate\Support\Facades\Session::flash('success', 'Deleted successfully');
        return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
    }

    function add_note() {
        $admin = Auth::user()->id;
        $date = date('Y-m-d h:i') . '<br>';
        $savenot = new \App\Admin_notes;
        $savenot->note = $_POST['note'];
        $savenot->user_id = $_POST['user_id'];
        $savenot->admin_id = $admin;
        $savenot->created_on = $date;
        $savenot->save();
        \Illuminate\Support\Facades\Session::flash('success', 'Note Added successfully');
        return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
    }

    function charge_user() {
        $active = \DB::table('users')
                ->join('save_payments', 'save_payments.user_id', '=', 'users.id')
                ->where('users.id', '=', $_POST['user_id'])
                ->where('users.is_active', '=', 1)
                ->where('users.stripe_active', '=', 1)
                ->where('users.role', '=', '3')
                ->where('save_payments.total_amount', '>=', 25)
                ->count();
        if ($active) {
            $amount = $_POST['amount'] * 100;
            $user = \App\User::find($_POST['user_id']);
            $company = \App\User::find($_POST['user_id'])->company;
            $data = $user->charge($amount);
            $udata = array(
                'comapny' => $company[0]->comp_f_name,
                'amount' => $_POST['amount']
            );
            $email = $user->email;
            $emaildata = array('to' => $email, 'to_name' => $company[0]->comp_f_name);
            \Illuminate\Support\Facades\Mail::send('email_charge_email', $udata, function($message) use ($emaildata) {
                $message->to($emaildata['to'], $emaildata['to_name'])
                        ->from('no-reply@instalimos.com', 'InstaLimos')
                        ->subject('You have Been charged!');
            });
            $amount = \App\Savepayments::where('user_id', '=', $_POST['user_id']);
            $amount->increment('total_amount', $_POST['amount']);
            $savepayment = new \App\Payment_details;
            $savepayment->user_id = $_POST['user_id'];
            $savepayment->transaction_id = $data->id;
            $savepayment->amount = $_POST['amount'];
            $savepayment->remaing_amount = $_POST['amount'];
            $savepayment->save();
            \Illuminate\Support\Facades\Session::flash('success', 'User Has Been charged $' . $_POST['amount'] . ' successfully');
            return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        } else {
            \Illuminate\Support\Facades\Session::flash('error', 'Unable to charge deactive account');
            return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        }
    }

    function add_lead_cradit() {
//        print_r($_POST);exit;
        $lead = \DB::table('save_payments')->where('user_id', '=', $_POST['user_id'])->get();

        if ($lead) {
            $user = \App\User::find($_POST['user_id']);
            $user->is_active = 1;
            $user->save();
            $amount = \App\Savepayments::where('user_id', '=', $_POST['user_id']);
            $amount->increment('total_amount', $_POST['amount']);
            \Illuminate\Support\Facades\Session::flash('success', 'User Has Been added with $' . $_POST['amount'] . ' successfully');
            return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        } else {
            $user = \App\User::find($_POST['user_id']);
            $user->is_active = 1;
            $user->save();
            $addlead = new \App\Savepayments;
            $addlead->user_id = $_POST['user_id'];
            $addlead->total_amount = $_POST['amount'];
            $addlead->save();
            \Illuminate\Support\Facades\Session::flash('success', 'User Has Been added with $' . $_POST['amount'] . ' successfully');
            return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        }
    }

    public function admin_list() {
        $admin = Auth::user();
        if ($admin->role == 1) {
            $data['admin'] = \DB::table('users')
                            ->where('role', '=', '2')->get();
            $data['title'] = 'Admin Accounts';
            return view('admin/list_of_admin', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
    public function search_details() { 
        $admin = Auth::user();
        if ($admin->role == 1) {
            $data['search_details'] = 
                    \DB::table('searched_compnies')
                     ->join('visters', 'visters.id', '=', 'searched_compnies.vister_id')
//                    ->leftjoin('leeds_meesage', 'leeds_meesage.comp_id', '=', 'searched_compnies.comp_id')
                    ->orderBy('searched_compnies.created_at', 'dec')
                    ->get();
//            echo '<pre>';
//            print_r($data['search_details']);exit;
            $data['title'] = 'Search Details'; 
            return view('admin/search_details', $data);   
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    public function search_details_date() { 
        $admin = Auth::user();
        if ($admin->role == 1) {
            $date=$_GET['date'];
            $data['search_details'] = 
                    \DB::table('searched_compnies')
                     ->join('visters', 'visters.id', '=', 'searched_compnies.vister_id')
                    ->orderBy('searched_compnies.created_at', 'dec')
                    ->where('searched_compnies.created_at', 'like', "%$date%")
                    ->get();
            $data['title'] = 'Search Details'; 
            return view('admin/search_details', $data);   
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
    
    public function search_details_csv() 
    {
        $csvObj = new \mnshankar\CSV\CSV();
        $array = \DB::table('visters')->orderBy('created_at', 'dec')->get();
        $arr = [];
        
        $i=1;
        foreach ($array as $key => $value) {
            $arr[$key] = array(
                            'Sr.'=>$i,
                            'First Name' => $value->firstname, 
                            'Last Name' => $value->lastname,
                            'Email' => $value->email,
                            'Phone Number' => $value->contact,
                            'Service/Event Type' => $value->occian_type,
                            'Number of Passengers' => $value->passengers,
                            'Pickup Location' => $value->location,
                            'Service Date' => $value->pickdate,
                            'Search Date' => $value->created_at,
                            'Number of Results' => $value->no_of_companies,
                            );
        $i++;
        };
        return $csvObj->fromArray($arr)->render('search.csv'); 
    }
    
    public function search_results() {
        $admin = Auth::user();
        if ($admin->role == 1) {
            $data['title'] = 'Search Results';
            
            $data['searches'] = \DB::table('searched_compnies')
            ->select(\DB::raw('DATE(searched_compnies.created_at) as date'), \DB::raw('ROUND(avg(searched_compnies.compnies_searched), 2) as companies'), \DB::raw('count(*) as views'))
            ->join('visters', 'visters.id', '=', 'searched_compnies.vister_id')
                    ->groupBy('date')
            ->orderBy('searched_compnies.created_at', 'dec')
            ->get();
            return view('admin/result_details', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
    public function search_results_byDate() {
        $admin = Auth::user();
        if ($admin->role == 1) {
            
            if($_POST['from'])
            {
                $from = $_POST['from'];
            }
            else{
                $from = '';
            }

            if($_POST['to'])
            {

                $to = new \DateTime($_POST['to']);
                $to->modify('+1 day');
           
            }
            else{
                $to = '';
            }
           $data['title'] = 'Search Results';

            if($to && $from){
            
                $data['fromDate'] = $_POST['from'];
                $data['toDate'] = $_POST['to'];
                
                $data['searches'] = \DB::table('visters')
                ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('ROUND(avg(no_of_companies), 2) as companies'), \DB::raw('count(*) as views'))
                 ->whereBetween(\DB::raw('created_at'),array($from, $to))
                ->groupBy('date')
                ->orderBy('created_at', 'dec')
                ->get();

                
            }
            else {
                 $data['searches'] = \DB::table('visters')

                ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('ROUND(avg(no_of_companies), 2) as companies'), \DB::raw('count(*) as views'))
                 
                ->groupBy('date')
                ->orderBy('created_at', 'dec')
                ->get();
            }
            return view('admin/result_details', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
    public function search_results_csv() 
    {
        $from =  \Illuminate\Support\Facades\Session::get('fromDate');
        $to =  \Illuminate\Support\Facades\Session::get('toDate');
        
        $csvObj = new \mnshankar\CSV\CSV();
        
        if(isset($from) && isset($to))
        {
            $to = new \DateTime($to);
            $to->modify('+1 day');
            
            $array =\DB::table('visters')
            ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('ROUND(avg(no_of_companies), 2) as companies'), \DB::raw('count(*) as views'))
            ->whereBetween(\DB::raw('created_at'),array($from, $to))
            ->groupBy('date')
            ->orderBy('created_at', 'dec')
            ->get();
        }
        else 
        {
            $array =\DB::table('visters')
            ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('ROUND(avg(no_of_companies), 2) as companies'), \DB::raw('count(*) as views'))
            ->groupBy('date')
            ->orderBy('created_at', 'dec')
            ->get();
        }
        
        
        $arr = [];
        
        $i=1;
        foreach ($array as $key => $value) {
            $arr[$key] = array(
                            'Sr.'=>$i,
                            'Date' => $value->date, 
                            'Number of Searches' => $value->views,
                            'Average Number of Results' => $value->companies,
                            
                            );
        $i++;
        };
        return $csvObj->fromArray($arr)->render('result.csv'); 
    }
    
    
    public function revenue_details() {
        $admin = Auth::user();
        if ($admin->role == 1) {

            $data['title'] = 'Revenue';
            
            $data['dates'] = \DB::table('leeds_meesage')
            ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('count(*) as views'))
            ->groupBy('date')
            ->orderBy('created_at', 'dec')
            ->get();
            return view('admin/revenue_details', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
    public function revenue_details_byDate() {
        $admin = Auth::user();
        if ($admin->role == 1) {
            
            if($_POST['from'])
            {
                $from = $_POST['from'];
            }
            else{
                $from = '';
            }

            if($_POST['to'])
            {

                $to = new \DateTime($_POST['to']);
                $to->modify('+1 day');
           
            }
            else{
                $to = '';
            }
           $data['title'] = 'Revenue';

            if($to && $from){
                $data['fromDate'] = $_POST['from'];
                $data['toDate'] = $_POST['to'];
                
                $data['dates'] = \DB::table('leeds_meesage')
                    ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('count(*) as views'))
                    ->whereBetween(\DB::raw('created_at'),array($from, $to))
                    ->groupBy('date')
                    ->orderBy('created_at', 'dec')
                    ->get();

                
            }
            else {
                 $data['dates'] = \DB::table('leeds_meesage')
                    ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('count(*) as views'))
                    ->groupBy('date')
                    ->orderBy('created_at', 'dec')
                    ->get();
            }
            return view('admin/revenue_details', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
    public function revenue_detail_csv() 
    {
        $from =  \Illuminate\Support\Facades\Session::get('fromDate');
        $to =  \Illuminate\Support\Facades\Session::get('toDate');
        
        $csvObj = new \mnshankar\CSV\CSV();
        
        if(isset($from) && isset($to))
        {
            $to = new \DateTime($to);
            $to->modify('+1 day');
                
            $array = \DB::table('leeds_meesage')
            ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('count(*) as views'))
            ->whereBetween(\DB::raw('created_at'),array($from, $to))
            ->groupBy('date')
            ->orderBy('created_at', 'dec')
            ->get();
        }
        else{
            $array = \DB::table('leeds_meesage')
            ->select(\DB::raw('DATE(created_at) as date'), \DB::raw('count(*) as views'))
            ->groupBy('date')
            ->orderBy('created_at', 'dec')
            ->get();
        }
        
        $arr = [];
        
        $i=1;
        foreach ($array as $key => $value) {
            $arr[$key] = array(
                            'Sr.'=>$i,
                            'Date' => $value->date, 
                            'Number of Leeds' => $value->views,
                            'Total Revenue' => $value->views*1.99,
                            );
        $i++;
        };
        return $csvObj->fromArray($arr)->render('revenue.csv'); 
    }

    public function getrefund($id) {
        $admin = Auth::user();
        if ($admin->role == 1) {
            $data['payments'] = \DB::table('payment_details')
                            ->where('user_id', '=', $id)->get();
            $data['user'] = \App\User::find($id)->company;

            $data['title'] = 'Refund';
            $data['id'] = $id;
            return view('admin/refund_list', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function refund() {
        $amount = \DB::table('payment_details')->select('*')->where('transaction_id', '=', $_POST['transid'])->get();
        if ($amount[0]->remaing_amount >= $_POST['amount']) {
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
            $rp = \Stripe_Charge::retrieve($_POST['transid']);
            $re = $rp->refund(array('amount' => $_POST['amount'] * 100));
              \DB::table('payment_details')->where('transaction_id', '=', $_POST['transid'])->decrement('remaing_amount', $_POST['amount']);
             \DB::table('save_payments')->where('user_id', '=', $amount[0]->user_id)->decrement('total_amount', $_POST['amount']);
            \Illuminate\Support\Facades\Session::flash('success', 'Refunded successfully');
            return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        } else {
            \Illuminate\Support\Facades\Session::flash('error', 'You can only refund ' . $amount[0]->remaing_amount);
            return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        }
    }

    public function checkrefund() {
        $amount = \DB::table('payment_details')->select('remaing_amount')->where('transaction_id', '=', $_GET['trnsid'])->get();
        if ($amount[0]->remaing_amount >= $_GET['amount']) {
            echo TRUE;
        } else {
            echo FALSE;
        }
    }

    public function approve_user($id) {
        \App\User::where('id', '=', $id)->update(['is_active' => 1]);
        $user = \App\User::find($id);
        $company = \App\User::find($id)->company;
        $data['name'] = $company[0]->comp_f_name;
        $emaildata = array('to' => $user->email, 'to_name' => $company[0]->comp_f_name);
        \Illuminate\Support\Facades\Mail::send('email_register', $data, function($message) use ($emaildata) {
            $message->to($emaildata['to'], $emaildata['to_name'])
                    ->from('no-reply@instalimos.com', 'InstaLimos')
                    ->subject('Congrats! You have been approved to the Instalimos network!');
        });
        \Illuminate\Support\Facades\Session::flash('success', 'Successfully Approved');
        return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
    }
    public function visitor_list() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['visitors'] = \App\Vister::get();
            
            $data['title'] = 'Visters List';
            return view('admin/list_of_visters', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    public function search_list() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
            $data['search'] = \App\Search_record::get();
            $data['title'] = 'Search Record';
            return view('admin/list_of_search', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    public function application($id) {
        $data['title'] ='Application Form';
        $data['user']=  \App\User::find($id);
        $data['company'] =  \App\User::find($id)->company;
        return view('admin/application', $data);
    }
    public function update_time_out(){
        \App\User::where('id', '=', $_GET['user_id'])->update(['time_out' => $_GET['status']]);
    }
    
    public function adminChangePassword() {
        $admin = Auth::user();
        if ($admin->role == 1 || $admin->role == 2) {
       $newpass = \Illuminate\Support\Facades\Hash::make($_POST['password']);
            $admin=\App\User::find($admin->id);
            $admin->password=$newpass;
            $admin->save();
\Illuminate\Support\Facades\Session::flash('success', 'Password Changed Successfully');
        return \Illuminate\Support\Facades\Redirect::to(\Illuminate\Support\Facades\URL::previous());
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

}
