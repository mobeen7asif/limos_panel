<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Stripe;
use Stripe_Error;
use \Illuminate\Support\Facades\DB;

class Adminviewusers extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    function viewdashboard($id) {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            \Illuminate\Support\Facades\Session::put('user_id', $id);
            $data['user'] =  \App\User::find($id);
//            echo '<pre>';
//            print_r($data);exit;
            $data['reviews'] = DB::table('reviews')
                    ->join('visters', 'visters.id', '=', 'reviews.reviewed_by_id')
                    ->where('reviews.user_id', '=', $id)
                    ->orderBy('reviews.created_at', 'desc')
                    ->get();
            $data['title'] = 'Dashboard';
            $data['limos'] = DB::table('vehicles')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('vehicles.user_id', '=', $id)
                    ->where('vehimages.main_img', 1)
                    ->get();
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $id)->get();
           
            $data['company'] = DB::table('company')->where('user_id', '=', $id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $id)->get();
            return view('/admin/user/dashboard', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function delete_limo() {
        $user_id = \Illuminate\Support\Facades\Session::get('user_id');
        $id = \Illuminate\Support\Facades\Request::segment(2);
        \App\Vehicals::where('limo_id', '=', $id)->delete();
        \App\Vehimages::where('user_id', '=', $id)->delete();
//        \Illuminate\Support\Facades\File::delete();
        \Illuminate\Support\Facades\Session::flash('success', 'Deleted successfully');
        return \Illuminate\Support\Facades\Redirect::to('viewdashboard/' . $user_id);
    }

    public function update_limo_view() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $id = \Illuminate\Support\Facades\Request::segment(2);
            $data['limos'] = \App\Vehicals::where('limo_id', '=', $id)->get();
            $data['types'] = \App\Limotypes::all()->toArray();
            $data['aminties'] = \App\Limoamen::all()->toArray();
            $data['countries'] = DB::table('cities')
                    ->groupBy('country_name')
                    ->get();
            $data['selected_type'] = DB::table('vehicles')
                    ->join('limo_type', 'limo_type.limo_type_id', '=', 'vehicles.limo_type')
                    ->where('vehicles.limo_id', '=', $id)
                    ->get();
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['photos'] = DB::table('vehimages')->where('vehicals_id', '=', $id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            $data['title'] = 'Dashboard';
            return view('/admin/user/update_limo', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    function gallery() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $limo_id = \Illuminate\Support\Facades\Request::segment(2);
            $data['photos'] = DB::table('vehimages')->where('vehicals_id', '=', $limo_id)->get();
            $data['title'] = 'Gallery';
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/gallery', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('logout');
        }
    }

    public function add_limos() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $data['title'] = 'Add Limo';
            $data['types'] = \App\Limotypes::all()->toArray();
            $data['aminties'] = \App\Limoamen::all()->toArray();
            $data['countries'] = DB::table('cities')
                    ->groupBy('country_name')
                    ->get();
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/add_limo', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('logout');
        }
    }

    public function company_profile() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['id'] = Auth::user()->id;
            $data['title'] = 'Company Info';
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return View('/admin/user/company_profile', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    function billing_address() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $data['title'] = 'Add Billing';
            $data['user'] = DB::table('users')->where('id', '=', $user_id)->get();
            $data['amount'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['sec_card'] = DB::table('craditcard_details')->where('user_id', '=', $user_id)->get();
            $data['billing_info'] = DB::table('save_billing_info')->where('user_id', '=', $user_id)->get();
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/billing_address', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    function account_info() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $data['title'] = 'Account Setting';
            $email = DB::table('users')->where('id', '=', $user_id)->select('email')->first();
            
            $data['email'] = $email->email;
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/account_info', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }
    
     function payment_detrail() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            
            $data['title'] = 'Payment Detail';
//            $data['email'] = Auth::user()->email;
            $email = DB::table('users')->where('id', '=', $user_id)->select('email')->first();
            
            $data['payments'] = DB::table('payment_details')
                      ->join('users', 'payment_details.user_id', '=', 'users.id')
                     ->where('user_id', '=', $user_id)
                     ->select('users.last_four', 'payment_details.*')
                     ->get();
            
            $data['email'] = $email->email;
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
           
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/payments_detail', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    function leads_recieved() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $data['title'] = 'Leads';
            $data['leads'] = DB::table('leeds_meesage')
                    ->join('visters', 'visters.id', '=', 'leeds_meesage.searched_by_id')
                    ->join('vehicles', 'vehicles.limo_id', '=', 'leeds_meesage.lemo_searched')
                    ->where('leeds_meesage.user_id', '=', $user_id)
                    ->orderBy('leeds_meesage.created_at', 'desc')
                    ->select( 'visters.firstname','visters.lastname', 'visters.contact', 'leeds_meesage.occian_type', 'leeds_meesage.search_date', 'leeds_meesage.passengers', 'leeds_meesage.picklocation', 'leeds_meesage.created_at')
                    ->get();
//            echo '<pre>';
//            print_r($data['leads']);exit;
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/leads_recieved', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    function inbox() {
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 2) {
            $user_id = \Illuminate\Support\Facades\Session::get('user_id');
            $data['title'] = 'Leads';
            $data['messages'] = DB::table('message')
                    ->join('visters', 'visters.id', '=', 'message.message_from')
                    ->where('message.message_to', '=', $user_id)
                    ->orderBy('message.message_date', 'desc')
                    ->paginate(15);
            $data['account'] = DB::table('save_payments')->where('user_id', '=', $user_id)->get();
            $data['company'] = DB::table('company')->where('user_id', '=', $user_id)->get();
            $data['check_limos'] = DB::table('vehicles')->where('user_id', '=', $user_id)->get();
            return view('/admin/user/inbox_messages', $data);
        } else {
            return \Illuminate\Support\Facades\Redirect::to('adminlogout');
        }
    }

    public function addlimos() {

        $amin = '';
        if (isset($_POST['amen_title'])) {
            foreach ($_POST['amen_title'] as $test) {
                $amin = $test . ',' . $amin;
            }
        }
        $user_id = \Illuminate\Support\Facades\Session::get('user_id');

        $files = \Illuminate\Support\Facades\Input::file('attachments');
        foreach ($files as $file) {
            if ($file->isValid()) {
                $destinationPath = 'images/limo_image'; // upload path
                $extension = $file->getClientOriginalExtension(); // getting image extension
                $name = rand(11111, 99999) . '.' . $extension;
                $fileName[] = $name;

                $file->move($destinationPath, $name); // uploading file to given path   
            }
        }
        if ($fileName) {
            $array = [
                "user_id" => $user_id,
                "limo_type" => $_POST['limo_type'],
                "veh_name" => $_POST['veh_name'],
                "veh_des" => $_POST['veh_des'],
                "veh_rules" => $_POST['veh_rules'],
                "color" => $_POST['color'],
                "veh_seats" => $_POST['veh_seats'],
                "veh_price" => $_POST['veh_price'],
                "veh_booking_mintime" => $_POST['veh_booking_mintime'],
                "service_area" => $_POST['service_area'],
                'lat' => $_POST['lat'],
                'lng' => $_POST['lng'],
                "veh_policy" => $_POST['veh_policy'],
                "owner_country" => $_POST['owner_country'],
                "owner_state" => $_POST['owner_state'],
                "owner_city" => $_POST['owner_city'],
                "owner_address" => $_POST['owner_address'],
                "is_active" => 1,
                "amenities" => $amin
            ];
            $id = DB::table('vehicles')->insertGetId($array);
            $i = 1;
            foreach ($fileName as $image_name):
                $saveimage = new \App\Vehimages;
                $saveimage->user_id = $user_id;
                $saveimage->vehicals_id = $id;
                $saveimage->image_name = $image_name;
                if ($i == 1) {
                    $saveimage->main_img = '1';
                }
                $saveimage->save();
                $i++;
            endforeach;
            \Illuminate\Support\Facades\Session::flash('success', 'Inserted successfully');
            return \Illuminate\Support\Facades\Redirect::to('user_add_limo');
        } else {
            \Illuminate\Support\Facades\Session::flash('error', 'Ivalid Files');
            return \Illuminate\Support\Facades\Redirect::to('user_add_limo');
        }
    }

    function update_limo() {
        $amin = '';
        if (isset($_POST['amen_title'])) {
            foreach ($_POST['amen_title'] as $test) {
                $amin = $test . ',' . $amin;
            }
        }
        $vehicals = \App\Vehicals::where('limo_id', '=', $_POST['limo_id'])->update([
            'veh_name' => $_POST['veh_name'],
            'veh_rules' => $_POST['veh_rules'],
            'amenities' => $amin,
            'veh_des' => $_POST['veh_des'],
            'veh_seats' => $_POST['veh_seats'],
            'veh_price' => $_POST['veh_price'],
            'lat' => $_POST['lat'],
            'lng' => $_POST['lng'],
            'color' => $_POST['color'],
            'veh_policy' => $_POST['veh_policy'],
            'veh_booking_mintime' => $_POST['veh_booking_mintime'],
            'service_area' => $_POST['service_area'],
            'owner_country' => $_POST['country_name'],
            'owner_state' => $_POST['owner_state'],
            'owner_city' => $_POST['owner_city'],
            'owner_address' => $_POST['owner_address'],
            'limo_type' => $_POST['limo_type']
        ]);
        \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
        return \Illuminate\Support\Facades\Redirect::to('update_limo/' . $_POST['limo_id']);
    }

    public function update_company_profile() {
        if ($_FILES['comp_image']['name']) {
            if (\Illuminate\Support\Facades\Input::file('comp_image')->isValid()) {
                $destinationPath = 'images/comp_images'; // upload path
                $extension = \Illuminate\Support\Facades\Input::file('comp_image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                \Illuminate\Support\Facades\Input::file('comp_image')->move($destinationPath, $fileName); // uploading file to given path
                $company = DB::table('company')->where('user_id', '=', $_POST['user_id'])->get();
                if ($company) {
                    $company_update = \App\Company::where('user_id', '=', $_POST['user_id'])->update([
                        'comp_name' => $_POST['comp_name'],
                        'comp_f_name' => $_POST['comp_f_name'],
                        'comp_image' => $fileName,
                        'comp_l_name' => $_POST['comp_l_name'],
                        'comp_phone' => $_POST['comp_phone'],
                        'comp_mobile' => $_POST['comp_mobile'],
                        'comp_email' => $_POST['comp_email'],
                        'comp_biz_url' => $_POST['comp_biz_url'],
                        'comp_info' => $_POST['comp_info']
                    ]);
                    \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                    return \Illuminate\Support\Facades\Redirect::to('user_company_profile');
                } else {
                    $company_update = new \App\Company;
                    $company_update->comp_name = $_POST['comp_name'];
                    $company_update->comp_f_name = $_POST['comp_f_name'];
                    $company_update->comp_image = $fileName;
                    $company_update->comp_l_name = $_POST['comp_l_name'];
                    $company_update->comp_phone = $_POST['comp_phone'];
                    $company_update->comp_mobile = $_POST['comp_mobile'];
                    $company_update->comp_email = $_POST['comp_email'];
                    $company_update->comp_biz_url = $_POST['comp_biz_url'];
                    $company_update->comp_info = $_POST['comp_info'];
                    $company_update->user_id = $_POST['user_id'];
                    $company_update->save();
                    \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                    return \Illuminate\Support\Facades\Redirect::to('user_company_profile');
                }
            } else {
                \Illuminate\Support\Facades\Session::flash('error', 'Invalid File Type');
                return \Illuminate\Support\Facades\Redirect::to('user_company_profile');
            }
        } else {
            $company = DB::table('company')->where('user_id', '=', $_POST['user_id'])->get();
            if ($company) {
                $company_update = \App\Company::where('user_id', '=', $_POST['user_id'])->update([
                    'comp_name' => $_POST['comp_name'],
                    'comp_f_name' => $_POST['comp_f_name'],
                    'comp_l_name' => $_POST['comp_l_name'],
                    'comp_phone' => $_POST['comp_phone'],
                    'comp_mobile' => $_POST['comp_mobile'],
                    'comp_email' => $_POST['comp_email'],
                    'comp_biz_url' => $_POST['comp_biz_url'],
                    'comp_info' => $_POST['comp_info']
                ]);
                \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                return \Illuminate\Support\Facades\Redirect::to('user_company_profile');
            } else {
                $company_update = new \App\Company;
                $company_update->comp_name = $_POST['comp_name'];
                $company_update->comp_f_name = $_POST['comp_f_name'];
                $company_update->comp_l_name = $_POST['comp_l_name'];
                $company_update->comp_phone = $_POST['comp_phone'];
                $company_update->comp_mobile = $_POST['comp_mobile'];
                $company_update->comp_email = $_POST['comp_email'];
                $company_update->comp_biz_url = $_POST['comp_biz_url'];
                $company_update->comp_info = $_POST['comp_info'];
                $company_update->user_id = $_POST['user_id'];
                $company_update->save();
                \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
                return \Illuminate\Support\Facades\Redirect::to('user_company_profile');
            }
        }
    }
    
    function billing_address_get() {
        $id = \Illuminate\Support\Facades\Session::get('user_id');
        $creditCardToken = $_POST['stripeToken'];
        $user = \App\User::find($id);

        if ($user->subscribed()) {
           
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
            $rp = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
            if ($user->default_card == 0) {
               
                $user->default_card = $rp->data[1]->id;
                \App\User::where('id', '=', $id)->update(['default_card' => 1]);
            }

            $source = array(
                'cvc' => $_POST['cvc'],
                'number' => $_POST['number'],
                'exp_month' => $_POST['exp_month'],
                'exp_year' => $_POST['exp_year'],
                'object' => 'card'
            );
            $rp->source = $source;
            $rp->save();
            \App\User::where('id', '=', $id)->update(['account_status' => 1, 'last_four' => $rp->sources->data[0]->last4]);
            $payments = DB::table('save_payments')->where('user_id', '=', $id)->get();
            if ($payments[0]->total_amount < 25) {
                
                $details = $user->charge(10000);
                if ($details['status'] != 'succeeded') {
                    \App\User::where('id', '=', $id)->update('account_status', '=', 0);
                } else {
                                  $savepayment = new \App\Payment_details;
                                  $savepayment->user_id = $id;
                                  $savepayment->transaction_id = $details->id;
                                  $savepayment->amount = 100;
                                  $savepayment->remaing_amount = 100;
                                  $savepayment->save();
                                  $amount = \App\Savepayments::where('user_id', '=', $id);
                                  $amount->increment('total_amount', 100);
                    $amount = \App\Savepayments::where('user_id', '=', $id);
                    $amount->increment('total_amount', 100);
                }
            }
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('user_billing_address');
        } else {
//            try{
                $user->subscription('Instalimos2')->create($creditCardToken);
            
//            } catch(\Stripe\Error\Card $e) {
//
//                dd("Card declined");
//            } catch (\Stripe\Error\InvalidRequest $e) {
//                dd("Invalid parameters were supplied to Stripe's API");
//            } catch (\Stripe\Error\Authentication $e) {
//                dd("Authentication with Stripe's API failed");
//            } catch (\Stripe\Error\ApiConnection $e) {
//                dd("Network communication with Stripe failed");
//            } catch (\Stripe\Error\Base $e) {
//                dd("Display a very generic error to the user");
//            } catch (Exception $e) {
//                dd("Something else happened, completely unrelated to Stripe");
//            }
           $details = $user->charge(10000);
            \App\User::where('id', '=', $id)->update(['account_status' => 1]);
            \App\User::where('id', '=', $id)->update(['default_card' => 1]);
                                  $savepayment = new \App\Payment_details;
                                  $savepayment->user_id = $id;
                                  $savepayment->transaction_id = $details->id;
                                  $savepayment->amount = 100;
                                  $savepayment->remaing_amount = 100;
                                  $savepayment->save();
            DB::table('save_payments')->insert(
                    ['user_id' => $id,
                        'total_amount' => '100']);
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('user_billing_address');
        }
    }
    function add_other_card() {
        $id = \Illuminate\Support\Facades\Session::get('user_id');
        $user = \App\User::find($id);
        $sec_card = DB::table('craditcard_details')->where('user_id', '=', $id)->get();
        if ($sec_card) {
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
            $customer = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
            
            $card = $customer->sources->retrieve($sec_card[0]->stripe_c_id);
            
            $card->delete();
            $card = $customer->sources->create(array("source" => $_POST['stripeToken']));
            DB::table('craditcard_details')->where('user_id', '=', $id)->delete();
            $savecard = new \App\Craditcard;
            $savecard->user_id = $id;
            $savecard->c_num_sec = $card->last4;
            $savecard->c_cvc_sec = $_POST['cvc'];
            $savecard->c_exp_sec = $card->exp_month . '-' . $card->exp_year;
            $savecard->stripe_c_id = $card->id;
            $savecard->save();
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('user_billing_address');
        } else {
            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
            $cu = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
            $card = $cu->sources->create(array("source" => $_POST['stripeToken']));
            $savecard = new \App\Craditcard;
            $savecard->user_id = $id;
            $savecard->c_num_sec = $card->last4;
            $savecard->c_cvc_sec = $_POST['cvc'];
            $savecard->c_exp_sec = $card->exp_month . '-' . $card->exp_year;
            $savecard->stripe_c_id = $card->id;
            $savecard->save();
            \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
            return \Illuminate\Support\Facades\Redirect::to('user_billing_address');
        }
    }
//    function checkpassword() {
//        $user_id = \Illuminate\Support\Facades\Session::get('user_id');
//        $user = \App\User::find($user_id);
//        $password = $user->password;
//
//        if (\Illuminate\Support\Facades\Hash::check($_GET['password'], $password)) {
//            echo TRUE;
//        } else {
//            echo False;
//        }
//    }
    
    function changepassword() {
        $id = \Illuminate\Support\Facades\Session::get('user_id');
        $newpass = \Illuminate\Support\Facades\Hash::make($_POST['password']);
        \App\User::where('id', '=', $id)->update(['password' => $newpass]);
        \Illuminate\Support\Facades\Session::flash('success', 'Updated successfully');
        return \Illuminate\Support\Facades\Redirect::to('user_account_info');
    }
    
    
    
    function change_password() {
        $id = \Illuminate\Support\Facades\Session::get('user_id');
        $newpass = \Illuminate\Support\Facades\Hash::make($_POST['password']);

        $user = \App\User::where('id', '=', $id)->first();
        $success=DB::transaction(function() use ($user, $newpass)
        {
            $user->password = $newpass;
            $user->save();
            return true;
        });
        
        if($success)
        {
            \Illuminate\Support\Facades\Session::flash('success', 'Password changed successfully.');
        }
        else{
            \Illuminate\Support\Facades\Session::flash('error', 'Error password change.');
        }
        return \Illuminate\Support\Facades\Redirect::to('detail/'.$id);
    }
    public function  update_calendar(){
        $dates='';
        foreach ($_GET['dates'] as $date){
            $dates="'$date'".','.$dates;
        }
        \App\Vehicals::where('limo_id',$_GET['limo_id'])->update(['limo_cal' => $dates]);
        
    }

}
