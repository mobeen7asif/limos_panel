<?php

namespace App\Http\Controllers;

use Auth;
use \Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Session;
class WelcomeController extends Controller {

    public function __construct() {
//        $this->middleware('guest');
    }

    public function login_view() {
        if (Auth::user()) {
            return \Illuminate\Support\Facades\Redirect::to('admindashboard');
        } else {
            return \Illuminate\Support\Facades\View::make('admin/admin_login', array('title' => 'Instalimos'));
        }
    }

    public function index() {
        $data['title'] = 'Instalimos';
        $data['types'] = \App\Limotypes::all()->toArray();
        $data['events'] = \App\Events::all()->toArray();
        return view('index', $data);
    }

    public function register() {
        $user = [
            'email' => $_POST['email'],
            'password' => bcrypt($_POST['password']),
            'role' => 3
        ];
        $id = DB::table('users')->insertGetId($user);
        $user_id = $id;
        $company = new \App\Company();
        $company->comp_mobile = $_POST['comp_mobile'];
        $company->comp_name = $_POST['comp_name'];
        $company->user_id = $user_id;
        $company->aboutus = $_POST['aboutus'];
        $company->city = $_POST['city'];
        $company->zipcode = $_POST['zipcode'];
        $company->address_2 = $_POST['address_2'];
        $company->advertised_bef = $_POST['advertised_bef'];
        $company->state_1 = $_POST['state_1'];
        $company->country_1 = $_POST['country_1'];
        $company->address_1 = $_POST['address_1'];
        $company->autherity_name = $_POST['autherity_name'];
        $company->permit_number = $_POST['permit_number'];
        $company->biz_date = $_POST['biz_date'];
        $company->insured_vehicals = $_POST['insured_vehicals'];
        $company->comp_l_name = $_POST['comp_l_name'];
        $company->comp_f_name = $_POST['comp_f_name'];
        $company->comp_biz_url = $_POST['comp_biz_url'];
        $company->comp_phone = $_POST['comp_phone'];
        $company->comp_email = $_POST['comp_email'];
        $company->dba_name = $_POST['dba_name'];
        $company->save();
        $data['name'] = $_POST['comp_name'];
        $emaildata = array('to' => $_POST['email'], 'to_name' => $_POST['comp_name']);
        \Illuminate\Support\Facades\Mail::send('email_register_request', $data, function($message) use ($emaildata) {
            $message->to($emaildata['to'], $emaildata['to_name'])
                    ->from('no-reply@instalimos.com', 'InstaLimos')
                    ->subject('We have received your application');
        });
        return \Illuminate\Support\Facades\Redirect::to('suucess');
    }

    public function authenticate() {
        if (Auth::attempt(['email' => $_GET['email'], 'password' => $_GET['password'], 'role' => 3])) {
            if (Auth::user()->is_active == 1) {
                echo TRUE;
            } else {
                Auth::logout();
                echo '2';
            }
        } else {
            echo FALSE;
        }
    }

    public function adminlogin($remember = 1) {
        if (Auth::attempt(['email' => $_POST['email'], 'password' => $_POST['password'], 'role' => 1, 'is_active' => 1], $remember)) {
            return \Illuminate\Support\Facades\Redirect::to('admindashboard');
        } else if (Auth::attempt(['email' => $_POST['email'], 'password' => $_POST['password'], 'role' => 2, 'is_active' => 1], $remember)) {
            return \Illuminate\Support\Facades\Redirect::to('admindashboard');
        } else {
            Session::flash('error', 'Invalid Email Or password');
            return \Illuminate\Support\Facades\Redirect::to('adminlogin');
        }
    }

    public function authenticate_email() {
        $getvister = DB::table('users')->where('email', '=', $_GET['email'])->get();
        if ($getvister) {
            echo FALSE;
        } else {
            echo TRUE;
        }
    }

    public function get_cities() {
        $data = \App\Cities::all()->toArray();
        echo json_encode($data);
    }

    public function save_vister() {
        $searchquery = Session::get('searchdata');
        $searchdate = date('m/d/Y', strtotime($searchquery['daterequired']));
        if (isset($searchquery['locality'])) {
            if (isset($searchquery['locality'])) {
                $city = $searchquery['locality'];
            } else {
                $city = '';
            }
            if (isset($searchquery['administrative_area_level_1'])) {
                $state = $searchquery['administrative_area_level_1'];
            } else {
                $state = '';
            }
            if (isset($searchquery['country'])) {
                $country = $searchquery['country'];
            } else {
                $country = '';
            }
            if (isset($_POST['notification'])) {
                $notification = $_POST['notification'];
            } else {
                $notification = 0;
            }

            $sort = "FIELD(vehicles.owner_city , '$city') DESC";


            $companies = DB::table('vehicles')
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('users.account_status', '=', 1)
                    ->where('users.deactive_account', '=', 0)
                    ->where('users.time_out', '=', 0)
                    ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                    ->where('vehicles.veh_seats', '>=', $searchquery['passengers'])
                    ->where('vehicles.veh_seats', '<=', $searchquery['passengers'] * 3)
                    ->where('vehicles.owner_country', '=', $country)
                    ->where('vehicles.is_active', '=', 1)
                    ->groupBy('vehicles.user_id')
                    ->get();
            $companycount = array();
            foreach ($companies as $companydata) {
                if ($searchquery['lat'] && $companydata->lng) {
                    $point1_lat = $searchquery['lat'];
                    $point1_lng = $searchquery['lng'];
                    $point2_lat = $companydata->lat;
                    $point2_lng = $companydata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    if ($miles <= $companydata->service_area) {
                        $companycount[] = $companydata;
                    }
                }
            }
            $result = count($companycount);
            $array = [
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'email' => $_POST['email'],
                'contact' => $_POST['contact'],
                'notification' => $notification,
                'occian_type' => $searchquery['occian_type'],
                'location' => $searchquery['picklocation'],
                'passengers' => $searchquery['passengers'],
                'pickdate' => $searchquery['daterequired'],
                'no_of_companies' => $result 
            ];

//        if ($city) {
//            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
//        } elseif ($state) {
//            $sort = "FIELD(vehicles.owner_state , '$state') DESC";
//        } else {
//            $sort = "FIELD(vehicles.owner_country , '$country') DESC";
//        }
//            $getvister = DB::table('visters')->where('email', '=', $_POST['email'])->get();
//            if ($getvister) {
//                $id = $getvister[0]->id;
//            } else {
                $id = DB::table('visters')->insertGetId($array);
//            }
//            echo $id;exit;
            $searchcompnies = new \App\searched_compnies();
            $searchcompnies->compnies_searched = $result ;
            $searchcompnies->vister_id = $id;
            $searchcompnies->save();
            $data['types'] = \App\Limotypes::all()->toArray();
            $neadta1 = array(
                'limo_type_id' => 1,
                'limo_title' => 'All Types',
                'created_at' => '-0001-11-30 00:00:00',
                'updated_at' => '-0001-11-30 00:00:00',
            );
            $data['types'][] = $neadta1;
            usort($data['types'], function($a, $b) {
                return $a['limo_type_id'] - $b['limo_type_id'];
            });
            $sdata = DB::table('vehicles')
                    ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                    ->where('vehimages.main_img', 1)
                    ->where('users.account_status', '=', 1)
                    ->where('users.deactive_account', '=', 0)
                    ->where('users.time_out', '=', 0)
                    ->where('vehicles.veh_seats', '>=', $searchquery['passengers'])
                    ->where('vehicles.veh_seats', '<=', $searchquery['passengers'] * 3)
                    ->where('vehicles.owner_country', '=', $country)
//                ->where('vehicles.limo_type', $searchquery['limo_type'])
                    ->groupBy('vehicles.limo_id')
                    ->where('vehicles.is_active', '=', 1)
                    ->orderByRaw($sort)
                    ->orderBy('count', 'desc')
                    // ->take(10)
                    ->get();
//            $savesearch = new \App\Search_record;
//            $savesearch->location = $searchquery['picklocation'];
//            $savesearch->passengers =$searchquery['passengers'];
//            $savesearch->veh_type ='All';
//            $savesearch->veh_amen ='All';
//            $savesearch->count = count($sdata);
//            $savesearch->save();

            $data['maxcount'] = 0;
            $getdistance = '';
            foreach ($sdata as $newdata) {
                if ($searchquery['lat'] && $newdata->lng) {
                    $point1_lat = $searchquery['lat'];
                    $point1_lng = $searchquery['lng'];
                    $point2_lat = $newdata->lat;
                    $point2_lng = $newdata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    if ($miles <= $newdata->service_area) {
                        $getdistance[] = $newdata;
                    }
                }
            }
            if (!$getdistance) {
                $getdistance = [];
            }
            $data['searchdata'] = $getdistance;
            foreach ($data['searchdata'] as $lead) {
                $limo_id[] = $lead->limo_id;
                if ($lead->veh_price > $data['maxcount']) {
                    $data['maxcount'] = $lead->veh_price;
                }
                $getvister = DB::table('leeds_meesage')->where('vister_email', '=', $_POST['email'])->where('lemo_searched', '=', $lead->limo_id)->get();
                $getuser = DB::table('users')->where('email', '=', $_POST['email'])->get();
                $checkVister = DB::table('leeds_meesage')
                        ->where('user_id', '=', $lead->id)
                        ->where('vister_email', '=', $_POST['email'])
                        ->where(\DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), \DB::raw('curdate()'))
                        ->get();
                if (!$checkVister && !$getuser) {
                    Session::put('limo_id', $limo_id);
                    $getvitername = DB::table('visters')->where('id', '=', $id)->get();
                    $leads = new \App\Leeds;
                    $leads->searched_by_id = $id;
                    $leads->vister_email = $_POST['email'];
                    $leads->lemo_searched = $lead->limo_id;
                    $leads->user_id = $lead->user_id;
                    $leads->passengers = $searchquery['passengers'];
                    $leads->occian_type = $searchquery['occian_type'];
                    $leads->picklocation = $searchquery['picklocation'];
                    $leads->search_date = $searchquery['daterequired'];
                    $leads->comp_id = $lead->comp_id;
                    $leads->save();
                    $data['text'] = $leads;
                    $data['fname'] = $getvitername[0]->firstname;
                    $data['lname'] = $getvitername[0]->lastname;
                    if ($getvitername[0]->contact) {
                        $contact = substr($getvitername[0]->contact, 0, 0) . "(" . substr($getvitername[0]->contact, 0, 3) . ") " . substr($getvitername[0]->contact, 3, 3) . "-" . substr($getvitername[0]->contact, 6);
                    } else {
                        $contact = '';
                    }
                    $data['contact'] = $contact;
                    $emaildata = array('replyto' => $_POST['email'], 'replytoname' => $_POST['firstname'], 'to' => $lead->comp_email, 'to_name' => $lead->username);
                    \Illuminate\Support\Facades\Mail::send('emails_welcome', $data, function($message) use ($emaildata) {
                        $message->to($emaildata['to'], 'Dear User')
                                ->from('no-reply@instalimos.com', 'InstaLimos')
                                ->replyTo($emaildata['replyto'], $emaildata['replytoname'])
                                ->subject('Timely: Instalimos Inquiry');
                    });
                    $payments = DB::table('save_payments')->where('user_id', '=', $lead->user_id)->get();
                    if ($payments[0]->total_amount < 0) {
                        $user = \App\User::find($lead->user_id);
                        $details = $user->charge(10000);
                        if (!$user->charge(10000)) {
                            $othercard = DB::table('craditcard_details')->where('user_id', '=', $lead->user_id)->get();
                            if ($othercard) {
                                \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
                                $rp = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
                                $rp->default_card = $othercard[0]->stripe_c_id;
                                $rp->save();
                                $amount = $user->charge(10000);
                                if (!$user->charge(10000)) {
                                    \App\User::where('id', '=', $lead->user_id)->update(['account_status'=>0]);
                                } else {
                                    $savepayment = new \App\Payment_details;
                                    $savepayment->user_id = $lead->user_id;
                                    $savepayment->transaction_id = $amount->id;
                                    $savepayment->amount = 100;
                                    $savepayment->save();
                                    $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                                    $amount->increment('total_amount', 100);
                                    \App\User::where('id', '=', $lead->user_id)->update(['default_card'=> 0]);
                                }
                            } else {
                                \App\User::where('id', '=', $lead->user_id)->update(['account_status'=> 0]);
                            }
                        } else {
                            $savepayment = new \App\Payment_details;
                            $savepayment->user_id = $lead->user_id;
                            $savepayment->transaction_id = $details->id;
                            $savepayment->amount = 100;
                            $savepayment->save();
                            $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                            $amount->increment('total_amount', 100);
                        }
                    } else {
                        $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                        $amount->decrement('total_amount', 2);
                    }
                }
            }
            Session::put('id', $id);
            Session::put('v_email', $_POST['email']);
            Session::put('v_fname', $_POST['firstname']);
            Session::put('v_lname', $_POST['lastname']);
            
            $data['title'] = 'Instalimos';
            $data['searchquery'] = Session::get('searchdata');
            $data['aminties'] = \App\Limoamen::all()->toArray();
            $neadta = array(
                'amen_id' => 1,
                'amen_title' => 'No Amenities',
                'created_at' => '-0001-11-30 00:00:00',
                'updated_at' => '-0001-11-30 00:00:00',
            );
            $data['aminties'][] = $neadta;
            foreach ($data['aminties'] as $key => $row) {
//    print_r($row);exit;
                $dates[$key] = $row['amen_id'];
                // of course, replace 0 with whatever is the date field's index
            }

            array_multisort($dates, SORT_ASC, $data['aminties']);

            return view('searchedpage', $data);
        } else {
            Session::flush();
            return \Illuminate\Support\Facades\Redirect::to('/');
        }
    }

    public function showsearchpage() {
        $id = \Illuminate\Support\Facades\Request::segment(2);
        $lid = \Illuminate\Support\Facades\Request::segment(3);
        $data['user'] = \App\User::find($id);
        $data['photo'] = \App\User::find($id)->images()->where('vehicals_id', '=', $lid)->get();
        $data['limo'] = \App\User::find($id)->vehicals()->where('limo_id', '=', $lid)->get();
        $data['company'] = \App\User::find($id)->company()->get();
        $data['reviews'] = DB::table('reviews')
                ->join('visters', 'visters.id', '=', 'reviews.reviewed_by_id')
                ->where('reviews.vehicals_id', '=', $lid)
                ->orderBy('reviews.created_at', 'desc')
                ->get();
        $data['rating'] = \App\User::find($id)->review()->where('vehicals_id', '=', $lid)->avg('review_rating');
        $data['count'] = \App\User::find($id)->review()->where('vehicals_id', '=', $lid)->count('review_rating');
        $data['aminties'] = \App\Limoamen::all()->toArray();
        $data['related_limo'] = DB::table('vehicles')
                ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                ->join('users', 'users.id', '=', 'vehicles.user_id')
                ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                ->where('vehicles.owner_city', '=', $data['limo'][0]->owner_city)
                ->where('vehimages.main_img', 1)
                ->where('vehicles.limo_id', '!=', $lid)
                ->where('vehicles.is_active', '=', 1)
                ->take(3)
                ->get();
//        echo '<pre>';
//        print_r($data['related_limo']);exit;
        $data['title'] = 'Holy Limo! Check out this sweet ride @instalimos. Everything is better #inalimo';
        $data['sum'] = round($data['rating']);
        return view('company_page', $data);
    }

    public function getcustomsearch() {
        $v_email = Session::get('v_email');
        $searchquery = Session::get('searchdata');
        $searchdate = date('m/d/Y', strtotime($_POST['searchdate']));
        if (isset($_POST['locality'])) {
            $city = $_POST['locality'];
            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        } elseif ($_POST['city']) {
            $city = $_POST['city'];
            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        } elseif (isset($searchquery['locality'])) {
            $city = $searchquery['locality'];
            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        }
        if ($city) {
            if (($_POST['country'])) {
                $country = $_POST['country'];
            } elseif (($_POST['country1'])) {
                $country = $_POST['country1'];
            } elseif (($searchquery['country'])) {
                $country = $searchquery['country'];
            }
            $id = Session::get('id');
            $companies = DB::table('vehicles')
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('users.account_status', '=', 1)
                    ->where('users.deactive_account', '=', 0)
                    ->where('users.time_out', '=', 0)
                    ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                    ->where('vehicles.veh_seats', '>=', intval($_POST['passenger']))
                    ->where('vehicles.veh_seats', '<=', intval($_POST['passenger']) * 3)
                    ->where('vehicles.owner_country', '=', $country)
                    ->orderByRaw($sort)
                    ->where('vehicles.is_active', '=', 1)
                    ->groupBy('vehicles.user_id')
                    ->get();
            $companycount = array();
            foreach ($companies as $companydata) {
                if ($searchquery['lat'] && $companydata->lng) {
                    $point1_lat = $searchquery['lat'];
                    $point1_lng = $searchquery['lng'];
                    $point2_lat = $companydata->lat;
                    $point2_lng = $companydata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    if ($miles <= $companydata->service_area) {
                        $companycount[] = $companydata;
                    }
                }
            }
            $result = count($companycount);
            $searchcompnies = new \App\searched_compnies();
            
            $searchcompnies->compnies_searched = $result ;
            $searchcompnies->vister_id = $id;
            $searchcompnies->save();
            $sdata = DB::table('vehicles')
                    ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('users.deactive_account', '=', 0)
                    ->where('vehicles.is_active', '=', 1)
                    ->where('users.account_status', '=', 1)
                    ->where('vehicles.veh_seats', '>=', intval($_POST['passenger']))
                    ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                    ->where('vehicles.veh_seats', '<=', $_POST['passenger'] * 3)
                    ->where('vehicles.owner_country', '=', $country)
                    ->groupBy('vehicles.limo_id')
                    ->orderByRaw($sort)
                    ->orderBy('count', 'desc')
                    ->get();
            
            $email = Session::get('v_email');
            $data['maxcount'] = 0;
            $getdistance = '';
            foreach ($sdata as $newdata) {
                if ($_POST['lat']) {
                    $point1_lat = $_POST['lat'];
                    $point1_lng = $_POST['lng'];
                    $point2_lat = $newdata->lat;
                    $point2_lng = $newdata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;

                    if ($miles <= $newdata->service_area) {
                        $getdistance[] = $newdata;
                    }
                }
            }
            if (!$getdistance) {
                $getdistance = [];
            }
            $data['searchdata'] = $getdistance;
            foreach ($data['searchdata'] as $lead) {
                $i = 0;
                $sessionlimos = Session::get('limo_id');
//            print_r($sessionlimos);exit;
                if ($sessionlimos) {
                    foreach ($sessionlimos as $limo) {
                        if ($limo == $lead->limo_id) {
                            $i + 1;
                        }
                    }
                }
                if ($lead->veh_price > $data['maxcount']) {
                    $data['maxcount'] = $lead->veh_price;
                }
                $getvister = DB::table('leeds_meesage')->where('vister_email', '=', $email)->where('lemo_searched', '=', $lead->limo_id)->get();
                $checkVister = DB::table('leeds_meesage')
                        ->where('user_id', '=', $lead->id)
                        ->where('vister_email', '=', $v_email)
                        ->where(\DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), \DB::raw('curdate()'))
                        ->get();
                $getuser = DB::table('users')->where('email', '=', $email)->get();
                if (!$checkVister && $i > 0) {
                    $sessionlimos[] = $lead['limo_id'];
                    Session::put('limo_id', $sessionlimos);
                    $getvitername = DB::table('visters')->where('id', '=', $id)->get();
                    $leads = new \App\Leeds;
                    $leads->searched_by_id = $id;
                    $leads->vister_email = $email;
                    $leads->comp_id = $searchcompnies->comp_id;
                    $leads->lemo_searched = $lead->limo_id;
                    $leads->user_id = $lead->user_id;
                    $leads->passengers = $_POST['passenger'];
                    $leads->occian_type = $searchquery['occian_type'];
                    $leads->picklocation = $_POST['picloc'];
                    $leads->search_date = $searchquery['daterequired'];
                    $leads->save();
                    $data['text'] = $leads;
                    $data['fname'] = $getvitername[0]->firstname;
                    $data['lname'] = $getvitername[0]->lastname;
                    if ($getvitername[0]->contact) {
                        $contact = substr($getvitername[0]->contact, 0, 0) . "(" . substr($getvitername[0]->contact, 0, 3) . ") " . substr($getvitername[0]->contact, 3, 3) . "-" . substr($getvitername[0]->contact, 6);
                    } else {
                        $contact = '';
                    }
                    $data['contact'] = $contact;
                    $emaildata = array('replyto' => $email, 'replytoname' => $getvitername[0]->firstname, 'to' => $lead->email, 'to_name' => $lead->username);
                    \Illuminate\Support\Facades\Mail::send('emails_welcome', $data, function($message) use ($emaildata) {
                        $message->to($emaildata['to'], 'Dear User')
                                ->from('no-reply@instalimos.com', 'InstaLimos')
                                ->replyTo($emaildata['replyto'], $emaildata['replytoname'])
                                ->subject('Timely: Instalimos Inquiry');
                    });
                    $payments = DB::table('save_payments')->where('user_id', '=', $lead->user_id)->get();
                    if ($payments[0]->total_amount < 25) {
                        $user = \App\User::find($lead->user_id);
                        $user->charge(10000);
                        if (!$user->charge(10000)) {
                            $othercard = DB::table('craditcard_details')->where('user_id', '=', $lead->user_id)->get();
                            if ($othercard) {
                                \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
                                $rp = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
                                $rp->default_card = $othercard[0]->stripe_c_id;
                                $rp->save();
                                $user->charge(10000);
                                if (!$user->charge(10000)) {
                                    \App\User::where('id', '=', $lead->user_id)->update(['account_status'=> 0]);
                                } else {
                                    $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                                    $amount->increment('total_amount', 100);
                                    \App\User::where('id', '=', $lead->user_id)->update(['default_card'=>0]);
                                }
                            } else {
                                \App\User::where('id', '=', $lead->user_id)->update(['account_status' => 0]);
                            }
                        } else {
                            $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                            $amount->increment('total_amount', 100);
                        }
                    } else {
                        $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                        $amount->decrement('total_amount', 2);
                    }
                }
            } if (isset($_POST['locality'])) {
                $scity = $_POST['locality'];
            } else {
                $scity = $_POST['city'];
            }
            $data['searchquery'] = array(
                'locality' => $scity,
                'administrative_area_level_1' => $_POST['state'],
                'country' => $_POST['country1'],
                'passengers' => $_POST['passenger']
//            'limo_type'=>$searchquery['limo_type']
            );
            $data['title'] = 'Instalimos';

            $data['types'] = \App\Limotypes::all()->toArray();
            $neadta1 = array(
                'limo_type_id' => 1,
                'limo_title' => 'All Types',
                'created_at' => '-0001-11-30 00:00:00',
                'updated_at' => '-0001-11-30 00:00:00',
            );
            $data['types'][] = $neadta1;
            usort($data['types'], function($a, $b) {
                return $a['limo_type_id'] - $b['limo_type_id'];
            });

            $data['aminties'] = \App\Limoamen::all()->toArray();
            $neadta = array(
                'amen_id' => 1,
                'amen_title' => 'No Amenities',
                'created_at' => '-0001-11-30 00:00:00',
                'updated_at' => '-0001-11-30 00:00:00',
            );
            $data['aminties'][] = $neadta;
            foreach ($data['aminties'] as $key => $row) {
//    print_r($row);exit;
                $dates[$key] = $row['amen_id'];
                // of course, replace 0 with whatever is the date field's index
            }
            array_multisort($dates, SORT_ASC, $data['aminties']);
            return view('searchedpage', $data);
        } else {
            Session::flush();
            return \Illuminate\Support\Facades\Redirect::to('/');
        }
    }

    public function getjssearch() {
        $v_email = Session::get('v_email');
        $id = Session::get('id');
        $searchquery = Session::get('searchdata');
        $searchdate = date('m/d/Y', strtotime($_GET['searechdate']));
        $city = $searchquery['locality'];
//        $state = $searchquery['administrative_area_level_1'];
        if ($_GET['country']) {
            $country = $_GET['country'];
        } else {
            $country = $searchquery['country'];
        }
        if ($_GET['city']) {
            $city = $_GET['city'];
            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        } elseif ($city) {
            $city = $searchquery['locality'];
            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        }
        $price = intval($_GET['price']);
        
        $companies = DB::table('vehicles')
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                ->where('vehimages.main_img', 1)
                ->where('users.deactive_account', '=', 0)
                ->where(function($query) {
                    if (isset($_GET['type']) && $_GET['type'] != 1) {
                        $query->where('vehicles.limo_type', '=', $_GET['type']);
                    }
                })
                ->where('vehicles.veh_price', '<=', $price)
                ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                ->where('users.account_status', '=', 1)
                ->where('vehicles.is_active', '=', 1)
                ->where('vehicles.owner_country', '=', $country)
                ->where('vehicles.veh_seats', '>=', $_GET['passengers'])
                ->where('vehicles.veh_seats', '<=', $_GET['passengers'] * 3)
                ->where(function($query) {
                    if (isset($_GET['selected'])) {
                        if ($_GET['selected'][0] != 'No Amenities') {
                            foreach ($_GET['selected'] as $sel):
                                $query->where('vehicles.amenities', 'like', '%' . $sel . '%');
                            endforeach;
                        }
                    }
                })
                    ->groupBy('vehicles.user_id')
                    ->get();
            $companycount = array();
            foreach ($companies as $companydata) {
                if ($searchquery['lat'] && $companydata->lng) {
                    $point1_lat = $searchquery['lat'];
                    $point1_lng = $searchquery['lng'];
                    $point2_lat = $companydata->lat;
                    $point2_lng = $companydata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    if ($miles <= $companydata->service_area) {
                        $companycount[] = $companydata;
                    }
                }
            }
            $result = count($companycount);
            if($result > 1)
            {
                $result = $result-1;
            }
            $searchcompnies = new \App\searched_compnies();
            $searchcompnies->compnies_searched = $result;
            $searchcompnies->vister_id = $id;
            $searchcompnies->save();
            
        $sdata = DB::table('vehicles')
                ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                ->join('users', 'users.id', '=', 'vehicles.user_id')
                ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                ->where('vehimages.main_img', 1)
                ->where('users.deactive_account', '=', 0)
                ->where(function($query) {
                    if (isset($_GET['type']) && $_GET['type'] != 1) {
                        $query->where('vehicles.limo_type', '=', $_GET['type']);
                    }
                })
                ->where('vehicles.veh_price', '<=', $price)
                ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                ->where('users.account_status', '=', 1)
                ->where('vehicles.is_active', '=', 1)
                ->where('vehicles.owner_country', '=', $country)
                ->where('vehicles.veh_seats', '>=', $_GET['passengers'])
                ->where('vehicles.veh_seats', '<=', $_GET['passengers'] * 3)
                ->where(function($query) {
                    if (isset($_GET['selected'])) {
                        if ($_GET['selected'][0] != 'No Amenities') {
                            foreach ($_GET['selected'] as $sel):
                                $query->where('vehicles.amenities', 'like', '%' . $sel . '%');
                            endforeach;
                        }
                    }
                })
                ->groupBy('vehicles.limo_id')
                ->orderByRaw($sort)
                ->orderBy('count', 'desc')
//                ->take(10)
                ->get();
        if (isset($_GET['type']) && $_GET['type'] != 1) {
            $types = DB::table('limo_type')->where('limo_type_id', '=', $_GET['type'])->lists('limo_title');
            $type = $types[0];
        } else {
            $type = 'All';
        }
        if (isset($_GET['selected'])) {
            if ($_GET['selected'][0] != 'No Amenities') {
                $selected = '';
                foreach ($_GET['selected'] as $sel) {
                    $selected = $selected . ' ,' . $sel;
                }
            } else {
                $selected = 'All';
            }
        } else {
            $selected = 'All';
        }
        
        $email = Session::get('v_email');
        $getdistance = '';
        foreach ($sdata as $newdata) {
            if ($searchquery['lat']) {
                $point1_lat = $_GET['lat'];
                $point1_lng = $_GET['lng'];
                $point2_lat = $newdata->lat;
                $point2_lng = $newdata->lng;
                $theta = $point1_lng - $point2_lng;
                $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                $miles = acos($miles);
                $miles = rad2deg($miles);
                $miles = $miles * 60 * 1.1515;
                if ($miles <= $newdata->service_area) {
                    $getdistance[] = $newdata;
                }
            }
        }if (!$getdistance) {
            $getdistance = [];
        }
        $data = $getdistance;
        foreach ($data as $lead) {
            $getvister = DB::table('leeds_meesage')->where('vister_email', '=', $email)->where('lemo_searched', '=', $lead->limo_id)->get();
            $checkVister = DB::table('leeds_meesage')
                    ->where('user_id', '=', $lead->id)
                    ->where('vister_email', '=', $v_email)
                    ->where(\DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), \DB::raw('curdate()'))
                    ->get();
            $getuser = DB::table('users')->where('email', '=', $email)->get();
            $getvitername = DB::table('visters')->where('id', '=', $id)->get();
            if (!$checkVister && !$getuser) {
                $leads = new \App\Leeds;
                $leads->searched_by_id = $id;
                $leads->vister_email = $email;
                $leads->lemo_searched = $lead->limo_id;
                $leads->comp_id = $searchcompnies->comp_id;
                $leads->user_id = $lead->user_id;
                $leads->passengers = $_GET['passengers'];
                $leads->occian_type = $searchquery['occian_type'];
                $leads->picklocation = $_GET['picklocation'];
                $leads->search_date = $searchquery['daterequired'];
                $leads->save();
                $data['text'] = $leads;
                $data['fname'] = $getvitername[0]->firstname;
                $data['lname'] = $getvitername[0]->lastname;
                if ($getvitername[0]->contact) {
                    $contact = substr($getvitername[0]->contact, 0, 0) . "(" . substr($getvitername[0]->contact, 0, 3) . ") " . substr($getvitername[0]->contact, 3, 3) . "-" . substr($getvitername[0]->contact, 6);
                } else {
                    $contact = '';
                }
                $data['contact'] = $contact;
                $emaildata = array('replyto' => $email, 'replytoname' => $getvitername[0]->firstname, 'to' => $lead->email, 'to_name' => $lead->username);
                \Illuminate\Support\Facades\Mail::send('emails_welcome', $data, function($message) use ($emaildata) {
                    $message->to($emaildata['to'], 'Dear User')
                            ->from('no-reply@instalimos.com', 'InstaLimos')
                            ->replyTo($emaildata['replyto'], $emaildata['replytoname'])
                            ->subject('Timly: Instalimos Inquiry');
                });
                $payments = DB::table('save_payments')->where('user_id', '=', $lead->user_id)->get();
                if ($payments[0]->total_amount < 25) {
                    $user = \App\User::find($lead->user_id);
                    $user->charge(10000);
                    if (!$user->charge(10000)) {
                        $othercard = DB::table('craditcard_details')->where('user_id', '=', $lead->user_id)->get();
                        if ($othercard) {
                            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
                            $rp = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
                            $rp->default_card = $othercard[0]->stripe_c_id;
                            $rp->save();
                            $user->charge(10000);
                            if (!$user->charge(10000)) {
                                \App\User::where('id', '=', $lead->user_id)->update(['account_status'=> 0]);
                            } else {
                                $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                                $amount->increment('total_amount', 100);
                                \App\User::where('id', '=', $lead->user_id)->update(['default_card'=> 0]);
                            }
                        } else {
                            \App\User::where('id', '=', $lead->user_id)->update(['account_status'=> 0]);
                        }
                    } else {
                        $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                        $amount->increment('total_amount', 100);
                    }
                } else {
                    $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                    $amount->decrement('total_amount', 2);
                }
            }
        }
        print_r(json_encode($data));
    }

    public function save_rating() {
        $randimage = \App\Review_images::orderBy(DB::raw('RAND()'))->take(1)->get();
        $vid = Session::get('id');
        $data = DB::table('reviews')
                ->where('vehicals_id', '=', $_GET['limo_id'])
                ->where('reviewed_by_id', '=', $vid)
                ->get();
        if ($data) {
            $affectedRows = \App\Reviews::where('reviewed_by_id', '=', $vid)->where('vehicals_id', '=', $_GET['limo_id'])->update(['review_rating' => $_GET['rating']]);
        } else {
            $reviews = new \App\Reviews;
            $reviews->user_id = $_GET['user_id'];
            $reviews->vehicals_id = $_GET['limo_id'];
            $reviews->reviewed_by_id = $vid;
            $reviews->review_rating = $_GET['rating'];
            $reviews->review_time = date('Y-m-d');
            $reviews->image = $randimage[0]->img_name;
            $reviews->save();
        }
    }

    function save_message() {
        $vid = Session::get('id');
        $user = \App\User::find($_GET['user_id']);
        $company = \App\User::find($_GET['user_id'])->company;
//        print_r();exit;
        $recemail = $user->email;
        $message = new \App\Messages;
        $message->message_to = $_GET['user_id'];
        $message->message = $_GET['message'];
        $message->message_from = $vid;
        $message->sender_email = $_GET['sender_email'];
        $message->sender_name = $_GET['sender_name'];
        $message->message_date = date('Y-m-d h:i:s');
        $message->save();
        $data['text'] = '';
        $data['company'] = $company[0]->comp_name;
        $emaildata = array('to' => $recemail, 'to_name' => $_GET['sender_name']);
        \Illuminate\Support\Facades\Mail::send('email_message', $data, function($message) use ($emaildata) {
            $message->to($emaildata['to'], 'Dear ' . $_GET['sender_name'])
                    ->from('no-reply@instalimos.com', 'InstaLimos')
                    ->subject('Someone really wants your limo!');
        });
    }

    function get_all_related() {
        $data['types'] = \App\Limotypes::all()->toArray();
        $data['searchdata'] = DB::table('vehicles')
                ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                ->join('users', 'users.id', '=', 'vehicles.user_id')
                ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                ->where('vehicles.owner_city', '=', $_POST['location'])
                ->where('vehimages.main_img', 1)
                ->where('vehicles.limo_id', '!=', $_POST['limo_id'])
                ->where('vehicles.is_active', '=', 1)
                ->take(10)
                ->get();
        $data['maxcount'] = 0;
        foreach ($data['searchdata'] as $lead) {
            if ($lead->veh_price > $data['maxcount']) {
                $data['maxcount'] = $lead->veh_price;
            }
        }
        $data['searchquery'] = array(
            'locality' => $_POST['location']
//            'limo_type'=>$searchquery['limo_type']
        );
//        print_r($data['searchdata']);exit;
        if ($data['searchdata'][0]->limo_id) {
            
        } else {
            $data['searchdata'] = [];
        }
        $data['title'] = 'Instalimos';
        $data['aminties'] = \App\Limoamen::all()->toArray();
        return view('searchedpage', $data);
    }

    function getjsorting() {

        $searchquery = Session::get('searchdata');
       
       $v_email = Session::get('v_email');
       
        $searchdate = date('m/d/Y', strtotime($_GET['searechdate']));
        if ($_GET['country']) {
            $country = $_GET['country'];
        } else {
            $country = $searchquery['country'];
        }
        $city = $searchquery['locality'];
        if ($_GET['city']) {
            $city = $_GET['city'];
//            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        } elseif ($city) {
            $city = $searchquery['locality'];
//            $sort = "FIELD(vehicles.owner_city , '$city') DESC";
        }
        $price = intval($_GET['price']);
        if ($_GET['order_by'] != 'rel') {
            $companies = DB::table('vehicles')
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('vehicles.veh_price', '<=', $price)
                    ->where('users.account_status', '=', 1)
                    ->where('vehicles.is_active', '=', 1)
                    ->where('vehicles.owner_country', '=', $country)
                    ->where('vehicles.veh_seats', '>=', $_GET['passengers'])
                    ->where('vehicles.veh_seats', '<=', $_GET['passengers'] * 3)
                    ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                    //->groupBy('vehicles.limo_id')
                    ->orderBy($_GET['column'], $_GET['order_by'])
                    ->where('users.deactive_account', '=', 0)
                    ->where(function($query) {
                        if (isset($_GET['type']) && $_GET['type'] != 1) {
                            $query->where('vehicles.limo_type', '=', $_GET['type']);
                        }
                    })
                    ->where(function($query) {
                        if (isset($_GET['selected'])) {
                            if ($_GET['selected'][0] != 'No Amenities') {
                            foreach ($_GET['selected'] as $sel):
                                $query->where('vehicles.amenities', 'like', '%' . $sel . '%');
                            endforeach;
                            }
                        }
                    })
                    ->groupBy('vehicles.user_id')
                    ->get();
            $companycount = array();
            foreach ($companies as $companydata) {
                if ($searchquery['lat'] && $companydata->lng) {
                    $point1_lat = $searchquery['lat'];
                    $point1_lng = $searchquery['lng'];
                    $point2_lat = $companydata->lat;
                    $point2_lng = $companydata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    if ($miles <= $companydata->service_area) {
                        $companycount[] = $companydata;
                    }
                }
            }
            $sdata = DB::table('vehicles')
                    ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('vehicles.veh_price', '<=', $price)
                    ->where('users.account_status', '=', 1)
                    ->where('vehicles.is_active', '=', 1)
                    ->where('vehicles.owner_country', '=', $country)
                    ->where('vehicles.veh_seats', '>=', $_GET['passengers'])
                    ->where('vehicles.veh_seats', '<=', $_GET['passengers'] * 3)
                    ->where('vehicles.limo_cal', 'NOT like', '%' . $searchdate . '%')
                    ->groupBy('vehicles.limo_id')
                    ->orderBy($_GET['column'], $_GET['order_by'])
                    ->where('users.deactive_account', '=', 0)
                    ->where(function($query) {
                        if (isset($_GET['type']) && $_GET['type'] != 1) {
                            $query->where('vehicles.limo_type', '=', $_GET['type']);
                        }
                    })
                    ->where(function($query) {
                        if (isset($_GET['selected'])) {
                            if ($_GET['selected'][0] != 'No Amenities') {
                            foreach ($_GET['selected'] as $sel):
                                $query->where('vehicles.amenities', 'like', '%' . $sel . '%');
                            endforeach;
                            }
                        }
                    })
//                    ->take(10)
                    ->get();
        }else {
            
            $companies = DB::table('vehicles')
                   //->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('vehicles.veh_price', '<=', $price)
                    ->where('users.account_status', '=', 1)
                    ->where('vehicles.is_active', '=', 1)
                    ->where('vehicles.owner_country', '=', $country)
//                    ->where('vehicles.limo_cal', 'NOT like', '%'.$searchdate.'%')
                    ->where('vehicles.veh_seats', '>=', $_GET['passengers'])
                    ->where('vehicles.veh_seats', '<=', $_GET['passengers'] * 3)
//                    ->groupBy('vehicles.limo_id')
                    ->where(function($query) {
                        if (isset($_GET['type'])) {
                            $query->where('vehicles.limo_type', '=', $_GET['type']);
                        }
                    })
                    ->where(function($query) {
                        if (isset($_GET['selected'])) {
                            if ($_GET['selected'][0] != 'No Amenities') {
                            foreach ($_GET['selected'] as $sel):
                                $query->where('vehicles.amenities', 'like', '%' . $sel . '%');
                            endforeach;
                        }}
                    })
                    ->groupBy('vehicles.user_id')
                    ->get();
            $companycount = array();
            foreach ($companies as $companydata) {
                if ($searchquery['lat'] && $companydata->lng) {
                    $point1_lat = $searchquery['lat'];
                    $point1_lng = $searchquery['lng'];
                    $point2_lat = $companydata->lat;
                    $point2_lng = $companydata->lng;
                    $theta = $point1_lng - $point2_lng;
                    $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                    $miles = acos($miles);
                    $miles = rad2deg($miles);
                    $miles = $miles * 60 * 1.1515;
                    if ($miles <= $companydata->service_area) {
                        $companycount[] = $companydata;
                    }
                }
            }
            $sdata = DB::table('vehicles')
                    ->select(array('vehicles.*', 'users.*', 'company.*', 'vehimages.*', DB::raw('AVG(reviews.review_rating) as count')))
                    ->join('users', 'users.id', '=', 'vehicles.user_id')
                    ->join('company', 'company.user_id', '=', 'vehicles.user_id')
                    ->join('vehimages', 'vehimages.vehicals_id', '=', 'vehicles.limo_id')
                    ->leftJoin('reviews', 'reviews.vehicals_id', '=', 'vehicles.limo_id')
                    ->where('vehimages.main_img', 1)
                    ->where('vehicles.veh_price', '<=', $price)
                    ->where('users.account_status', '=', 1)
                    ->where('vehicles.is_active', '=', 1)
                    ->where('vehicles.owner_country', '=', $country)
//                    ->where('vehicles.limo_cal', 'NOT like', '%'.$searchdate.'%')
                    ->where('vehicles.veh_seats', '>=', $_GET['passengers'])
                    ->where('vehicles.veh_seats', '<=', $_GET['passengers'] * 3)
                    ->groupBy('vehicles.limo_id')

//                ->orderBy($_GET['column'], $_GET['order_by'])
                    ->where(function($query) {
                        if (isset($_GET['type'])) {
                            $query->where('vehicles.limo_type', '=', $_GET['type']);
                        }
                    })
                    ->where(function($query) {
                        if (isset($_GET['selected'])) {
                            if ($_GET['selected'][0] != 'No Amenities') {
                            foreach ($_GET['selected'] as $sel):
                                $query->where('vehicles.amenities', 'like', '%' . $sel . '%');
                            endforeach;}
                        }
                    })
//                    ->take(10)
                    ->get();
        }
        $id = Session::get('id');
            $result = count($companycount);
            if($result > 1)
            {
                $result = $result-1;
            }
            $searchcompnies = new \App\searched_compnies();
            $searchcompnies->compnies_searched = $result;
            $searchcompnies->vister_id = $id;
            $searchcompnies->save();
        
        $email = Session::get('v_email');
        $getdistance = '';
        foreach ($sdata as $newdata) {
            if ($searchquery['lat']) {
                $point1_lat = $_GET['lat'];
                $point1_lng = $_GET['lng'];
                $point2_lat = $newdata->lat;
                $point2_lng = $newdata->lng;
                $theta = $point1_lng - $point2_lng;
                $miles = (sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($theta)));
                $miles = acos($miles);
                $miles = rad2deg($miles);
                $miles = $miles * 60 * 1.1515;
                if ($miles <= $newdata->service_area) {
                    $getdistance[] = $newdata;
                }
            }
        }if (!$getdistance) {
            $getdistance = [];
        }
        $data = $getdistance;
        foreach ($data as $lead) {
            $getvister = DB::table('leeds_meesage')->where('vister_email', '=', $email)->where('lemo_searched', '=', $lead->limo_id)->get();
            $checkVister = DB::table('leeds_meesage')
                    ->where('user_id', '=', $lead->id)
                    ->where('vister_email', '=', $v_email)
                    ->where(\DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), \DB::raw('curdate()'))
                    ->get();
            $getuser = DB::table('users')->where('email', '=', $email)->get();
            $getvitername = DB::table('visters')->where('id', '=', $id)->get();
            if (!$checkVister && !$getuser) {
                $leads = new \App\Leeds;
                $leads->searched_by_id = $id;
                $leads->vister_email = $email;
                $leads->lemo_searched = $lead->limo_id;
                $leads->user_id = $lead->user_id;
                $leads->passengers = $_GET['passengers'];
                $leads->occian_type = $searchquery['occian_type'];
                $leads->picklocation = $_GET['picklocation'];
                $leads->search_date = $searchquery['daterequired'];
                $leads->save();
                $data['text'] = $leads;
                $data['fname'] = $getvitername[0]->firstname;
                $data['lname'] = $getvitername[0]->lastname;
                if ($getvitername[0]->contact) {
                    $contact = substr($getvitername[0]->contact, 0, 0) . "(" . substr($getvitername[0]->contact, 0, 3) . ") " . substr($getvitername[0]->contact, 3, 3) . "-" . substr($getvitername[0]->contact, 6);
                } else {
                    $contact = '';
                }
                $data['contact'] = $contact;
                $emaildata = array('replyto' => $email, 'replytoname' => $getvitername[0]->firstname, 'to' => $lead->email, 'to_name' => $lead->username);
                \Illuminate\Support\Facades\Mail::send('emails_welcome', $data, function($message) use ($emaildata) {
                    $message->to($emaildata['to'], 'Dear User')
                            ->from('no-reply@instalimos.com', 'InstaLimos')
                            ->replyTo($emaildata['replyto'], $emaildata['replytoname'])
                            ->subject('Timely: Instalimos Inquiry');
                });
                $payments = DB::table('save_payments')->where('user_id', '=', $lead->user_id)->get();
                if ($payments[0]->total_amount < 25) {
                    $user = \App\User::find($lead->user_id);
                    $user->charge(10000);
                    if (!$user->charge(10000)) {
                        $othercard = DB::table('craditcard_details')->where('user_id', '=', $lead->user_id)->get();
                        if ($othercard) {
                            \Stripe::setApiKey('sk_live_iDuCgn5i3mHb0f9UZTsc5Eyx');
                            $rp = \Laravel\Cashier\Customer::retrieve($user->stripe_id);
                            $rp->default_card = $othercard[0]->stripe_c_id;
                            $rp->save();
                            $user->charge(10000);
                            if (!$user->charge(10000)) {
                                \App\User::where('id', '=', $lead->user_id)->update(['account_status' => 0]);
                            } else {
                                $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                                $amount->increment('total_amount', 100);
                                \App\User::where('id', '=', $lead->user_id)->update(['default_card'=> 0]);
                            }
                        } else {
                            \App\User::where('id', '=', $lead->user_id)->update(['account_status'=> 0]);
                        }
                    } else {
                        $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                        $amount->increment('total_amount', 100);
                    }
                } else {
                    $amount = \App\Savepayments::where('user_id', '=', $lead->user_id);
                    $amount->decrement('total_amount', 2);
                }
            }
        }
        print_r(json_encode($data));
    }

    function logout() {
        Auth::logout();
        Illuminate\Support\Facades\Cache::flush();
        Session::flush();
        return Redirect::to(\Illuminate\Support\Facades\URL::previous());
    }

    function terms() {
        return \Illuminate\Support\Facades\View::make('terms', array('title' => 'Terms And Condtions'));
    }

}
