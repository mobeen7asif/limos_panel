if (window.File && window.FileList) {
    var filesInput = $("#files");
    filesInput.on("change", function(e) {
        var files = e.target.files; //FileList object
		console.log (files);
        var result = $("#result");
        $.each(files, function(i, file) {
			
            var pReader = new FileReader();
            pReader.addEventListener("load", function(e) {
                var pic = e.target;
                result.append("<div class='preview'><img class='thumbnail' src='" + pic.result + "'/></div>");
                //show();
            });
            pReader.readAsDataURL(file);

        });


    });
} else {
    console.log("Not supported browser");
}

function show() {
    $(".thumbnail").click(function() {
        EXIF.getData(this, function() {
            var make = EXIF.getTag(this, "Make"),
                    model = EXIF.getTag(this, "Model");
            alert("I was taken by a " + make + " " + model);
        });
    });
}


!function(t) {
    t.fn.rating = function(i) {
        i = i || function() {
        }, this.each(function(n, a) {
            t(a).data("rating", {callback: i}).bind("init.rating", t.fn.rating.init).bind("set.rating", t.fn.rating.set).bind("hover.rating", t.fn.rating.hover).trigger("init.rating")
        })
    }, t.extend(t.fn.rating, {init: function() {
            for (var i = t(this), n = "", a = null, e = i.children(), r = 0, s = e.length; s > r; r++)
                n = n + '<a class="star" title="' + t(e[r]).val() + '" />', t(e[r]).is(":checked") && (a = t(e[r]).val());
            e.hide(), i.append('<div class="stars">' + n + "</div>").trigger("set.rating", a), t("a", i).bind("click", t.fn.rating.click), i.trigger("hover.rating")
        }, set: function(i, n) {
            var a = t(this), e = t("a", a), r = void 0;
            n && (e.removeClass("fullStar"), r = e.filter(function() {
                return t(this).attr("title") == n ? t(this) : !1
            }), r.addClass("fullStar").prevAll().addClass("fullStar"))
        }, hover: function() {
            var i = t(this), n = t("a", i);
            n.bind("mouseenter", function() {
                t(this).addClass("tmp_fs").prevAll().addClass("tmp_fs"), t(this).nextAll().addClass("tmp_es")
            }), n.bind("mouseleave", function() {
                t(this).removeClass("tmp_fs").prevAll().removeClass("tmp_fs"), t(this).nextAll().removeClass("tmp_es")
            })
        }, click: function(i) {
            i.preventDefault();
            var n = t(i.target), a = n.parent().parent(), e = a.children("input"), r = n.attr("title");
            matchInput = e.filter(function() {
                return t(this).val() == r ? !0 : !1
            }), matchInput.attr("checked", !0).siblings("input").attr("checked", !1), a.trigger("set.rating", matchInput.val()).data("rating").callback(r, i)
        }})
}(jQuery);

//equal height
equalheight = function(t) {
    var i, e = 0, h = 0, r = new Array;
    $(t).each(function() {
        if (i = $(this), $(i).height("auto"), topPostion = i.position().top, h != topPostion) {
            for (currentDiv = 0; currentDiv < r.length; currentDiv++)
                r[currentDiv].height(e);
            r.length = 0, h = topPostion, e = i.height(), r.push(i)
        } else
            r.push(i), e = e < i.height() ? i.height() : e;
        for (currentDiv = 0; currentDiv < r.length; currentDiv++)
            r[currentDiv].height(e)
    })
};
//triggers
jQuery(document).ready(function($) {
    jQuery('.fancybox').fancybox({modal: true, helpers: {overlay: {locked: false, closeClick: false}}, padding: 0});
    jQuery(".nav_toggle").click(function(e) {
            jQuery("#sidebar_dasboard").slideToggle();  
           });
    jQuery(".menu_trigger").click(function(e) {
        jQuery("#main_nav").slideToggle('fast');
        return false;
    });
    jQuery('#home_slider .bxslider').bxSlider({
        auto: true,
        pause:4000,
        controls:false
    });
    jQuery('#limo_slider .bxslider').bxSlider({
        pagerCustom: '#bx-pager'
    });
    $(".button").on("click", function() {

  var $button = $(this);
  var oldValue = $button.parent().find("input").val();

  if ($button.text() == "+") {
	  var newVal = parseFloat(oldValue) + 1;
          if (oldValue == "" ) {
            newVal = 1;
          }

         
	}
     else {
   // Don't allow decrementing below zero
    if (oldValue > 1 ) {
      var newVal = parseFloat(oldValue) - 1;
    }
   
    else {
      newVal = 1;
    }
  }

  $button.parent().find("input").val(newVal);

});
//    jQuery(function($) {
//       // $('#popupDatepicker').datepick();
//        $('#popupDatepicker').datepick({
//            
//    minDate: 0,
//
//});
//        $('#inlineDatepicker').datepick({onSelect: showDate});
//    });
    $('#datetimepicker').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        formatTimeScroller: 'g:i A',
         minDate:'0'

    });
        $('#searchdatepicker').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        formatTimeScroller: 'g:i A',
         minDate:'0'

    });
     $('#datepicker').datetimepicker({
        timepicker:false,
        format:'d.m.Y'

    });
    jQuery('.drop_trigger').click(function(e) {
        jQuery(this).toggleClass('active');
        jQuery(this).parent().find('.dropdown_hide').toggle();
        var txt = jQuery(this).parent().find('.dropdown_hide').text();

    });
    
    jQuery('.review_trigger').click(function(e) {
        jQuery(".reviews_list .review").toggle();
        jQuery(this).hide();
        return false;
    });
    jQuery('.field_row label').click(function(e) {
        var txt = jQuery(this).text();
        jQuery(this).parents('.inputfield').find('.drop_trigger').text(txt);
		jQuery('.dropdown_hide').hide();

    });
    jQuery('.star_rating').rating();
    jQuery("#part_trigger ").click(function(e) {
        jQuery("#party_drp").toggle();
    });
    jQuery("#party_drp li").click(function(e) {
        var txt = jQuery(this).html();
        jQuery(this).parents(".inputfield").find("#part_trigger").html(txt);
        jQuery(this).parent().hide();
    });
    jQuery("body").click(function(e){
        jQuery("#party_drp").hide();
    });
    jQuery("#part_trigger").click(function(event){
        event.stopPropagation();
    });
    jQuery("#dropdown_hide input:radio").change({
    });
	
});
$(window).resize(function() {
     equalheight('.equal_columns');
});
$(window).load(function() {
    equalheight('.equal_columns');
    jQuery("#slider-range").noUiSlider({
        start: [500],
        step: 1,
        range: {
            'min': 0,
            'max': 500,
        }
    });
    jQuery("#slider-range").Link('lower').to('-inline-<div class="tooltip"></div>');
    
	//jQuery('.fancybox_close').fancybox({padding: 0});
    jQuery(".fancybox_auto").fancybox({padding: 0, modal: true, helpers: {overlay: {closeClick: false}}}).last().trigger('click');
    jQuery('.g_fancybox, .fancybox_close').fancybox({
        padding: 0,
		helpers: {overlay: {locked: false, closeClick: false}}
    });

});
        $(document).ready(function(e) {
            $("#register").hide();
           $(".register_trigger").click(function(e) {
               $("#login").hide();
               $("#register").show();
                $(this).removeClass('active');
                $(".login_trigger").addClass('active');
                return false;
           });
            $(".login_trigger").click(function(e) {
                $("#register").hide();
                $("#login").show();
                $(this).removeClass('active');
                $(".register_trigger").addClass('active');
                return false;
           });
            
          
        });
