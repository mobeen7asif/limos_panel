!function(t){t.fn.rating=function(i){i=i||function(){},this.each(function(n,a){t(a).data("rating",{callback:i}).bind("init.rating",t.fn.rating.init).bind("set.rating",t.fn.rating.set).bind("hover.rating",t.fn.rating.hover).trigger("init.rating")})},t.extend(t.fn.rating,{init:function(){for(var i=t(this),n="",a=null,e=i.children(),r=0,s=e.length;s>r;r++)n=n+'<a class="star" title="'+t(e[r]).val()+'" />',t(e[r]).is(":checked")&&(a=t(e[r]).val());e.hide(),i.append('<div class="stars">'+n+"</div>").trigger("set.rating",a),t("a",i).bind("click",t.fn.rating.click),i.trigger("hover.rating")},set:function(i,n){var a=t(this),e=t("a",a),r=void 0;n&&(e.removeClass("fullStar"),r=e.filter(function(){return t(this).attr("title")==n?t(this):!1}),r.addClass("fullStar").prevAll().addClass("fullStar"))},hover:function(){var i=t(this),n=t("a",i);n.bind("mouseenter",function(){t(this).addClass("tmp_fs").prevAll().addClass("tmp_fs"),t(this).nextAll().addClass("tmp_es")}),n.bind("mouseleave",function(){t(this).removeClass("tmp_fs").prevAll().removeClass("tmp_fs"),t(this).nextAll().removeClass("tmp_es")})},click:function(i){i.preventDefault();var n=t(i.target),a=n.parent().parent(),e=a.children("input"),r=n.attr("title");matchInput=e.filter(function(){return t(this).val()==r?!0:!1}),matchInput.attr("checked",!0).siblings("input").attr("checked",!1),a.trigger("set.rating",matchInput.val()).data("rating").callback(r,i)}})}(jQuery);

//equal height
equalheight=function(t){var i,e=0,h=0,r=new Array;$(t).each(function(){if(i=$(this),$(i).height("auto"),topPostion=i.position().top,h!=topPostion){for(currentDiv=0;currentDiv<r.length;currentDiv++)r[currentDiv].height(e);r.length=0,h=topPostion,e=i.height(),r.push(i)}else r.push(i),e=e<i.height()?i.height():e;for(currentDiv=0;currentDiv<r.length;currentDiv++)r[currentDiv].height(e)})};
//triggers
jQuery(document).ready(function($) {

		jQuery(".menu_trigger").click(function(e) {
			jQuery("#main_nav").slideToggle('fast');
			return false;
		});
		jQuery('#home_slider .bxslider').bxSlider();
		jQuery('#limo_slider .bxslider').bxSlider({
				 pagerCustom: '#bx-pager'
			});
		 jQuery(function($) {
			$('#popupDatepicker').datepick();
				$('#inlineDatepicker').datepick({onSelect: showDate});
			});
		jQuery('.drop_trigger').click(function(e) {
			jQuery(this).toggleClass('active');
            jQuery(this).parent().find('.dropdown_hide').toggle();
			var txt = jQuery(this).parent().find('.dropdown_hide').text();
			
        });
		jQuery('.field_row label').click(function(e) {
			
			var txt = jQuery(this).text();
			//alert(txt);
			jQuery(this).parents('.inputfield').find('.drop_trigger').text(txt);
			
        });
		 jQuery('.star_rating').rating();
		
		 jQuery('.fancybox').fancybox({  helpers: {overlay: {locked: false,closeClick: false }},padding: 0});
		 jQuery(".fancybox_auto").fancybox({padding: 0, helpers:{overlay : {closeClick: false}}}).last().trigger('click');
		
		});
$(window).load(function(){
		equalheight('.equal_columns');
		jQuery("#slider-range").noUiSlider({
			start: [15],
			step: 1,
			range: {
				'min': 1,
				'max': 100,
			}
		});
		 jQuery("#slider-range").Link('lower').to('-inline-<div class="tooltip"></div>');
	});
